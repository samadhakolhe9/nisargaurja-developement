//
//  AddressCell.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 31/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class AddressCell: UITableViewCell {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var btnAddressTitle: UIButton!
    @IBOutlet weak var btnServiceStatus: UIButton!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnSetAsDefault: UIButton!
    @IBOutlet weak var lblDefaultAddressText: UILabel!
    @IBOutlet weak var addressLabelTopConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        outerView.applyCommonShadow()
        btnSetAsDefault.layer.borderColor = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1).cgColor
        btnSetAsDefault.layer.borderWidth = 0.5
        btnSetAsDefault.layer.cornerRadius = 5
        btnSetAsDefault.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
