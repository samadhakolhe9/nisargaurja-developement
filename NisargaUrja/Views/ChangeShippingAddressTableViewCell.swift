//
//  ChangeShippingAddressTableViewCell.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 12/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class ChangeShippingAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var radioButton: UIButton!
    @IBOutlet weak var btnAddressTitle: UIButton!
    @IBOutlet weak var btnServiceStatus: UIButton!
    @IBOutlet weak var lblAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        outerView.applyCommonShadow()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
