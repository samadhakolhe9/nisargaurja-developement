//
//  RelatedRecipesCell.swift
//  NisargaUrja
//  Created by Samadhan Kolhe on 20/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class RelatedRecipesCell: UICollectionViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblOrganizeBy: UILabel!
    @IBOutlet weak var btnViews: UIButton!
    @IBOutlet weak var btnFavorites: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        
        bgView.layer.cornerRadius = 5
        bgView.layer.masksToBounds = false
        bgView.layer.shadowRadius = 5
        bgView.layer.shadowOpacity = 1
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowOffset = CGSize(width: 0 , height:2)
        bgView.backgroundColor = .white
    }
    
    @IBAction func btnFavoritesPressed(_ sender: UIButton) {
    }
}
