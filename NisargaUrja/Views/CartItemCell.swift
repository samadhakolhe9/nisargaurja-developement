//
//  CartItemCell.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 19/09/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class CartItemCell: UITableViewCell {

    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblBrandName: UILabel!
    @IBOutlet weak var stprProducctQuantity: GMStepper!
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var lblDiscountedPrice: UILabel!
    @IBOutlet weak var lblMRP: UILabel!
    
    var quantityCount:Double = 0
    var variationUID = ""
    var isAbleToChange = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnAddToCart.applyCommonShadow(shadowColor: AppCommonColor)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func StepperAction(_ sender: GMStepper) {
           if isAbleToChange {
               if stprProducctQuantity.value > quantityCount {
                   AddToCart()
                   print("Increasing...", stprProducctQuantity.value)
               } else if stprProducctQuantity.value < quantityCount || self.stprProducctQuantity.value == 0.0 {
                   RemoveFromCart()
                   print("Decreasing...", stprProducctQuantity.value)
               }
           }
       }
       
       @IBAction func AddToCartButtonAction(_ sender: UIButton) {
           AddToCart()
       }
       
       func AddToCart() {
           let urlString = APIConstants.addToCartItem+"variationUID=\(variationUID)&phoneNo=\(globalUserPhone)"
           ApiManager().postApiRequest(url: urlString, dict: [:], runLoader: true, showError: false) { (Response, success) in
            self.isAbleToChange = false
               if success {
                   self.btnAddToCart.isHidden = true
                   self.stprProducctQuantity.isHidden = false
                   self.stprProducctQuantity.value = self.stprProducctQuantity.value == 0.0 ? 1 : self.stprProducctQuantity.value
               } else {
                   self.stprProducctQuantity.value = self.quantityCount
               }
               self.quantityCount = self.stprProducctQuantity.value
               self.isAbleToChange = true
               NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateCartItem"), object: nil)
           }
       }
       
       func RemoveFromCart() {
           let urlString = APIConstants.removeFromCartItem+"variationUID=\(variationUID)&phoneNo=\(globalUserPhone)"
           ApiManager().postApiRequest(url: urlString, dict: [:], runLoader: true, showError: false) { (Response, success) in
           self.isAbleToChange = false
               if success {
                   self.btnAddToCart.isHidden = self.stprProducctQuantity.value == 0.0 ? false : true
                   self.stprProducctQuantity.isHidden = !self.btnAddToCart.isHidden
               } else {
                   self.stprProducctQuantity.value = self.quantityCount
               }
               self.quantityCount = self.stprProducctQuantity.value
               self.isAbleToChange = true
               NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateCartItem"), object: nil)
           }
       }
    
}
