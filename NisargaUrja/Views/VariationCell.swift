//
//  VariationCell.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 13/09/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class VariationCell: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblVariationTitle: UILabel!
    @IBOutlet weak var lblMRP: UILabel!
    @IBOutlet weak var lblDiscountedPrice: UILabel!
    @IBOutlet weak var stprProducctQuantity: GMStepper!
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var addButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgViewWidthConstraint: NSLayoutConstraint!
    
    var isAbleToChange = false
    var variationUID = ""
    var quantityCount:Double = 0
    
    @IBAction func StepperAction(_ sender: GMStepper) {
        if self.isAbleToChange {
          if self.stprProducctQuantity.value > self.quantityCount {
                self.AddToCart()
                print("Increasing...", self.stprProducctQuantity.value)
            } else if self.stprProducctQuantity.value < self.quantityCount || self.stprProducctQuantity.value == 0.0 {
                self.RemoveFromCart()
                print("Decreasing...", self.stprProducctQuantity.value)
            }
            self.quantityCount = self.stprProducctQuantity.value
        }
    }
    
    @IBAction func AddToCartButtonAction(_ sender: UIButton) {
        AddToCart()
    }
    
    func AddToCart() {
        if DefaultAddressArray != nil && DefaultAddressArray.isDeliverable {
                  let urlString = APIConstants.addToCartItem+"variationUID=\(variationUID)&phoneNo=\(globalUserPhone)"
                  ApiManager().postApiRequest(url: urlString, dict: [:], runLoader: true, showError: false) { (Response, success) in
                  self.isAbleToChange = false
                      if success {
                      } else {
                          self.stprProducctQuantity.value = self.quantityCount
                      }
                      self.quantityCount = self.stprProducctQuantity.value
                      self.btnAddToCart.isHidden = true
                      self.stprProducctQuantity.isHidden = false
                      self.stprProducctQuantity.value = self.stprProducctQuantity.value == 0.0 ? 1 : self.stprProducctQuantity.value
                    CartRightButton.badge = "\(Int("\(CartRightButton.badge != "" ? CartRightButton.badge! : "0")")! + 1)"
                      self.isAbleToChange = true
                      hhTabBarView.tabBarTabs[2].badge = CartRightButton.badge
                      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateCartTotal"), object: nil)
                  }
        } else {
            self.stprProducctQuantity.value = self.quantityCount
            AddressNotServisable()
        }
    }
    
    func RemoveFromCart() {
        let urlString = APIConstants.removeFromCartItem+"variationUID=\(variationUID)&phoneNo=\(globalUserPhone)"
        ApiManager().postApiRequest(url: urlString, dict: [:], runLoader: true, showError: false) { (Response, success) in
        self.isAbleToChange = false
            if success {
            } else {
                self.stprProducctQuantity.value = self.quantityCount
            }
            self.quantityCount = self.stprProducctQuantity.value
            self.btnAddToCart.isHidden = self.stprProducctQuantity.value == 0.0 ? false : true
            self.stprProducctQuantity.isHidden = !self.btnAddToCart.isHidden
            CartRightButton.badge = "\(Int("\(CartRightButton.badge != "" ? CartRightButton.badge! : "0")")! - 1)"
            self.isAbleToChange = true
            hhTabBarView.tabBarTabs[2].badge = CartRightButton.badge
            hhTabBarView.tabBarTabs[2].badgeLabel.isHidden = (CartRightButton.badge == "" || CartRightButton.badge == "0") ? true : false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateCartTotal"), object: nil)
        }
    }
    
    func AddressNotServisable() {
        let alertController = UIAlertController(title: "Warning!", message: "You current address is not serviceable. Please change shipping address.", preferredStyle: UIAlertController.Style.alert)
        let saveAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "idChangeShippingAddressVC") as! ChangeShippingAddressVC
            UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
        
    }
}
