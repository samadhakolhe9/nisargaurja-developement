//
//  CouponCell.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 30/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class CouponCell: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblCouponText: UILabel!
    
}
