//
//  BrandCell.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 04/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class BrandCell: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgBrand: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lblBrandName: UILabel!
    @IBOutlet weak var lblOfferPercentage: UILabel!
    
    override func awakeFromNib() {

    }
}
