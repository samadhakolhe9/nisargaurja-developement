//
//  RecipeStepsCell.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 24/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class RecipeStepsCell: UITableViewCell {

    @IBOutlet weak var lblStepDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
