//
//  SearchListCell.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 29/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class SearchListCell: UITableViewCell {

    @IBOutlet weak var lblSearchedText: UILabel!
    @IBOutlet weak var lblSearchType: UILabel!
    @IBOutlet weak var searchIconWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
