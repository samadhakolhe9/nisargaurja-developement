//
//  ImageHeaderCell.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 11/3/15.
//  Copyright © 2015 Yuji Hato. All rights reserved.
//

import UIKit
protocol ImageHeaderViewDelegate {
    func didCloseSideMenu()
}
class ImageHeaderView : UIView {
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblUserAddress: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    var delegate:ImageHeaderViewDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgUser.applyCircleShadow(shadowRadius: 5, shadowOpacity: 2, shadowColor: UIColor.lightGray.cgColor, shadowOffset: CGSize(width: 0 , height:2))
    }
    
    @IBAction func CloseMenuButtonAction(_ sender: UIButton) {
        delegate.didCloseSideMenu()
    }
}
