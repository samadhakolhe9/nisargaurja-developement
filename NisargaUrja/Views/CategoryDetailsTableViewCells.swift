//
//  CategoryDetailsTableViewCells.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 18/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class OffersCell: UITableViewCell {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var imgGradientBackground: UIImageView!
    @IBOutlet weak var lblOfferPercentAmount: UILabel!
    @IBOutlet weak var offerSeperatorView: UIView!
    @IBOutlet weak var lblOfferMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //imgGradientBackground.applyCommonShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class ItemCategoryDetailsCell: UITableViewCell {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var btnHeart: UIButton!
    @IBOutlet weak var lblOriginalMRP: UILabel!
    @IBOutlet weak var lblByNU: UILabel!
    @IBOutlet weak var lblOfferMRP: UILabel!
    @IBOutlet weak var stprProducctQuantity: GMStepper!
    @IBOutlet weak var viewGrayBorderLine: UIView!
    @IBOutlet weak var outerViewOfferMessage: UIView!
    @IBOutlet weak var lblOfferPercentage: UILabel!
    @IBOutlet weak var viewOfferLabelSeperator: UIView!
    @IBOutlet weak var lblOfferMessage: UILabel!
    @IBOutlet weak var viewGrayBorderBottom: UIView!
    @IBOutlet weak var lblNUQuoteMessage: UILabel!
    @IBOutlet weak var btnWeightDropdown: UIButton!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var lblSelectedKG: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var txtDropdownQuantity: DropDown!
    @IBOutlet weak var btnAddToCart: UIButton!
    @IBOutlet weak var addButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var percentageView: UIView!
    @IBOutlet weak var dropdownWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var dropdownDividerWidthConstraint: NSLayoutConstraint!
    
    var CategoryProducts:CategoryData!
    var CartListArray:[CartData]!
    var allMRP:[String]!
    var allDiscountPrice:[Double]!
    var quantityCount:Double = 0
    var variationUID = ""
    var isAbleToChange = false
    var isAddButton = true
    override func awakeFromNib() {
        super.awakeFromNib()
       // outerView.applyCommonShadow()
        
        btnAddToCart.backgroundColor = isAddButton ? AppCommonColor : .clear // #colorLiteral(red: 0.8980392157, green: 0.5333333333, blue: 0.3137254902, alpha: 1)
        btnAddToCart.layer.cornerRadius = 15
        btnAddToCart.layer.borderWidth = 0.2
        btnAddToCart.layer.masksToBounds = false
        btnAddToCart.layer.shadowRadius = 5
        btnAddToCart.layer.shadowOpacity = 0.5
        btnAddToCart.layer.shadowOffset = CGSize(width: 0 , height:2)
        
        percentageView.applyCommonShadow(borderColor: AppCommonColor)
    
        
        imgProduct.clipsToBounds = true
        imgProduct.layer.cornerRadius = 0
        
        txtDropdownQuantity.didSelect{(selectedTextArray , indexs , ids) in
            self.variationUID = self.CategoryProducts.variationUID[indexs[0]]
            let cartValue = self.CartListArray.filter{$0.variationUID == self.variationUID}
            self.isAbleToChange = false
            
            self.lblOriginalMRP.attributedText = "MRP: ₹\(self.allMRP[indexs[0]])".strikeThrough()
            self.lblOfferMRP.text = String(format:"You Pay: ₹ %.1f", self.allDiscountPrice[indexs[0]])

            let MRP = Double("\(self.allMRP[indexs[0]])")!
            let discountedPrices = Double("\(self.allDiscountPrice[indexs[0]])")!
            let diffrence = MRP-discountedPrices
            let multiply = diffrence*100
            self.lblOfferPercentage.text = String(format:"%.0f", multiply/MRP) + "% OFF!"
            
            self.stprProducctQuantity.value = cartValue.count > 0 ? Double(Int("\(cartValue[0].quantity!)")!) : 0.0
            self.quantityCount = self.stprProducctQuantity.value
            self.btnAddToCart.isHidden = self.stprProducctQuantity.value == 0.0 ? false : true
            self.stprProducctQuantity.isHidden = !self.btnAddToCart.isHidden
            self.txtDropdownQuantity.text = selectedTextArray[0]
            self.isAbleToChange = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnHeartPressed(_ sender: UIButton) {
        SetFavorites()
    }
    
    
    @IBAction func btnWeightDropdownPressed(_ sender: UIButton) {
        
    }
    
    @IBAction func StepperActionOne(_ sender: GMStepper) {
        print("Editing changed...")
    }
    
    @IBAction func StepperAction(_ sender: GMStepper) {
        if self.isAbleToChange {
           if self.stprProducctQuantity.value > self.quantityCount {
                self.AddToCart()
                print("Increasing...", self.stprProducctQuantity.value)
            } else if self.stprProducctQuantity.value < self.quantityCount || self.stprProducctQuantity.value == 0.0 {
                self.RemoveFromCart()
                print("Decreasing...", self.stprProducctQuantity.value)
            }
            self.quantityCount = self.stprProducctQuantity.value
        }
    }
    
    @IBAction func AddToCartButtonAction(_ sender: UIButton) {
        AddToCart()
    }
    
    func AddToCart() {
        if DefaultAddressArray != nil && DefaultAddressArray.isDeliverable {
            let urlString = APIConstants.addToCartItem+"variationUID=\(variationUID)&phoneNo=\(globalUserPhone)"
            ApiManager().postApiRequest(url: urlString, dict: [:], runLoader: true, showError: false) { (Response, success) in
                print(success)
                print(Response)
                self.isAbleToChange = false
                if success {
                } else {
                    self.stprProducctQuantity.value = self.quantityCount
                }
                self.quantityCount = self.stprProducctQuantity.value
                self.btnAddToCart.isHidden = true
                self.stprProducctQuantity.isHidden = false
                self.stprProducctQuantity.value = self.stprProducctQuantity.value == 0.0 ? 1 : self.stprProducctQuantity.value
                self.isAbleToChange = true
                UpdateCartCount()
            }
        } else {
            self.stprProducctQuantity.value = self.quantityCount
            AddressNotServisable()
        }
    }
    
    func RemoveFromCart() {
        let urlString = APIConstants.removeFromCartItem+"variationUID=\(variationUID)&phoneNo=\(globalUserPhone)"
        ApiManager().postApiRequest(url: urlString, dict: [:], runLoader: true, showError: false) { (Response, success) in
            self.isAbleToChange = false
            if success {
            } else {
                self.stprProducctQuantity.value = self.quantityCount
            }
            self.quantityCount = self.stprProducctQuantity.value
            self.btnAddToCart.isHidden = self.stprProducctQuantity.value == 0.0 ? false : true
            self.stprProducctQuantity.isHidden = !self.btnAddToCart.isHidden
            self.isAbleToChange = true
            UpdateCartCount()
        }
    }
    
    func SetFavorites() {
        let urlString = APIConstants.setFavorites+"prodUID=\(CategoryProducts.productUID!)&phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    self.btnHeart.isSelected = fieldsArray[KeysArray["status"].intValue].boolValue
                }
            }
        }
    }
    
    func AddressNotServisable() {
        let alertController = UIAlertController(title: "Warning!", message: "You current address is not serviceable. Please change shipping address.", preferredStyle: UIAlertController.Style.alert)
        let saveAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "idChangeShippingAddressVC") as! ChangeShippingAddressVC
            UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
        
    }
}

