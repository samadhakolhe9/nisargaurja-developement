//
//  OrderDetailsTableViewCells.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 17/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class AddressLabelCell: UITableViewCell {
    
    @IBOutlet weak var lblInvoiceTo: UILabel!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblCustomerMobile: UILabel!
    @IBOutlet weak var lblCustomerEmail: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

class InvoiceHeadersCell: UITableViewCell {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

class ItemsDetailsCell: UITableViewCell {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblItemQuantity: UILabel!
    @IBOutlet weak var lblItemRate: UILabel!
    @IBOutlet weak var lblItemTotalPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

class TotalBillAmountCell: UITableViewCell {
    
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var lblSubtotalAmount: UILabel!
    @IBOutlet weak var lblLoyaltyPoints: UILabel!
    @IBOutlet weak var lblCreditsUsed: UILabel!
    @IBOutlet weak var lblCouponCode: UILabel!
    @IBOutlet weak var lblTotalDiscount: UILabel!
    @IBOutlet weak var lblDeliveryCharges: UILabel!
    @IBOutlet weak var lblLoyaltyPointsAmount: UILabel!
    @IBOutlet weak var lblCreditPointsAmount: UILabel!
    @IBOutlet weak var lblCouponCodeAmount: UILabel!
    @IBOutlet weak var lblDeliveryAmount: UILabel!
    @IBOutlet weak var lblLoyaltyPointsUsed: UILabel!
    @IBOutlet weak var lblAppliedCouponCode: UILabel!
    @IBOutlet weak var lblExtraForTotalDiscount: UILabel!
    @IBOutlet weak var lblTotalDiscountAmount: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblFreeDelivery: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblTransactionNumber: UILabel!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    @IBOutlet weak var lblHSNSAC: UILabel!
    @IBOutlet weak var lblActualOrderNumber: UILabel!
    @IBOutlet weak var lblActualTransactionNumber: UILabel!
    @IBOutlet weak var lblActualPaymentMethod: UILabel!
    @IBOutlet weak var lblActualHSNSAC: UILabel!
    @IBOutlet weak var lblCompositionTax: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if lblPaymentStatus.text?.uppercased() == "NOT PAID" {
            lblPaymentStatus.textColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1)
            lblPaymentStatus.layer.borderColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1).cgColor
        } else if lblPaymentStatus.text?.uppercased() == "PAID" {
            lblPaymentStatus.textColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1)
            lblPaymentStatus.layer.borderColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        } else if lblPaymentStatus.text?.uppercased() == "DELIVERED" {
            lblPaymentStatus.textColor  = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1)
            lblPaymentStatus.layer.borderColor = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1).cgColor
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
