//
//  CouponsCell.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 26/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class CouponsCell: UITableViewCell {

    @IBOutlet weak var imgHotPrice: UIImageView!
    @IBOutlet weak var lblCouponCode: UILabel!
    @IBOutlet weak var btnApplyNow: UIButton!
    @IBOutlet weak var lblCouponDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnApplyNowPressed(_ sender: UIButton) {
        
    }
    
    func updateUI(coupon: Coupons) {
        self.lblCouponCode.text = coupon.couponCode
        self.lblCouponDescription.text = coupon.couponDescription
    }
}
