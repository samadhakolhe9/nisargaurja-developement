//
//  MyOrdersTableViewCell.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 13/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class MyOrdersTableViewCell: UITableViewCell {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblDelivery: UILabel!
    @IBOutlet weak var lblProductDetails: UILabel!
    @IBOutlet weak var lblDeliveryDate: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblPaymentDeliveryStatus: UILabel!
    @IBOutlet weak var lblPayementStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        outerView.applyCommonShadow()
        lblPaymentDeliveryStatus.layer.borderWidth = 0.5
        lblPaymentDeliveryStatus.layer.cornerRadius = 5
        lblPaymentDeliveryStatus.layer.masksToBounds = false
        lblPaymentDeliveryStatus.clipsToBounds = true
        
        lblPayementStatus.layer.borderWidth = 0.5
        lblPayementStatus.layer.cornerRadius = 5
        lblPayementStatus.layer.masksToBounds = false
        lblPayementStatus.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
