//
//  CategoryCell.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 04/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgFruits: UIImageView!
    @IBOutlet weak var lblFruitName: UILabel!
    @IBOutlet weak var lblOfferPercentage: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var percentageView: UIView!
    @IBOutlet weak var shadowView: UIView!
    
    override func awakeFromNib() {
//        bgView.layer.cornerRadius = 5
//        bgView.layer.masksToBounds = false
//        bgView.layer.shadowRadius = 5
//        bgView.layer.shadowOpacity = 1
//        bgView.layer.shadowColor = UIColor.lightGray.cgColor
//        bgView.layer.shadowOffset = CGSize(width: 0 , height:2)
//        bgView.backgroundColor = .white
    }
}
