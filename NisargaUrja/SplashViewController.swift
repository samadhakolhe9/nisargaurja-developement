//
//  ViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 01/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit
var globalUserPhone = ""

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            globalUserPhone = LocalDB.shared.getUserMobileNumber() //"8446628681" //
            if globalUserPhone != "" {
                let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "idLeftViewController") as! LeftViewController
                let slideMenuController = ExSlideMenuController(mainViewController:setupReferenceUITabBarController(), leftMenuViewController: leftViewController)
                self.navigationController?.push(viewController: slideMenuController)
            } else {
                let loginCV = self.storyboard?.instantiateViewController(withIdentifier: "idLoginViewController") as! LoginViewController
                self.navigationController?.push(viewController: loginCV)
            }
        }
    }


}

