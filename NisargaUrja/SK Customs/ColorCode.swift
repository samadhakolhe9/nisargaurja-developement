
import Foundation
import UIKit


//set common color here to use whole project
public var AppCommonColor          = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1)
public var ImageIconTintColor      = UIColor.white
public var MenuButtonColor         = UIColor.white
public var lightGrayColor          = UIColor.lightGray
public var AppCommonBGColor        = UIColor ( red: 0.224, green: 0.227, blue: 0.231, alpha: 1.0 )
