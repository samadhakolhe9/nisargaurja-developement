//
//  RecipeDetailsViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 23/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class RecipeDetailsViewController: UITableViewController {

    @IBOutlet weak var lblRecipeName: UILabel!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var btnYoutube: UIButton!
    @IBOutlet weak var btnAuthorName: UIButton!
    @IBOutlet weak var lblIngredients: UILabel!
    @IBOutlet weak var lblSteps: UILabel!
    @IBOutlet weak var lblRelatedProducts: UILabel!
    @IBOutlet weak var relatedProductsCollectionView: UICollectionView!
    @IBOutlet var StretchyHeaderView: StretchyTableHeaderView!
    
    var RecipeDetailsArray:[RecipeDetails] = []
    struct RecipeDetails {
        var recipeUID: String!
        var recipeTitle: String!
        var recipeDescription: [String]!
        var recipeImageURL: String!
        var recipeInstructions: String!
        var recipeDate: String!
        var recipeLink: String!
        var ingredientTitle: [String]!
        var ingredientQuantity: [String]!
        var ingredientUnit: [String]!
        var authorUID: String!
        var authorBrandName: String!
        var favouriteOf: Bool!
    }
    var RecipeUID = ""
    let backButton = UIButton(type: .custom)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //   self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        backButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backButton.setImage(UIImage(named: "backArrow")!.imageWithColor(color: .white
        ), for: UIControl.State.normal)
        backButton.addTarget(self, action: #selector(self.backButtonAction), for: UIControl.Event.touchUpInside)
        backButton.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        backButton.contentMode = .center
        let back = UIBarButtonItem(customView: backButton)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.leftBarButtonItems = [back]
        
        self.navigationController?.navigationBar.setNavbar(backgroundColorAlpha: 0.0, newColor: .white)
    }
    
    @objc func backButtonAction(_ sender: UIButton) {
        self.navigationController?.navigationBar.setNavbar(backgroundColorAlpha: 1.0, newColor: .white)
        self.navigationController?.popViewController(animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        tableView.separatorStyle = .none
        setupCollectionView(collectionView: relatedProductsCollectionView, itemSpacing: 5)
        
        tableView.register(UINib(nibName: "RecipeIngredientsCell", bundle: nil), forCellReuseIdentifier: "IngredientsCell")
        tableView.register(UINib(nibName: "RecipeStepsCell", bundle: nil), forCellReuseIdentifier: "StepsCell")
        
        self.tableView.estimatedRowHeight = 40
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.tableFooterView = UIView()
        
        StretchyHeaderView = StretchyTableHeaderView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 200))
        StretchyHeaderView.imageView.alpha = 0.3
        StretchyHeaderView.imageView.image = UIImage(named: "splash_logo")
        self.tableView.tableHeaderView = StretchyHeaderView
        
        self.tableView.reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getRecipeDetails()
        self.navigationController?.navigationBar.setNavbar(backgroundColorAlpha: 0.0, newColor: .white)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let navigationBar = navigationController!.navigationBar
        navigationBar.reset()
        
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.navigationBar.setNavbar(backgroundColorAlpha: 1.0, newColor: .white)
    }
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let headerView = self.tableView.tableHeaderView as! StretchyTableHeaderView
        headerView.scrollViewDidScroll(scrollView: scrollView)
        let denominator: CGFloat = 50 //your offset treshold
        let alpha = min(1, scrollView.contentOffset.y / denominator)
        
        backButton.setImage(UIImage(named: "backArrow")!.imageWithColor(color: alpha > 0.8 ? .black : .white), for: UIControl.State.normal)
        self.title = alpha > 0.8 ? "Recipe details" : ""
        self.navigationController?.navigationBar.setNavbar(backgroundColorAlpha: alpha, newColor: .white)
    }
    
    func setupCollectionView(collectionView: UICollectionView, itemSpacing: CGFloat) {
           let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
           collectionView.collectionViewLayout = layout
           collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
           
            let width = collectionView.frame.size.width
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
               layout.minimumInteritemSpacing = 5
               layout.minimumLineSpacing = 5
               layout.itemSize = CGSize(width: width/3 - 5, height: width/2)
               layout.invalidateLayout()
           }
    }
    
    @IBAction func btnAuthorNamePressed(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "idAuthorDetailsViewController") as! AuthorDetailsViewController
        controller.AuthorUID = RecipeDetailsArray[0].authorUID
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnFavoritesPressed(_ sender: UIButton) {
        let urlString = APIConstants.toggleFavouriteRecipe+"recipeUID=\(RecipeUID)&phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    self.btnFavorite.isSelected = fieldsArray[KeysArray["status"].intValue].boolValue
                }
            }
        }
    }
    
    @IBAction func btnYoutubePressed(_ sender: UIButton) {
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func setAllDetails() {
        if RecipeDetailsArray.count > 0 {
            lblRecipeName.text = RecipeDetailsArray[0].recipeTitle
            self.btnFavorite.setImage(UIImage(named: "empty-heart")?.imageWithColor(color: .red), for: .normal)
            self.btnFavorite.setImage(UIImage(named: "heart")?.imageWithColor(color: .red), for: .selected)
            self.btnFavorite.isSelected = RecipeDetailsArray[0].favouriteOf
            btnAuthorName.setTitle("By "+RecipeDetailsArray[0].authorBrandName, for: .normal)

            let imagePath = RecipeDetailsArray[0].recipeImageURL
            let imagePathString = imagePath!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: imagePathString)!
            let resource = ImageResource(downloadURL: url)
            
            KingfisherManager.shared.retrieveImage(with: resource,  options: nil) { result in
                switch result {
                case .success(let value):
                    self.StretchyHeaderView.imageView.image = value.image
                    self.StretchyHeaderView.imageView.alpha = 1.0
                    //self.tableView.addScalableCover(with: value.image, maxHeight: 100)
                case .failure( _):
                    self.StretchyHeaderView.imageView.alpha = 0.3
                    self.StretchyHeaderView.imageView.image = UIImage(named: "splash_logo")
                }
            }
        }
        tableView.reloadData()
    }
    
    func getRecipeDetails() {
        let urlString = APIConstants.getRecipeDetails+"recipeUID=\(RecipeUID)&phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.RecipeDetailsArray = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    
                    let commonProduct = Response.filter{$0.fields[$0.fieldLookup["recipeUID"].intValue].stringValue == fieldsArray[KeysArray["recipeUID"].intValue].stringValue}
                    
                    self.RecipeDetailsArray.append(RecipeDetails(recipeUID: fieldsArray[KeysArray["recipeUID"].intValue].stringValue, recipeTitle: fieldsArray[KeysArray["recipeTitle"].intValue].stringValue, recipeDescription: [fieldsArray[KeysArray["recipeDescription"].intValue].stringValue], recipeImageURL: fieldsArray[KeysArray["recipeImageURL"].intValue].stringValue, recipeInstructions: fieldsArray[KeysArray["recipeInstructions"].intValue].stringValue, recipeDate: fieldsArray[KeysArray["recipeDate"].intValue].stringValue, recipeLink: fieldsArray[KeysArray["recipeLink"].intValue].stringValue, ingredientTitle: commonProduct.map{$0.fields[$0.fieldLookup["ingredientTitle"].intValue].stringValue}, ingredientQuantity: commonProduct.map{$0.fields[$0.fieldLookup["ingredientQuantity"].intValue].stringValue}, ingredientUnit: commonProduct.map{$0.fields[$0.fieldLookup["ingredientUnit"].intValue].stringValue},authorUID: fieldsArray[KeysArray["authorUID"].intValue].stringValue, authorBrandName: fieldsArray[KeysArray["authorBrandName"].intValue].stringValue, favouriteOf: fieldsArray[KeysArray["favouriteOf"].intValue].boolValue))
                }
                self.RecipeDetailsArray = self.RecipeDetailsArray.filterDuplicates{$0.recipeUID == $1.recipeUID}
                print("Recipe details data: ", self.RecipeDetailsArray)
                self.setAllDetails()
            }
            self.tableView.reloadData()
        }
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if RecipeDetailsArray.count == 0 {
            return 0
        }
        return section == 1 ? self.RecipeDetailsArray[0].ingredientTitle.count+1: section == 2 ? RecipeDetailsArray[0].recipeDescription.count+1 : super.tableView(tableView, numberOfRowsInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 && indexPath.row > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "IngredientsCell", for: indexPath) as! RecipeIngredientsCell
            let newIndex = indexPath.row-1
            cell.lblIngredientName.text = RecipeDetailsArray[0].ingredientTitle[newIndex]
            cell.lblIngredientQty.text = RecipeDetailsArray[0].ingredientQuantity[newIndex] + " " + RecipeDetailsArray[0].ingredientUnit[newIndex]
            return cell
        } else if indexPath.section == 2 && indexPath.row > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StepsCell", for: indexPath) as! RecipeStepsCell
            let newIndex = indexPath.row-1
            cell.lblStepDescription.text = RecipeDetailsArray[0].recipeDescription[newIndex]
            return cell
        }
        return super.tableView(tableView, cellForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
           if indexPath.section == 1 && indexPath.row > 0 {
               let newIndexPath = IndexPath(row: 0, section: indexPath.section)
               return super.tableView(tableView, indentationLevelForRowAt: newIndexPath)
           }
           if indexPath.section == 2 && indexPath.row > 0 {
               let newIndexPath = IndexPath(row: 0, section: indexPath.section)
               return super.tableView(tableView, indentationLevelForRowAt: newIndexPath)
           }
           return super.tableView(tableView, indentationLevelForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return [1,2].contains(indexPath.section) && indexPath.row > 0 ? UITableView.automaticDimension : indexPath.section == 3 ? 0 : super.tableView(tableView, heightForRowAt: indexPath)
    }
}

extension RecipeDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RelatedRecipesCell
            
            return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var cellSize: CGSize = collectionView.bounds.size
        cellSize.width = cellSize.width/3-5
        
        return CGSize(width: cellSize.width, height: cellSize.height)
    }
    
}
