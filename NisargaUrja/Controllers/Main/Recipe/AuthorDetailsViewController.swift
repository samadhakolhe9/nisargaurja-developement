//
//  AuthorDetailsViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 23/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class AuthorDetailsViewController: UITableViewController {

    @IBOutlet weak var imgAuthor: UIImageView!
    @IBOutlet weak var lblAuthorName: UILabel!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnInstagram: UIButton!
    @IBOutlet weak var btnYoutube: UIButton!
    @IBOutlet weak var lblAuthorDetailURL: UILabel!
    @IBOutlet weak var lblAuthorIntroduction: UILabel!
    @IBOutlet weak var lblAllRecipes: UILabel!
    @IBOutlet weak var collectionAllRecipes: UICollectionView!
    @IBOutlet weak var lblRelatedRecipes: UILabel!
    var AuthorUID = ""
    
    
    var AuthorDetailsData:AuthorDetails!
    struct AuthorDetails {
        var authorName: String!
        var authorDescription: String!
        var authorBrandName: String!
        var authorImageURL: String!
        var authorWebsite: String!
        var authorInstagram: String!
        var authorFacebook: String!
        var authorYoutube: String!
        var recipeUID: String!
        var recipeTitle: String!
        var recipeImageURL: String!
        var recipeFavouriteOf: String!
        var authorFavouriteOf: String!
    }
    var RelatedRecipeArray:[RelatedRecipe] = []
    struct RelatedRecipe {
        var recipeUID: String!
        var recipeTitle: String!
        var recipeImageURL: String!
        var authorBrandName: String!
        var isFavourite: Bool!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.title = "Author"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        imgAuthor.layer.cornerRadius = imgAuthor.frame.size.width / 2
//        imgAuthor.clipsToBounds = true
        imgAuthor.applyCircleShadow()
        setupCollectionView(collectionView: collectionAllRecipes, itemSpacing: 5)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getAuthorDetails()
        getAllRecipes()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let navigationBar = navigationController!.navigationBar
        navigationBar.reset()
    }
    
    func setupCollectionView(collectionView: UICollectionView, itemSpacing: CGFloat) {
           let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
           collectionView.collectionViewLayout = layout
           collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
           
            let width = collectionView.frame.size.width
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
               layout.minimumInteritemSpacing = 5
               layout.minimumLineSpacing = 5
               layout.itemSize = CGSize(width: width/3 - 5, height: width/2)
               layout.invalidateLayout()
           }
    }
    
    @IBAction func FacebookButtonAction(_ sender: UIButton) {
        guard let url = URL(string: AuthorDetailsData.authorFacebook) else {
          return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func InstagramButtonAction(_ sender: UIButton) {
        guard let url = URL(string: AuthorDetailsData.authorInstagram) else {
          return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func YoutubeButtonAction(_ sender: UIButton) {
        guard let url = URL(string: AuthorDetailsData.authorYoutube) else {
          return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    func setUIDetails() {

        let imagePath = AuthorDetailsData.authorImageURL
        let imagePathString = imagePath!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let url = URL(string: imagePathString)!
        let resource = ImageResource(downloadURL: url)
        
        KingfisherManager.shared.retrieveImage(with: resource,  options: nil) { result in
            switch result {
            case .success(let value):
                self.imgAuthor.image = value.image
                //self.tableView.addScalableCover(with: value.image, maxHeight: 100)
            case .failure( _):
                self.imgAuthor.image = UIImage(named: "splash_logo")
            }
        }
        self.title = AuthorDetailsData.authorName
        lblAuthorName.text = AuthorDetailsData.authorName
        lblAuthorDetailURL.text = AuthorDetailsData.authorWebsite
        lblAuthorIntroduction.text = AuthorDetailsData.authorDescription
        
//        btnFacebook: UIButton!
//        btnInstagram: UIButton!
//        btnYoutube: UIButton!
    }
    
    func getAuthorDetails() {
        let urlString = APIConstants.getAuthorDetails+"authorUID=\(AuthorUID)&phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.AuthorDetailsData = nil
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    
                    self.AuthorDetailsData = AuthorDetails(authorName: fieldsArray[KeysArray["authorName"].intValue].stringValue, authorDescription: fieldsArray[KeysArray["authorDescription"].intValue].stringValue, authorBrandName: fieldsArray[KeysArray["authorBrandName"].intValue].stringValue, authorImageURL: fieldsArray[KeysArray["authorImageURL"].intValue].stringValue, authorWebsite: fieldsArray[KeysArray["authorWebsite"].intValue].stringValue, authorInstagram: fieldsArray[KeysArray["authorInstagram"].intValue].stringValue, authorFacebook: fieldsArray[KeysArray["authorFacebook"].intValue].stringValue, authorYoutube: fieldsArray[KeysArray["authorYoutube"].intValue].stringValue, recipeUID: fieldsArray[KeysArray["recipeUID"].intValue].stringValue, recipeTitle: fieldsArray[KeysArray["recipeTitle"].intValue].stringValue, recipeImageURL: fieldsArray[KeysArray["recipeImageURL"].intValue].stringValue, recipeFavouriteOf: fieldsArray[KeysArray["recipeFavouriteOf"].intValue].stringValue, authorFavouriteOf: fieldsArray[KeysArray["authorFavouriteOf"].intValue].stringValue)
                }
                self.setUIDetails()
            }
        }
    }
    func getAllRecipes() {
           let urlString = APIConstants.getAllRecipes+"phoneNo=\(globalUserPhone)"
           ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
               self.RelatedRecipeArray = []
               if Response.count > 0 {
                   self.lblRelatedRecipes.text = "Related Recipes"
                   self.lblRelatedRecipes.font = UIFont.init(name: "Lato-Regular", size: 18.0)
                   for item in Response {
                       let KeysArray = item.fieldLookup
                       let fieldsArray = item.fields
                       
                       self.RelatedRecipeArray.append(RelatedRecipe(recipeUID: fieldsArray[KeysArray["recipeUID"].intValue].stringValue, recipeTitle: fieldsArray[KeysArray["recipeTitle"].intValue].stringValue, recipeImageURL: fieldsArray[KeysArray["recipeImageURL"].intValue].stringValue, authorBrandName: fieldsArray[KeysArray["authorBrandName"].intValue].stringValue, isFavourite: fieldsArray[KeysArray["isFavourite"].intValue].boolValue))
                   }
               } else {
                   self.lblRelatedRecipes.font = UIFont.init(name: "Lato-Regular", size: 14.0)
                   self.lblRelatedRecipes.text = "No more related recipes"
               }
               self.collectionAllRecipes.reloadData()
               self.tableView.reloadData()
           }
       }
}

extension AuthorDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return RelatedRecipeArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RelatedRecipesCell
            
              cell.lblProductName.text = RelatedRecipeArray[indexPath.row].recipeTitle
              cell.lblOrganizeBy.text = RelatedRecipeArray[indexPath.row].authorBrandName
              cell.btnFavorites.isSelected = RelatedRecipeArray[indexPath.row].isFavourite
              
              cell.btnFavorites.setImage(UIImage(named: "empty-heart")?.imageWithColor(color: .red), for: .normal)
              cell.btnFavorites.setImage(UIImage(named: "heart")?.imageWithColor(color: .red), for: .selected)
              
              let imagePath = RelatedRecipeArray[indexPath.row].recipeImageURL
              let imagePathString = imagePath!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
              let url = URL(string: imagePathString)!
              let resource = ImageResource(downloadURL: url)

              cell.imgProduct.image = UIImage(named: "splash_logo")
              cell.imgProduct.alpha = 0.3
              cell.activityIndicator.isHidden = false
              cell.activityIndicator.startAnimating()
              KingfisherManager.shared.retrieveImage(with: resource,  options: nil) { result in
                  cell.activityIndicator.isHidden = true
                  cell.activityIndicator.stopAnimating()
                switch result {
                  case .success(let value):
                      cell.imgProduct.image = value.image
                      cell.imgProduct.alpha = 1.0
                  case .failure(let error):
                      print("Error: \(error)")
                      cell.imgProduct.image = UIImage(named: "splash_logo")
                      cell.imgProduct.alpha = 0.3
                }
              }
              cell.imgProduct.contentMode = .scaleAspectFill //scaleToFill //
              cell.imgProduct.clipsToBounds = true
              return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        var cellSize: CGSize = collectionView.bounds.size
        cellSize.width = cellSize.width/3-5
        return CGSize(width: cellSize.width, height: cellSize.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "idRecipeDetailsViewController") as! RecipeDetailsViewController
        controller.RecipeUID = RelatedRecipeArray[indexPath.row].recipeUID
        self.navigationController?.pushViewController(controller, animated: true)
    }
}



