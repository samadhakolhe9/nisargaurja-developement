//
//  ChangeShippingAddressVC.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 12/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class ChangeShippingAddressVC: UIViewController {

    @IBOutlet weak var addressTableView: UITableView!
    @IBOutlet weak var btnAddNewAddress: UIButton!
    
    var selectedIndex: Int?
    var userAddresses: [UserAddresses] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // self.addLeftButtonWithImage(UIImage(named: "backArrow")!)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.title = "Change Shipping Address"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addressTableView.delegate = self
        addressTableView.dataSource = self
        //addressTableView.maxHeight = 400
        btnAddNewAddress.layer.cornerRadius = 5
        btnAddNewAddress.clipsToBounds = true
        self.addressTableView.separatorStyle = UITableViewCell.SeparatorStyle.none

        getAllAddresses()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getAllAddresses()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
    }
    
    @IBAction func btnAddNewAddressPressed(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "idSetDeliveryLocationController") as! SetDeliveryLocationController
        controller.isFromRegistration = false
        controller.isChangeAddress = false
        
        userRegisterParams["phoneNo"] = LocalDB.shared.UserInfo.phoneNo
        userRegisterParams["firstName"] = LocalDB.shared.UserInfo.firstName
        userRegisterParams["lastName"] = LocalDB.shared.UserInfo.lastName
        userRegisterParams["title"] = ""
        userRegisterParams["address"] = ""
        userRegisterParams["house"] = ""
        userRegisterParams["pincode"] = ""
        userRegisterParams["lat"] = ""
        userRegisterParams["lng"] = ""
        userRegisterParams["isDeliverable"] = ""
            
            setupReferenceUITabBarController().navigationController?.pushViewController(controller, animated: true)
    }
    
    //TODO:: Get All Addresses
    func getAllAddresses() {
        let urlString = APIConstants.getAddresses+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
            self.userAddresses = []
            if responseData.count > 0 {
                responseData.forEach { (data) in
                    self.userAddresses.append(UserAddresses(addressUID: data.fields[0].stringValue, title: data.fields[1].stringValue, houseNo: data.fields[2].stringValue, address: data.fields[3].stringValue, pincode: data.fields[4].stringValue, isDeliverable: data.fields[7].boolValue, isBillingAddress: data.fields[8].boolValue, isDefaultAddress: data.fields[9].boolValue))
                }
            }
            self.addressTableView.reloadData()
        }
    }
}

//TODO:: TableView Delegate and DataSource Methods
extension ChangeShippingAddressVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userAddresses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "idShippingAddressCell") as? ChangeShippingAddressTableViewCell else {
            return UITableViewCell()
        }
        
        cell.btnAddressTitle.isSelected = userAddresses[indexPath.row].title != "Home"
        cell.btnAddressTitle.setTitle(userAddresses[indexPath.row].title, for: .normal)
        cell.btnAddressTitle.setTitle(userAddresses[indexPath.row].title, for: .selected)
        cell.btnServiceStatus.isSelected = userAddresses[indexPath.row].isDeliverable!
        cell.lblAddress.text = userAddresses[indexPath.row].address
        
        cell.radioButton.isSelected = (DefaultAddressArray != nil && DefaultAddressArray.addressUID == userAddresses[indexPath.row].addressUID) || selectedIndex == indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let addressUID = userAddresses[indexPath.row].addressUID
        let urlString = APIConstants.setasdefaultAddress+"phoneNo=\(globalUserPhone)&addressUID=\(addressUID!)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.getUserDefaultAddress()
            self.navigationController?.popViewController(animated: true)
            ReusableManager.sharedInstance.showAlert(alertString: "Your shipping address changed successfully.")
        }
    }
    
    func getUserDefaultAddress() {
        let urlString = APIConstants.getDefaultAddress+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (Response, success) in
            if Response.count > 0 {
                let KeysArray = Response[0].fieldLookup
                let fieldsArray = Response[0].fields
                defaultAddress = fieldsArray[KeysArray["address"].intValue].stringValue
                DefaultAddressArray = Address(addressUID: fieldsArray[KeysArray["addressUID"].intValue].stringValue, title: fieldsArray[KeysArray["title"].intValue].stringValue, house: fieldsArray[KeysArray["house"].intValue].stringValue, address: fieldsArray[KeysArray["address"].intValue].stringValue, pincode: fieldsArray[KeysArray["pincode"].intValue].stringValue, latitude: fieldsArray[KeysArray["latitude"].intValue].stringValue, longitude: fieldsArray[KeysArray["longitude"].intValue].stringValue, isDeliverable: fieldsArray[KeysArray["isDeliverable"].intValue].boolValue, isBillingAddress: fieldsArray[KeysArray["isBillingAddress"].intValue].boolValue, isDefaultAddress: fieldsArray[KeysArray["isDefaultAddress"].intValue].boolValue)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension
        } else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            let vw = UIView()
        vw.backgroundColor = UIColor.white
        btnAddNewAddress.frame = CGRect(x:10,y: 10 ,width:200,height:30)
        vw.addSubview(btnAddNewAddress)
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
}
