//
//  HomeViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 03/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

var defaultAddressTitle = ""
var defaultAddressText = ""

//Gobal Declaration
var BannersArray:[Banner] = []
struct Banner {
    var bannerTarget: String!
    var bannerValue: String!
    var bannerURL: String!
    var bannerPriority: String!
}

var upcommingOrdersArray:[Upcoming] = []
struct Upcoming {
    var orderUID: String!
    var deliveryDate: String!
    var orderTotal: String!
    var orderStatus: String!
    var deliveryStatus: String!
    var productName: [String]!
    var variationTitle: [String]!
    var quantityPurchased: [String]!
}

var DefaultAddressArray:Address!
struct Address {
    var addressUID: String!
    var title: String!
    var house: String!
    var address: String!
    var pincode: String!
    var latitude: String!
    var longitude: String!
    var isDeliverable: Bool!
    var isBillingAddress: Bool!
    var isDefaultAddress: Bool!
}

var MyFavoriteArray:[Favorite] = []
struct Favorite {
    var productUID: String!
    var productName: String!
    var productPackageImage: String!
    var brandName: String!
}

var CategoryArray:[Category] = []
struct Category {
    var c_UID: String!
    var categoryName: String!
    var categoryPackageImage: String!
    var categoryPriority: String!
    var uptoDiscount: Double!
}

var CouponArray:[Coupon] = []
struct Coupon {
    var couponUID: String!
    var couponName: String!
    var couponDescription: String!
    var couponCode: String!
}

var RecipeArray:[Recipes] = []
struct Recipes {
    var r_UID: String!
    var recipeTitle: String!
    var recipeImageURL: String!
    var brandName: String!
}

var BrandsArray:[Brands] = []
struct Brands {
    var brandUID: String!
    var brandLogo: String!
    var brandName: String!
    var uptoDiscount: Double!
}

var TopSellingArray:[TopSelling] = []
struct TopSelling {
    var productUID: String!
    var productName: String!
    var productPackageImage: String!
    var prodCount: String!
    var isFavourite: Bool!
    var brandName: String!
}

var TestimonialsArray:[Testimonials] = []
struct Testimonials {
    var testimonialUID: String!
    var testimonial: String!
    var firstName: String!
    var lastName: String!
    var testimonialDate: String!
}

class TestimonialsCell: UICollectionViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTestimonialDate: UILabel!
    @IBOutlet weak var lblTestimonial: UILabel!
    
    override func awakeFromNib() {
    }
}

class HomeViewController: UITableViewController {

    @IBOutlet weak var categoryCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblDeliverAddress: UILabel!
    @IBOutlet weak var bannerBGView: UIView!
    @IBOutlet weak var upcomingView: UIView!
    @IBOutlet weak var btnShowMoreFavorite: UIButton!
    @IBOutlet weak var btnShowMoreRecipe: UIButton!
    @IBOutlet weak var favoritesCollectionView: UICollectionView!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var yourFavoriteCollectionView: UICollectionView!
    @IBOutlet weak var brandsCollectionView: UICollectionView!
    @IBOutlet weak var topSellingCollectionView: UICollectionView!
    @IBOutlet weak var couponCollectionView: UICollectionView!
    @IBOutlet weak var couponPageControl: UIPageControl!
    @IBOutlet weak var testimonialsCollectionView: UICollectionView!
    @IBOutlet weak var testimonialsPageControl: UIPageControl!
    @IBOutlet weak var btnPrevTestimonial: UIButton!
    @IBOutlet weak var btnNextTestimonial: UIButton!
    @IBOutlet weak var lblTestimonialHeader: UILabel!
    @IBOutlet weak var labelUpcomingOrder: UILabel!
    
    
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.itemSize = FSPagerView.automaticSize
        }
    }
    
    @IBOutlet weak var pageControl: FSPageControl! {
        didSet {
            self.pageControl.numberOfPages = 2
            self.pageControl.contentHorizontalAlignment = .center
            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
    }
    
    //UPCOMING ORDER
    @IBOutlet weak var lblItemSummary: UILabel!
    @IBOutlet weak var lblItemTotal: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblNoUpcomingOrder: UILabel!
    @IBOutlet weak var labelSUmmary: UILabel!
    @IBOutlet weak var labelDelivery: UILabel!
    
    var isLoader = false
    var isAvailableOrder = false
    var numberOfItems = 5
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        testimonialsPageControl.customizePageControl(dotFillColor: .black, dotBorderColor: .black, dotBorderWidth: 1)
        testimonialsCollectionView.isPagingEnabled = true
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "app_icon")
        imageView.image = image
        navigationItem.titleView = imageView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CartRightButton.badgeEdgeInsets = UIEdgeInsets(top: 14, left: 6, bottom: 0, right: 0)
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
        lblDeliverAddress.textAlignment = .left

        self.btnCheckBox.isHidden = true
        pagerConfiguration()
        
        pagerView.backgroundView?.backgroundColor = .clear
        pagerView.backgroundColor = .clear
        
        setupCollectionView(collectionView: categoryCollectionView, itemSpacing: 5, isVertical: true)
        setupCollectionView(collectionView: favoritesCollectionView, itemSpacing: 5, isVertical: false)
        setupCollectionView(collectionView: yourFavoriteCollectionView, itemSpacing: 5, isVertical: false)
        setupCollectionView(collectionView: brandsCollectionView, itemSpacing: 0, isVertical: false)
        setupCollectionView(collectionView: topSellingCollectionView, itemSpacing: 5, isVertical: false)
        setupCollectionView(collectionView: testimonialsCollectionView, itemSpacing: 0, isVertical: false)
        lblDeliverAddress.text = ""
        isLoader = true
        
        btnPrevTestimonial.applyCircleShadow()
        btnPrevTestimonial.layer.cornerRadius = btnPrevTestimonial.layer.frame.height / 2
        btnPrevTestimonial.clipsToBounds = true
        
        btnNextTestimonial.applyCircleShadow()
        btnNextTestimonial.layer.cornerRadius = btnPrevTestimonial.layer.frame.height / 2
        btnNextTestimonial.clipsToBounds = true
        
        self.refresher = UIRefreshControl()
        self.tableView!.alwaysBounceVertical = true
        self.refresher.tintColor = AppCommonColor
        self.refresher.attributedTitle = NSAttributedString(string: "Pull to refresh...", attributes: [NSAttributedString.Key.foregroundColor: AppCommonColor])
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.tableView!.addSubview(refresher)
    }
    
    func layoutSubviews() {
        self.categoryCollectionViewHeightConstraint.constant = self.categoryCollectionView.collectionViewLayout.collectionViewContentSize.height
    }
    
    var refresher:UIRefreshControl!
    @objc func loadData() {
        isLoader = true
        getUserDefaultAddress()
    }
    
    func pagerConfiguration() {
        self.pagerView.automaticSlidingInterval = 3.0 - self.pagerView.automaticSlidingInterval
        self.pagerView.decelerationDistance = FSPagerView.automaticDistance
        self.pagerView.interitemSpacing = 0
        self.pagerView.frame.size.applying(CGAffineTransform(scaleX: 1, y: 1))
    }
    
    func setDefaultAddress(title: String, address: String) {
        defaultAddressTitle = title
        defaultAddressText = address
        let key = NSMutableAttributedString(string: "\(defaultAddressTitle):", attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
        let value = NSMutableAttributedString(string: " \(defaultAddressText)", attributes: [:])
        key.append(value)
        lblDeliverAddress.attributedText = key
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setAPNToken()
        getUserDInfo()
        getUserDefaultAddress()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        isLoader = false
    }
    
    func setupCollectionView(collectionView: UICollectionView, itemSpacing: CGFloat, isVertical: Bool) {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = isVertical ? .vertical : .horizontal
        collectionView.collectionViewLayout = layout
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
         let width = collectionView.frame.size.width
         if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumInteritemSpacing = itemSpacing
            layout.minimumLineSpacing = itemSpacing
            layout.itemSize = CGSize(width: width/3 - itemSpacing, height: width)
            layout.invalidateLayout()
        }
    }

    @IBAction func MoreButtonAction(_ sender: UIButton) {
        if sender.tag == 0 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "idMyFavoritesViewController") as! MyFavoritesViewController
            self.navigationController?.pushViewController(controller, animated: true)
        } else if sender.tag == 1 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "idAllRecipesViewController") as! AllRecipesViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func LocationButtonAction(_ sender: UIButton) {
    }
    
    @IBAction func CheckButtonAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "idChangeShippingAddressVC") as! ChangeShippingAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnNextTestimonialPressed(_ sender: UIButton) {
        let nextIndex = min(testimonialsPageControl.currentPage + 1, TestimonialsArray.count - 1)
        let indexPath = IndexPath(item: nextIndex, section: 0)
        testimonialsPageControl.currentPage = nextIndex
        testimonialsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func btnPreviousTestimonialPressed(_ sender: UIButton) {
        let nextIndex = max(testimonialsPageControl.currentPage - 1, 0)
        let indexPath = IndexPath(item: nextIndex, section: 0)
        testimonialsPageControl.currentPage = nextIndex
        testimonialsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 13 {
            return 180 //brandsCollectionView.bounds.width/3
        } else if [3].contains(indexPath.row) {
           return (isAvailableOrder ? UITableView.automaticDimension : 0)
        } else if [4,5].contains(indexPath.row)  {
            return (CategoryArray.count > 0 ? UITableView.automaticDimension : 0)
        } else if [6,7].contains(indexPath.row)  {
            return (MyFavoriteArray.count > 0 ? super.tableView(tableView, heightForRowAt: indexPath) : 0)
        } else if [8,9].contains(indexPath.row)  {
            return (CouponArray.count > 0 ? super.tableView(tableView, heightForRowAt: indexPath) : 0)
        } else if [10,11].contains(indexPath.row)  {
            return (RecipeArray.count > 0 ? super.tableView(tableView, heightForRowAt: indexPath) : 0)
        } else if [12,13].contains(indexPath.row)  {
            return (BrandsArray.count > 0 ? super.tableView(tableView, heightForRowAt: indexPath) : 0)
        } else if [14,15].contains(indexPath.row)  {
            return (TopSellingArray.count > 0 ? super.tableView(tableView, heightForRowAt: indexPath) : 0)
        } else if [16,17].contains(indexPath.row)  {
            return (TestimonialsArray.count > 0 ? super.tableView(tableView, heightForRowAt: indexPath) : 0)
        }

        return indexPath.row == 0 ? 45 : super.tableView(tableView, heightForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "idChangeShippingAddressVC") as! ChangeShippingAddressVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.row == 3 && upcommingOrdersArray.count > 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "idInvoiceViewController") as! InvoiceViewController
            vc.title = "Order Detail"
            vc.selectedOrderUID = upcommingOrdersArray[0].orderUID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == favoritesCollectionView {
            return MyFavoriteArray.count
        } else if collectionView == categoryCollectionView {
            return CategoryArray.count
        } else if collectionView == couponCollectionView {
            return CouponArray.count
        } else if collectionView == yourFavoriteCollectionView {
            return RecipeArray.count
        } else if collectionView == brandsCollectionView {
            return BrandsArray.count
        } else if collectionView == topSellingCollectionView {
            return TopSellingArray.count
        } else if collectionView == testimonialsCollectionView {
            return TestimonialsArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == favoritesCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FavoriteCell
            cell.lblFruitName.text = MyFavoriteArray[indexPath.row].productName
            cell.lblOrganizeBy.text = MyFavoriteArray[indexPath.row].brandName
            cell.btnFavorite.isSelected = true
            
            cell.btnFavorite.setImage(UIImage(named: "empty-heart")?.imageWithColor(color: .red), for: .normal)
            cell.btnFavorite.setImage(UIImage(named: "heart")?.imageWithColor(color: .red), for: .selected)
            let imagePath = MyFavoriteArray[indexPath.row].productPackageImage
            let imagePathString = imagePath!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: imagePathString)!
            let resource = ImageResource(downloadURL: url)

            cell.imgFruits.image = UIImage(named: "splash_logo")
            cell.imgFruits.alpha = 0.3
            cell.activityIndicator.isHidden = false
            cell.activityIndicator.startAnimating()
            KingfisherManager.shared.retrieveImage(with: resource,  options: nil) { result in
                cell.activityIndicator.isHidden = true
                cell.activityIndicator.stopAnimating()
              switch result {
                case .success(let value):
                    cell.imgFruits.image = value.image
                    cell.imgFruits.alpha = 1.0
                case .failure(let error):
                    print("Error: \(error)")
                    cell.imgFruits.image = UIImage(named: "splash_logo")
                    cell.imgFruits.alpha = 0.3
              }
              cell.imgFruits.contentMode = .scaleAspectFill //scaleToFill //
              cell.imgFruits.clipsToBounds = true
              cell.imgFruits.roundCorners(corners: [.topLeft, .topRight], radius: 5)
            }
            return cell
        } else if collectionView == categoryCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCell
            cell.lblFruitName.text = CategoryArray[indexPath.row].categoryName
            
            let imagePath = CategoryArray[indexPath.row].categoryPackageImage
            let imagePathString = imagePath!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: imagePathString)!
            let resource = ImageResource(downloadURL: url)

            cell.imgFruits.image = UIImage(named: "splash_logo")
            cell.imgFruits.alpha = 0.3
            cell.activityIndicator.isHidden = false
            cell.activityIndicator.startAnimating()
            KingfisherManager.shared.retrieveImage(with: resource,  options: nil) { result in
                cell.activityIndicator.isHidden = true
                cell.activityIndicator.stopAnimating()
              switch result {
                case .success(let value):
                    cell.imgFruits.image = value.image
                    cell.imgFruits.alpha = 1.0
                case .failure(let error):
                    print("Error: \(error)")
                    cell.imgFruits.image = UIImage(named: "splash_logo")
                    cell.imgFruits.alpha = 0.3
              }
              cell.imgFruits.contentMode = .scaleAspectFill //scaleToFill //
              cell.imgFruits.clipsToBounds = true
            }
            cell.lblOfferPercentage.text = String(format:"UPTO %.0f", CategoryArray[indexPath.row].uptoDiscount) + "% OFF"
            cell.percentageView.layer.cornerRadius = 5
            cell.percentageView.backgroundColor = AppCommonColor
            cell.percentageView.layer.shadowColor = UIColor.gray.cgColor
            cell.percentageView.layer.shadowOpacity = 0.5
            cell.percentageView.layer.shadowRadius = 5.0
            cell.percentageView.layer.shadowOffset = CGSize(width: 0, height: 5)
            
            cell.shadowView.layer.shadowColor = UIColor.black.cgColor
            cell.shadowView.layer.shadowOpacity = 0.5
            cell.shadowView.layer.shadowRadius = 5.0
            cell.shadowView.layer.shadowOffset = CGSize(width: 3, height: 3)
            
            cell.lblOfferPercentage.isHidden = CategoryArray[indexPath.row].uptoDiscount > 0 ? false : true
            cell.percentageView.isHidden = CategoryArray[indexPath.row].uptoDiscount > 0 ? false : true
            
            return cell
        } else if collectionView == couponCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CouponCell
            cell.bgView.applyCommonShadow(borderColor: cell.lblCouponText.textColor)
            cell.lblCouponText.text = CouponArray[indexPath.row].couponDescription
            
            return cell
        } else if collectionView == testimonialsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "testimonialsCell", for: indexPath) as! TestimonialsCell
            cell.lblName.text = TestimonialsArray[indexPath.row].firstName + " " + TestimonialsArray[indexPath.row].lastName
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy"
            let dateObj = dateFormatter.date(from: TestimonialsArray[indexPath.row].testimonialDate)
            cell.lblTestimonialDate.text = dateObj?.convertDateFormat(date: dateObj!)
            cell.lblTestimonial.text = TestimonialsArray[indexPath.row].testimonial
            
            return cell
        } else if collectionView == yourFavoriteCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! YourFavoriteCell
            cell.lblFruitName.text = RecipeArray[indexPath.row].recipeTitle
            cell.lblOrganizeBy.text = RecipeArray[indexPath.row].brandName
            cell.btnFavorite.isSelected = true
            
            cell.btnFavorite.setImage(UIImage(named: "empty-heart")?.imageWithColor(color: .red), for: .normal)
            cell.btnFavorite.setImage(UIImage(named: "heart")?.imageWithColor(color: .red), for: .selected)
            let imagePath = RecipeArray[indexPath.row].recipeImageURL
            let imagePathString = imagePath!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: imagePathString)!
            let resource = ImageResource(downloadURL: url)

            cell.imgFruits.image = UIImage(named: "splash_logo")
            cell.imgFruits.alpha = 0.3
            cell.activityIndicator.isHidden = false
            cell.activityIndicator.startAnimating()
            KingfisherManager.shared.retrieveImage(with: resource,  options: nil) { result in
                cell.activityIndicator.isHidden = true
                cell.activityIndicator.stopAnimating()
              switch result {
                case .success(let value):
                    cell.imgFruits.image = value.image
                    cell.imgFruits.alpha = 1.0
                case .failure(let error):
                    print("Error: \(error)")
                    cell.imgFruits.image = UIImage(named: "splash_logo")
                    cell.imgFruits.alpha = 0.3
              }
              cell.imgFruits.contentMode = .scaleAspectFill //scaleToFill //
              cell.imgFruits.clipsToBounds = true
              cell.imgFruits.roundCorners(corners: [.topLeft, .topRight], radius: 5)
            }
            
            return cell
        } else if collectionView == brandsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! BrandCell
            //cell.bgView.applyCommonShadow(borderColor: UIColor.lightGray)
            
            cell.lblBrandName.text = BrandsArray[indexPath.row].brandName.uppercased()
            let imagePath = BrandsArray[indexPath.row].brandLogo
            let imagePathString = imagePath!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: imagePathString)!
            let resource = ImageResource(downloadURL: url)

            cell.imgBrand.image = UIImage(named: "splash_logo")?.circleMasked
            cell.imgBrand.alpha = 0.3
            cell.activityIndicator.isHidden = false
            cell.activityIndicator.startAnimating()
            KingfisherManager.shared.retrieveImage(with: resource,  options: nil) { result in
                cell.activityIndicator.isHidden = true
                cell.activityIndicator.stopAnimating()
              switch result {
                case .success(let value):
                    cell.imgBrand.image = value.image.circleMasked
                    cell.imgBrand.alpha = 1.0
                case .failure(let error):
                    print("Error: \(error)")
                    cell.imgBrand.image = UIImage(named: "splash_logo")?.circleMasked
                    cell.imgBrand.alpha = 0.3
              }
            }
            cell.imgBrand.clipsToBounds = true
            cell.imgBrand.layer.cornerRadius = 37
            cell.imgBrand.layer.masksToBounds = false
            
            cell.lblOfferPercentage.text = String(format:"%.0f", BrandsArray[indexPath.row].uptoDiscount) + "% OFF"
            cell.lblOfferPercentage.layer.cornerRadius = 5
            cell.lblOfferPercentage.clipsToBounds = true
            cell.lblOfferPercentage.layer.shadowRadius = 5
            cell.lblOfferPercentage.layer.shadowOpacity = 0.5
            cell.lblOfferPercentage.layer.shadowColor = UIColor.gray.cgColor
            cell.lblOfferPercentage.layer.shadowOffset = CGSize(width: 0 , height:2)
            cell.lblOfferPercentage.isHidden = BrandsArray[indexPath.row].uptoDiscount > 0 ? false : true
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TopSellingCell
            cell.lblFruitName.text = TopSellingArray[indexPath.row].productName
            cell.lblOrganizeBy.text = TopSellingArray[indexPath.row].brandName
            cell.btnFavorite.isSelected = TopSellingArray[indexPath.row].isFavourite
            
            cell.btnFavorite.setImage(UIImage(named: "empty-heart")?.imageWithColor(color: .red), for: .normal)
            cell.btnFavorite.setImage(UIImage(named: "heart")?.imageWithColor(color: .red), for: .selected)
            let imagePath = TopSellingArray[indexPath.row].productPackageImage
            let imagePathString = imagePath!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: imagePathString)!
            let resource = ImageResource(downloadURL: url)

            cell.imgFruits.image = UIImage(named: "splash_logo")
            cell.imgFruits.alpha = 0.3
            cell.activityIndicator.isHidden = false
            cell.activityIndicator.startAnimating()
            KingfisherManager.shared.retrieveImage(with: resource,  options: nil) { result in
                cell.activityIndicator.isHidden = true
                cell.activityIndicator.stopAnimating()
              switch result {
                case .success(let value):
                    cell.imgFruits.alpha = 1.0
                    cell.imgFruits.image = value.image
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                      cell.imgFruits.roundCorners(corners: [.topLeft, .topRight], radius: 5)
                    }
                case .failure(let error):
                    print("Error: \(error)")
                    cell.imgFruits.alpha = 0.3
                    cell.imgFruits.image = UIImage(named: "splash_logo")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                      cell.imgFruits.roundCorners(corners: [.topLeft, .topRight], radius: 5)
                    }
              }
            }
            return cell
        }
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in couponCollectionView.visibleCells {
            let indexPath = couponCollectionView.indexPath(for: cell)
            self.couponPageControl.currentPage = indexPath!.row
        }
        
        for cell in testimonialsCollectionView.visibleCells {
            let indexPath = testimonialsCollectionView.indexPath(for: cell)
            self.testimonialsPageControl.currentPage = indexPath!.row
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var cellSize: CGSize = collectionView.bounds.size
        if categoryCollectionView == collectionView {
            return CGSize(width: cellSize.width/3-5, height: cellSize.width/2.1)
        }
        cellSize.width = 130 //cellSize.width/3-5
        
        return collectionView == couponCollectionView ? CGSize(width: collectionView.bounds.size.width, height: cellSize.height) : collectionView == brandsCollectionView ? CGSize(width: 130, height: 130) : collectionView == testimonialsCollectionView ? CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height) : cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == favoritesCollectionView {
            let productViewController = self.storyboard?.instantiateViewController(withIdentifier: "idProductDetailsViewController") as! ProductDetailsViewController
            productViewController.prodUID = MyFavoriteArray[indexPath.row].productUID
            self.navigationController?.pushViewController(productViewController, animated: true)
        } else if collectionView == categoryCollectionView {
            self.title = ""
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "idCategoryDetailsViewController") as! CategoryDetailsViewController
            controller.title = CategoryArray[indexPath.row].categoryName
            controller.categoryName = CategoryArray[indexPath.row].categoryName
            controller.type = "category"
            self.navigationController?.pushViewController(controller, animated: true)
        } else if collectionView == yourFavoriteCollectionView {  //Recipe
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "idRecipeDetailsViewController") as! RecipeDetailsViewController
            controller.RecipeUID = RecipeArray[indexPath.row].r_UID
            self.navigationController?.pushViewController(controller, animated: true)
        } else if collectionView == brandsCollectionView {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "idCategoryDetailsViewController") as! CategoryDetailsViewController
            controller.title = BrandsArray[indexPath.row].brandName
            controller.brandName = BrandsArray[indexPath.row].brandName
            controller.type = "brand"
            self.navigationController?.pushViewController(controller, animated: true)
        } else if collectionView == topSellingCollectionView {
            let productViewController = self.storyboard?.instantiateViewController(withIdentifier: "idProductDetailsViewController") as! ProductDetailsViewController
            productViewController.prodUID = TopSellingArray[indexPath.row].productUID
            self.navigationController?.pushViewController(productViewController, animated: true)
        }
    }
    
    func ShowLoader() {
        if isLoader {
           RappleActivityIndicatorView.startAnimatingWithLabel("Processing...", attributes: RappleModernAttributes)
        }
    }
    
    func HideLoader() {
        self.refresher.endRefreshing()
        DispatchQueue.main.async {
            RappleActivityIndicatorView.stopAnimation(completionIndicator: .none, completionLabel: "", completionTimeout: 0.1)
        }
    }
}

extension HomeViewController: FSPagerViewDataSource,FSPagerViewDelegate {

    // MARK:- FSPagerView DataSource
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return BannersArray.count
    }

    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)

        let imagePath = BannersArray[index].bannerURL
        let imagePathString = imagePath!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let url = URL(string: imagePathString)!
        let resource = ImageResource(downloadURL: url)

        cell.imageView?.image = UIImage(named: "splash_logo")
        cell.imageView?.alpha = 0.3
        KingfisherManager.shared.retrieveImage(with: resource,  options: nil) { result in
            switch result {
              case .success(let value):
                  cell.imageView?.image = value.image
                  cell.imageView?.alpha = 1.0
              case .failure(let error):
                  print("Error: \(error)")
                  cell.imageView?.image = UIImage(named: "splash_logo")
                  cell.imageView?.alpha = 0.3
            }
        }
        cell.imageView?.contentMode = .scaleAspectFill //scaleToFill //
        cell.imageView?.clipsToBounds = true
        
        cell.imageView?.backgroundColor = .clear
        cell.imageView?.applyshadowWithCorner(containerView: bannerBGView, cornerRadious: 10, shadowColor: .clear)
        
        cell.textLabel?.text = ""//BannersArray[index].bannerValue
        
        
        
        bannerBGView.layer.cornerRadius = 5
        bannerBGView.backgroundColor = .clear
        bannerBGView.layer.shadowColor = UIColor.gray.cgColor
        bannerBGView.layer.shadowOpacity = 0.5
        bannerBGView.layer.shadowRadius = 2.0
        bannerBGView.layer.shadowOffset = CGSize(width: 0, height: 5)
        return cell
    }

    // MARK:- FSPagerView Delegate
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        
        if BannersArray[index].bannerTarget == "category-products" {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "idCategoryDetailsViewController") as! CategoryDetailsViewController
            controller.title = BannersArray[index].bannerValue
            controller.categoryName = BannersArray[index].bannerValue
            self.navigationController?.pushViewController(controller, animated: true)
        } else if BannersArray[index].bannerTarget == "category-brand" || BannersArray[index].bannerTarget == "brand-products" {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "idCategoryDetailsViewController") as! CategoryDetailsViewController
            controller.title = BannersArray[index].bannerValue
            controller.brandName = BannersArray[index].bannerValue
            self.navigationController?.pushViewController(controller, animated: true)
        } else if BannersArray[index].bannerTarget == "product-detail" {
            let productViewController = self.storyboard?.instantiateViewController(withIdentifier: "idProductDetailsViewController") as! ProductDetailsViewController
            productViewController.prodUID = BannersArray[index].bannerValue
            self.navigationController?.pushViewController(productViewController, animated: true)
        }
        
    }

    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }

    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControl.currentPage = pagerView.currentIndex
    }

}
//MARK: API RESPONCES
extension HomeViewController {
    func setAPNToken() {
        let deviceId = UIDevice.current.identifierForVendor?.uuidString
        let urlString = APIConstants.setAPNToken+"phoneNo=\(globalUserPhone)&token=\(userDeviceToken)&deviceUID=\(deviceId!)&deviceName=\(UIDevice.current.name)&device=\(UIDevice.modelName)&deviceOS=ios"
        print("set apn api: ", urlString)
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (Response, success) in
            if Response.count > 0 {
                
            }
        }
    }
    
    func getUserDInfo() {
        let urlString = APIConstants.getUserInfo+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (Response, success) in
            if Response.count > 0 {
                let KeysArray = Response[0].fieldLookup
                let fieldsArray = Response[0].fields
                LocalDB.shared.UserInfo = User(customerUID: fieldsArray[KeysArray["customerUID"].intValue].stringValue, firstName: fieldsArray[KeysArray["firstName"].intValue].stringValue, lastName: fieldsArray[KeysArray["lastName"].intValue].stringValue, phoneNo: fieldsArray[KeysArray["phoneNo"].intValue].stringValue, email: fieldsArray[KeysArray["email"].intValue].stringValue, isValidated: fieldsArray[KeysArray["isValidated"].intValue].stringValue)
            }
        }
    }
    
    func getUserDefaultAddress() {
        ShowLoader()
        let urlString = APIConstants.getDefaultAddress+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (Response, success) in
            if Response.count > 0 {
                let KeysArray = Response[0].fieldLookup
                let fieldsArray = Response[0].fields
                defaultAddress = fieldsArray[KeysArray["address"].intValue].stringValue
                self.setDefaultAddress(title: fieldsArray[KeysArray["title"].intValue].stringValue, address: fieldsArray[KeysArray["address"].intValue].stringValue)

                DefaultAddressArray = Address(addressUID: fieldsArray[KeysArray["addressUID"].intValue].stringValue, title: fieldsArray[KeysArray["title"].intValue].stringValue, house: fieldsArray[KeysArray["house"].intValue].stringValue, address: fieldsArray[KeysArray["address"].intValue].stringValue, pincode: fieldsArray[KeysArray["pincode"].intValue].stringValue, latitude: fieldsArray[KeysArray["latitude"].intValue].stringValue, longitude: fieldsArray[KeysArray["longitude"].intValue].stringValue, isDeliverable: fieldsArray[KeysArray["isDeliverable"].intValue].boolValue, isBillingAddress: fieldsArray[KeysArray["isBillingAddress"].intValue].boolValue, isDefaultAddress: fieldsArray[KeysArray["isDefaultAddress"].intValue].boolValue)
            }
            self.btnCheckBox.isHidden = false
            self.btnCheckBox.isSelected = DefaultAddressArray != nil ? DefaultAddressArray.isDeliverable : false
            self.getBanners()
            UpdateCartCount()
        }
    }
    
    func getBanners() {
        let urlString = APIConstants.getBanners
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (Response, success) in
            BannersArray = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    BannersArray.append(Banner(bannerTarget: fieldsArray[KeysArray["bannerTarget"].intValue].stringValue, bannerValue: fieldsArray[KeysArray["bannerValue"].intValue].stringValue, bannerURL: fieldsArray[KeysArray["bannerURL"].intValue].stringValue, bannerPriority: fieldsArray[KeysArray["bannerPriority"].intValue].stringValue))
                }
            }
            self.tableView.reloadData()
            self.pagerView.reloadData()
            self.getUpcomingOrders()
        }
    }
    
    func getUpcomingOrders() {
        let urlString = APIConstants.getUpcomingOrders+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (Response, success) in
            upcommingOrdersArray = []
            if Response.count > 0 {
                self.isAvailableOrder = true
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    
                    let commonProduct = Response.filter{$0.fields[$0.fieldLookup["orderUID"].intValue].stringValue == fieldsArray[KeysArray["orderUID"].intValue].stringValue}
                    
                    upcommingOrdersArray.append(Upcoming(orderUID: fieldsArray[KeysArray["orderUID"].intValue].stringValue, deliveryDate: fieldsArray[KeysArray["deliveryDate"].intValue].stringValue, orderTotal: fieldsArray[KeysArray["orderTotal"].intValue].stringValue, orderStatus: fieldsArray[KeysArray["orderStatus"].intValue].stringValue, deliveryStatus: fieldsArray[KeysArray["deliveryStatus"].intValue].stringValue, productName: commonProduct.map{$0.fields[$0.fieldLookup["productName"].intValue].stringValue}, variationTitle: commonProduct.map{$0.fields[$0.fieldLookup["variationTitle"].intValue].stringValue}, quantityPurchased: commonProduct.map{$0.fields[$0.fieldLookup["quantityPurchased"].intValue]["low"].stringValue}))
                }
                upcommingOrdersArray = upcommingOrdersArray.filterDuplicates{$0.orderUID == $1.orderUID}
                
                if upcommingOrdersArray.count > 0 {
                    let date = upcommingOrdersArray[0].deliveryDate!
                    let weekString = getConvertedDate(string: date).dayOfWeek()!
                    let fullDateString = getDate(date: date, format: "dd-MM-yyyy", reverseFormat: "MMMM dd, yyyy")
                    self.lblOrderDate.attributedText = self.getAttributedString(label: self.lblOrderDate, key: [weekString+",\n"], value: [fullDateString], color: self.lblOrderDate.textColor)
                    self.lblOrderDate.textAlignment = .right
                        
                     //   weekString + ",\n\n" + fullDateString
                    self.lblOrderStatus.text = upcommingOrdersArray[0].orderStatus!
                    self.lblOrderStatus.text = self.lblOrderStatus.text!.uppercased()

                    let productNameArray = upcommingOrdersArray[0].productName!
                    let variationTitle = upcommingOrdersArray[0].variationTitle!
                    let quantityPurchased = upcommingOrdersArray[0].quantityPurchased!
                    if productNameArray.count > 0 {
                        var allItems = ""
                        for index in 0..<productNameArray.count {
                            allItems = allItems + "\(productNameArray[index]) (\(variationTitle[index])) x \(quantityPurchased[index])"
                            allItems = allItems + (index == productNameArray.count-1 ? "" : ",\n")
                        }
                        self.lblItemSummary.text = allItems
                    } else{
                        self.lblItemSummary.text = "No items found."
                    }
                    self.lblItemTotal.text = "Total: ₹" + upcommingOrdersArray[0].orderTotal!
                    

                    self.lblItemSummary.isHidden = false
                    self.lblItemTotal.isHidden = false
                    self.lblOrderDate.isHidden = false
                    self.lblOrderStatus.isHidden = false
                    self.labelSUmmary.isHidden = false
                    self.labelDelivery.isHidden = false
                    
                    self.lblNoUpcomingOrder.isHidden = true
                }
            } else {
                self.lblItemSummary.isHidden = true
                self.lblItemTotal.isHidden = true
                self.lblOrderDate.isHidden = true
                self.lblOrderStatus.isHidden = true
                self.labelSUmmary.isHidden = true
                self.labelDelivery.isHidden = true
                
                self.lblNoUpcomingOrder.isHidden = false
                self.isAvailableOrder = true
            }
            self.labelUpcomingOrder.text = self.isAvailableOrder ? " Upcoming Orders" : " No upcoming order"
            self.labelUpcomingOrder.letterSpace = 1.5
            self.tableView.reloadData()
            self.getFavouriteProducts()
        }
    }
    
    func getFavouriteProducts() {
        let urlString = APIConstants.getFavouriteProducts+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (Response, success) in
            MyFavoriteArray = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    MyFavoriteArray.append(Favorite(productUID: fieldsArray[KeysArray["productUID"].intValue].stringValue, productName: fieldsArray[KeysArray["productName"].intValue].stringValue, productPackageImage: fieldsArray[KeysArray["productPackageImage"].intValue].stringValue, brandName: fieldsArray[KeysArray["brandName"].intValue].stringValue))
                }
            }
            self.btnShowMoreFavorite.isHidden = MyFavoriteArray.count > 3 ? false : true
            self.tableView.reloadData()
            self.favoritesCollectionView.reloadData()
            self.getCategories()
        }
    }
    
    func getCategories() {
        let urlString = APIConstants.getCategories
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (Response, success) in
            CategoryArray = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    CategoryArray.append(Category(c_UID: fieldsArray[KeysArray["c.UID"].intValue].stringValue, categoryName: fieldsArray[KeysArray["categoryName"].intValue].stringValue, categoryPackageImage: fieldsArray[KeysArray["categoryPackageImage"].intValue].stringValue, categoryPriority: fieldsArray[KeysArray["categoryPriority"].intValue]["low"].stringValue, uptoDiscount: fieldsArray[KeysArray["uptoDiscount"].intValue].doubleValue))
                }
            }
            self.categoryCollectionView.reloadData()
            self.tableView.reloadData()
            self.layoutSubviews()
            self.getCoupons()
        }
    }
    
    func getCoupons() {
        let urlString = APIConstants.getCoupons+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (Response, success) in
            CouponArray = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    CouponArray.append(Coupon(couponUID: fieldsArray[KeysArray["couponUID"].intValue].stringValue, couponName: fieldsArray[KeysArray["couponName"].intValue].stringValue, couponDescription: fieldsArray[KeysArray["couponDescription"].intValue].stringValue, couponCode: fieldsArray[KeysArray["couponCode"].intValue].stringValue))
                }
            }
            self.couponPageControl.numberOfPages = CouponArray.count
            self.couponPageControl.currentPage = 0
            self.couponCollectionView.reloadData()
            self.tableView.reloadData()
            self.getFavouriteRecipes()
        }
    }
    
    func getFavouriteRecipes() {
        let urlString = APIConstants.getFavouriteRecipes+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (Response, success) in
            RecipeArray = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    RecipeArray.append(Recipes(r_UID: fieldsArray[KeysArray["c.UID"].intValue].stringValue, recipeTitle: fieldsArray[KeysArray["recipeTitle"].intValue].stringValue, recipeImageURL: fieldsArray[KeysArray["recipeImageURL"].intValue].stringValue, brandName: fieldsArray[KeysArray["brandName"].intValue].stringValue))
                }
            }
            self.btnShowMoreRecipe.isHidden = MyFavoriteArray.count > 3 ? false : true
            self.yourFavoriteCollectionView.reloadData()
            self.tableView.reloadData()
            self.getBrands()
        }
    }
    
    func getBrands() {
        let urlString = APIConstants.getBrands
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (Response, success) in
            BrandsArray = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    BrandsArray.append(Brands(brandUID: fieldsArray[KeysArray["brandUID"].intValue].stringValue, brandLogo: fieldsArray[KeysArray["brandLogo"].intValue].stringValue, brandName: fieldsArray[KeysArray["brandName"].intValue].stringValue, uptoDiscount: fieldsArray[KeysArray["uptoDiscount"].intValue].doubleValue))
                }
            }
            self.brandsCollectionView.reloadData()
            self.tableView.reloadData()
            self.getBestsellers()
        }
    }
    
    func getBestsellers() {
        let urlString = APIConstants.getBestsellers+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (Response, success) in
            TopSellingArray = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    TopSellingArray.append(TopSelling(productUID: fieldsArray[KeysArray["productUID"].intValue].stringValue, productName: fieldsArray[KeysArray["productName"].intValue].stringValue, productPackageImage: fieldsArray[KeysArray["productPackageImage"].intValue].stringValue, prodCount: fieldsArray[KeysArray["prodCount"].intValue].stringValue, isFavourite: fieldsArray[KeysArray["isFavourite"].intValue].boolValue, brandName: fieldsArray[KeysArray["brandName"].intValue].stringValue))
                }
            }
            self.topSellingCollectionView.reloadData()
            self.tableView.reloadData()
            self.getTestimonials()
        }
    }
    
    
    func getTestimonials() {
        let urlString = APIConstants.getTestimonials
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (Response, success) in
            TestimonialsArray = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    TestimonialsArray.append(Testimonials(testimonialUID: fieldsArray[KeysArray["testimonialUID"].intValue].stringValue, testimonial: fieldsArray[KeysArray["testimonial"].intValue].stringValue, firstName: fieldsArray[KeysArray["firstName"].intValue].stringValue, lastName: fieldsArray[KeysArray["lastName"].intValue].stringValue, testimonialDate: fieldsArray[KeysArray["testimonialDate"].intValue].stringValue))
                }
            }
            self.testimonialsPageControl.numberOfPages = TestimonialsArray.count
            self.testimonialsPageControl.currentPage = 0
            self.tableView.reloadData()
            self.testimonialsCollectionView.reloadData()
            self.HideLoader()
        }
    }
    
    func getAttributedString(label:UILabel, key: [String], value: [String], color: UIColor) -> NSAttributedString{
        let attributedString = NSMutableAttributedString()
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = CGFloat(10)
        let keyColor = color.withAlphaComponent(0.9)
        for (index, text) in key.enumerated() {
            let attrs1 = [NSAttributedString.Key.font : label.font, NSAttributedString.Key.foregroundColor : keyColor]
            
            let attrs2 = [NSAttributedString.Key.font : label.font, NSAttributedString.Key.foregroundColor : color]
            
            let attributedString1 = NSMutableAttributedString(string:text, attributes:attrs1 as [NSAttributedString.Key : Any])
            
            let attributedString2 = NSMutableAttributedString(string: value.count-1==index ? value[index] : value[index], attributes:attrs2 as [NSAttributedString.Key : Any])
            
            attributedString.append(attributedString1)
            attributedString.append(attributedString2)
        }
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        return attributedString
    }
}
