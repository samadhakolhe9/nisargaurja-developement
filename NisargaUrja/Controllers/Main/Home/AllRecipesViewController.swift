//
//  AllRecipesViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 21/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class AllRecipesViewController: UICollectionViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "All Recipes"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let layout = UICollectionViewFlowLayout()
         layout.scrollDirection = .vertical
        collectionView.collectionViewLayout = layout
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 50, right: 10)
        
         let width = collectionView.frame.size.width
         if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumInteritemSpacing = 5
            layout.minimumLineSpacing = 5
            layout.itemSize = CGSize(width: width/3 - 15, height: width/2.2)
            layout.invalidateLayout()
        }
    }
        
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return RecipeArray.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! YourFavoriteCell
        cell.lblFruitName.text = RecipeArray[indexPath.row].recipeTitle
        cell.lblOrganizeBy.text = RecipeArray[indexPath.row].brandName
        cell.btnFavorite.isSelected = true
        
        cell.btnFavorite.setImage(UIImage(named: "empty-heart")?.imageWithColor(color: .red), for: .normal)
        cell.btnFavorite.setImage(UIImage(named: "heart")?.imageWithColor(color: .red), for: .selected)
        let imagePath = RecipeArray[indexPath.row].recipeImageURL
        let imagePathString = imagePath!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let url = URL(string: imagePathString)!
        let resource = ImageResource(downloadURL: url)

        cell.imgFruits.image = UIImage(named: "splash_logo")
        cell.imgFruits.alpha = 0.3
        cell.activityIndicator.isHidden = false
        cell.activityIndicator.startAnimating()
        KingfisherManager.shared.retrieveImage(with: resource,  options: nil) { result in
            cell.activityIndicator.isHidden = true
            cell.activityIndicator.stopAnimating()
          switch result {
            case .success(let value):
                cell.imgFruits.image = value.image
                cell.imgFruits.alpha = 1.0
            case .failure(let error):
                print("Error: \(error)")
                cell.imgFruits.image = UIImage(named: "splash_logo")
                cell.imgFruits.alpha = 0.3
          }
          cell.imgFruits.roundCorners(corners: [.topLeft, .topRight], radius: 5)
          cell.imgFruits.contentMode = .scaleToFill
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var cellSize: CGSize = collectionView.bounds.size
        cellSize.width = 130 //cellSize.width/3-15
        cellSize.height = 220 //cellSize.width/2.2
        
        return cellSize
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "idRecipeDetailsViewController") as! RecipeDetailsViewController
        controller.RecipeUID = RecipeArray[indexPath.row].r_UID
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
