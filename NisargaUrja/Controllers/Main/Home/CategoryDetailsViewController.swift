//
//  CategoryDetailsViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 18/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

struct CategoryData {
    var brandName: String!
    var productUID: String!
    var productName: String!
    var productPackageImage: String!
    var variationUID: [String]!
    var variationTitle: [String]!
    var MRP: [String]!
    var discountPrice: [Double]!
    var inStock: Bool!
    var isFavourite: Bool!
    var tags: String!
}

struct CartData {
    var brandName: String!
    var productName: String!
    var productPackageImage: String!
    var variationUID: String!
    var variationTitle: String!
    var quantity: String!
    var MRP: String!
    var discountedPrice: String!
    var deliveryDate: String!
}

func UpdateCartCount() {
//       let urlString = APIConstants.getcartItem+"phoneNo=\(globalUserPhone)"
//       ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (responseData, success) in
//           CartRightButton.badge = "\(responseData.count)"
//           CartRightButton.badgeEdgeInsets = UIEdgeInsets(top: 14, left: 6, bottom: 0, right: 0)
//           hhTabBarView.tabBarTabs[2].badge = CartRightButton.badge
//       }
    

    if DefaultAddressArray != nil {
        let urlString = APIConstants.getcartTotals+"phoneNo=\(globalUserPhone)&addressUID=\(DefaultAddressArray.addressUID!)"
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (responseData, success) in
            print(responseData)
            if responseData.count > 0 {
                let numberOfItems = responseData[0].fields[9]["low"].intValue
                CartRightButton.badge = "\(numberOfItems)"
            } else {
                CartRightButton.badge = "0"
            }
            CartRightButton.badgeEdgeInsets = UIEdgeInsets(top: 14, left: 6, bottom: 0, right: 0)
            hhTabBarView.tabBarTabs[2].badge = CartRightButton.badge
            hhTabBarView.tabBarTabs[2].badgeLabel.isHidden = CartRightButton.badge == "0" ? true : false
        }
    } else {
        CartRightButton.badge = "0"
        hhTabBarView.tabBarTabs[2].badge = CartRightButton.badge
        hhTabBarView.tabBarTabs[2].badgeLabel.isHidden = true
    }
    
}

let CartRightButton = BadgeButton()
class CategoryDetailsViewController: UITableViewController, UISearchBarDelegate {
    
    var CategoryListArray:[CategoryData] = []
    var FilteredCategoryListArray:[CategoryData] = []
    var CartListArray:[CartData] = []
    var categoryName = ""
    var brandName = ""
    var type = ""
    var OffersPercent: Int = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        initialNavigation()
    }
    
    func initialNavigation() {
         CartRightButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
         CartRightButton.setImage(#imageLiteral(resourceName: "cart_icon").imageWithColor(color: .black), for: UIControl.State.normal)
         CartRightButton.addTarget(self, action: #selector(self.CartButtonAction), for: UIControl.Event.touchUpInside)
         CartRightButton.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
         CartRightButton.contentMode = .center
         CartRightButton.badgeEdgeInsets = UIEdgeInsets(top: 15, left: 50, bottom: 0, right: 0)
        // CartRightButton.badge = "0"
         CartRightButton.badgeBackgroundColor = .orange
         let notification = UIBarButtonItem(customView: CartRightButton)
         
         let rightButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "search_icon")!.imageWithColor(color: .black), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.searchButtonActionButton))
         
         self.navigationItem.rightBarButtonItems = [notification, rightButton]
    }
    
    @objc public func searchButtonActionButton() {
        if self.navigationController?.navigationBar.topItem?.titleView == nil {
            let searchBar = UISearchBar()
            searchBar.sizeToFit()
            searchBar.delegate = self
            searchBar.placeholder = "Search here"
            searchBar.becomeFirstResponder()
            self.navigationController?.navigationBar.topItem?.titleView = searchBar
        } else {
            self.FilteredCategoryListArray = self.CategoryListArray
            self.tableView.reloadData()
            self.view.endEditing(true)
            self.navigationController?.navigationBar.topItem?.titleView = nil
        }
    }
    
    @objc func CartButtonAction() {
        selectedTabIndex = 2
        referenceUITabBarController = setupReferenceUITabBarController()
        setupHHTabBarView(selectedIndex: selectedTabIndex)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 250
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
   
        NotificationCenter.default.addObserver(self, selector:#selector(self.reloadPage), name: UIApplication.willEnterForegroundNotification, object: UIApplication.shared)
    }

    
    @objc func reloadPage(notification: NSNotification) {
        UpdateCartCount()
        getCartItems()
        getOffers()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UpdateCartCount()
        getCartItems()
        getOffers()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let searchText = searchText.lowercased()
        self.FilteredCategoryListArray = self.CategoryListArray
        if searchText != "" {
            self.FilteredCategoryListArray = FilteredCategoryListArray.filter{$0.productName.lowercased().contains(searchText) || $0.brandName.lowercased().contains(searchText) || $0.tags.lowercased().contains(searchText)}
        }
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FilteredCategoryListArray.count+1 //Add 1 for header showing
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "idOffersCell") as? OffersCell else {
                return UITableViewCell()
            }
            cell.lblOfferMessage.text = "Get UPTO \(OffersPercent)% OFF on \(brandName != "" ? brandName : categoryName)"
            cell.lblOfferPercentAmount.text = String(OffersPercent)+"%"
           // cell.outerView.applyCommonShadow(borderColor: #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1))
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "idItemCategoryDetailsCell") as? ItemCategoryDetailsCell else {
                return UITableViewCell()
            }
            
            let newIndex = indexPath.row - 1
            cell.isAddButton = FilteredCategoryListArray[newIndex].inStock
            cell.isAbleToChange = false
            cell.CategoryProducts = FilteredCategoryListArray[newIndex]
            cell.lblProductName.text = FilteredCategoryListArray[newIndex].productName
            cell.lblByNU.text = ("By "+FilteredCategoryListArray[newIndex].brandName)///.uppercased()
            
            cell.variationUID = FilteredCategoryListArray[newIndex].variationUID.count > 0 ? FilteredCategoryListArray[newIndex].variationUID[0] : ""
            let cartValue = self.CartListArray.filter{$0.variationUID == cell.variationUID}
            
            cell.lblOriginalMRP.attributedText = "MRP: ₹\(FilteredCategoryListArray[newIndex].MRP![0])".strikeThrough()
            cell.lblOfferMRP.text = String(format:"You Pay: ₹ %.1f", FilteredCategoryListArray[newIndex].discountPrice[0])
            
            let MRP = Double("\(FilteredCategoryListArray[newIndex].MRP![0])")!
            let discountedPrices = Double("\(FilteredCategoryListArray[newIndex].discountPrice![0])")!
            let diffrence = MRP-discountedPrices
            let multiply = diffrence*100
            cell.lblOfferPercentage.text = String(format:"%.0f", multiply/MRP) + "% OFF!"
            cell.percentageView.isHidden = (multiply/MRP) > 0 ? false : true
            
            cell.allMRP = FilteredCategoryListArray[newIndex].MRP!
            cell.allDiscountPrice = FilteredCategoryListArray[newIndex].discountPrice!
            
            cell.btnHeart.isSelected = FilteredCategoryListArray[newIndex].isFavourite
            cell.lblNUQuoteMessage.text = FilteredCategoryListArray[newIndex].tags
            
            cell.btnHeart.setImage(UIImage(named: "empty-heart")?.imageWithColor(color: .red), for: .normal)
            cell.btnHeart.setImage(UIImage(named: "heart")?.imageWithColor(color: .red), for: .selected)
            
            cell.lblOfferPercentage.layer.cornerRadius = 5
            cell.lblOfferPercentage.clipsToBounds = true
            cell.lblOfferPercentage.layer.shadowRadius = 5
            cell.lblOfferPercentage.layer.shadowOpacity = 0.5
            cell.lblOfferPercentage.layer.shadowColor = UIColor.gray.cgColor
            cell.lblOfferPercentage.layer.shadowOffset = CGSize(width: 0 , height:2)
            
            
            
            if FilteredCategoryListArray[newIndex].inStock {
                cell.isAddButton = true
                cell.btnAddToCart.isEnabled = true
                cell.btnAddToCart.alpha = 1.0
                cell.btnAddToCart.setTitle("ADD", for: .normal)
                cell.addButtonWidthConstraint.constant = 70
                cell.stprProducctQuantity.value = cartValue.count > 0 ? Double(Int("\(cartValue[0].quantity!)")!) : 0.0
                cell.quantityCount = cell.stprProducctQuantity.value
                cell.btnAddToCart.isHidden = cell.stprProducctQuantity.value == 0.0 ? false : true
                cell.stprProducctQuantity.isHidden = !cell.btnAddToCart.isHidden
                cell.btnAddToCart.backgroundColor = AppCommonColor
                cell.btnAddToCart.layer.borderColor = UIColor.gray.cgColor
                cell.btnAddToCart.layer.shadowColor = UIColor.gray.cgColor
            } else {
                cell.btnAddToCart.isEnabled = false
                cell.btnAddToCart.alpha = 0.5
                cell.btnAddToCart.isHidden = false
                cell.btnAddToCart.setTitle("OUT OF STOCK", for: .normal)
                cell.addButtonWidthConstraint.constant = 100
                
                cell.btnAddToCart.backgroundColor = .clear
                cell.btnAddToCart.layer.borderWidth = 2
                cell.btnAddToCart.layer.borderColor = #colorLiteral(red: 0.8980392157, green: 0.5333333333, blue: 0.3137254902, alpha: 1)
                cell.btnAddToCart.layer.shadowColor = #colorLiteral(red: 0.8980392157, green: 0.5333333333, blue: 0.3137254902, alpha: 1)
                cell.btnAddToCart.setTitleColor(#colorLiteral(red: 0.8980392157, green: 0.5333333333, blue: 0.3137254902, alpha: 1), for: .normal)
            }
            
            cell.txtDropdownQuantity.text = FilteredCategoryListArray[newIndex].variationTitle.count > 0 ? FilteredCategoryListArray[newIndex].variationTitle[0] : ""
            cell.txtDropdownQuantity.isSearchEnable = false
            cell.txtDropdownQuantity.hideOptionsWhenSelect = true
            cell.txtDropdownQuantity.checkMarkEnabled = false
            
            cell.CartListArray = CartListArray
            cell.txtDropdownQuantity.optionArray = FilteredCategoryListArray[newIndex].variationTitle
            cell.dropdownWidthConstraint.constant = cell.txtDropdownQuantity.intrinsicContentSize.width + 15
            cell.dropdownDividerWidthConstraint.constant = cell.txtDropdownQuantity.intrinsicContentSize.width + 15
            
            let imagePath = FilteredCategoryListArray[newIndex].productPackageImage
            let imagePathString = imagePath!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: imagePathString)!
            let resource = ImageResource(downloadURL: url)
            cell.imgProduct.image = UIImage(named: "splash_logo")
            cell.imgProduct.alpha = 0.3
            cell.activityIndicator.isHidden = false
            cell.activityIndicator.startAnimating()
            KingfisherManager.shared.retrieveImage(with: resource,  options: nil) { result in
                cell.activityIndicator.isHidden = true
                cell.activityIndicator.stopAnimating()
                switch result {
                case .success(let value):
                    cell.imgProduct.image = value.image
                    cell.imgProduct.alpha = 1.0
                case .failure(let error):
                    print("Error: \(error)")
                    cell.imgProduct.image = UIImage(named: "splash_logo")
                    cell.imgProduct.alpha = 0.3
                }
            }
            cell.isAbleToChange = true
            cell.selectionStyle = .none
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 {
            let productViewController = self.storyboard?.instantiateViewController(withIdentifier: "idProductDetailsViewController") as! ProductDetailsViewController
            productViewController.prodUID = FilteredCategoryListArray[indexPath.row-1].productUID
            self.navigationController?.pushViewController(productViewController, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? (OffersPercent > 0 ? 60 : 0) : UITableView.automaticDimension
    }
    
    func getCartItems() {
        let urlString = APIConstants.getcartItem+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.CartListArray = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    
                    self.CartListArray.append(CartData(brandName: fieldsArray[KeysArray["brandName"].intValue].stringValue, productName: fieldsArray[KeysArray["productName"].intValue].stringValue, productPackageImage: fieldsArray[KeysArray["productPackageImage"].intValue].stringValue, variationUID: fieldsArray[KeysArray["variationUID"].intValue].stringValue, variationTitle: fieldsArray[KeysArray["variationTitle"].intValue].stringValue, quantity: fieldsArray[KeysArray["quantity"].intValue]["low"].stringValue, MRP: fieldsArray[KeysArray["MRP"].intValue].stringValue, discountedPrice: fieldsArray[KeysArray["discountedPrice"].intValue].stringValue))
                }
                //CartRightButton.badge = "\(self.CartListArray.count)"
            }
            self.getCategoriesByID()
        }
    }
    
    func getCategoriesByID() {
        let urlString = brandName != "" ? APIConstants.getBrandProducts+"brandName=\(brandName)&phoneNo=\(globalUserPhone)" :  APIConstants.getCategoryProducts+"categoryName=\(categoryName)&phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.FilteredCategoryListArray = []
            self.CategoryListArray = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    
                    let commonProduct = Response.filter{$0.fields[$0.fieldLookup["productUID"].intValue].stringValue == fieldsArray[KeysArray["productUID"].intValue].stringValue}
                    
                    self.FilteredCategoryListArray.append(CategoryData(brandName: fieldsArray[KeysArray["brandName"].intValue].stringValue, productUID: fieldsArray[KeysArray["productUID"].intValue].stringValue, productName: fieldsArray[KeysArray["productName"].intValue].stringValue, productPackageImage: fieldsArray[KeysArray["productPackageImage"].intValue].stringValue, variationUID: commonProduct.map{$0.fields[$0.fieldLookup["variationUID"].intValue].stringValue}, variationTitle: commonProduct.map{$0.fields[$0.fieldLookup["variationTitle"].intValue].stringValue}, MRP: commonProduct.map{$0.fields[$0.fieldLookup["MRP"].intValue].stringValue}, discountPrice: commonProduct.map{$0.fields[$0.fieldLookup["discountPrice"].intValue].doubleValue}, inStock: fieldsArray[KeysArray["inStock"].intValue].boolValue, isFavourite: fieldsArray[KeysArray["isFavourite"].intValue].boolValue, tags: (fieldsArray[KeysArray["tags"].intValue].arrayValue).map{$0.stringValue}.joined(separator: " · ")))
                }
                self.FilteredCategoryListArray = self.FilteredCategoryListArray.filterDuplicates{$0.productUID == $1.productUID}
                self.CategoryListArray = self.FilteredCategoryListArray
            }
            self.tableView.reloadData()
        }
    }

    func getOffers() {
        let urlString = APIConstants.getOffers+"type=\(type)&name=\(brandName != "" ? brandName : categoryName)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
            if responseData.count > 0 {
                for item in responseData {
                    let fieldsArray = item.fields
                    self.OffersPercent = fieldsArray[0].intValue
                }
            }
            self.tableView.reloadData()
        }
    }
}


