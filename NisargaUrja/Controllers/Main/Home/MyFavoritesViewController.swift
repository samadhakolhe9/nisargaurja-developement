//
//  MyFavoritesViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 21/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class MyFavoritesViewController: UICollectionViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Your Favorites"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = UICollectionViewFlowLayout()
         layout.scrollDirection = .vertical
        collectionView.collectionViewLayout = layout
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 50, right: 10)
        
         let width = collectionView.frame.size.width
         if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumInteritemSpacing = 5
            layout.minimumLineSpacing = 5
            layout.itemSize = CGSize(width: width/3 - 12, height: width/2.2)
            layout.invalidateLayout()
        }
        
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return MyFavoriteArray.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FavoriteCell
        cell.lblFruitName.text = MyFavoriteArray[indexPath.row].productName
        cell.lblOrganizeBy.text = MyFavoriteArray[indexPath.row].brandName
        cell.btnFavorite.isSelected = true
        
        cell.btnFavorite.setImage(UIImage(named: "empty-heart")?.imageWithColor(color: .red), for: .normal)
        cell.btnFavorite.setImage(UIImage(named: "heart")?.imageWithColor(color: .red), for: .selected)
        let imagePath = MyFavoriteArray[indexPath.row].productPackageImage
        let imagePathString = imagePath!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let url = URL(string: imagePathString)!
        let resource = ImageResource(downloadURL: url)

        cell.imgFruits.image = UIImage(named: "splash_logo")
        cell.imgFruits.alpha = 0.3
        cell.activityIndicator.isHidden = false
        cell.activityIndicator.startAnimating()
        KingfisherManager.shared.retrieveImage(with: resource,  options: nil) { result in
            cell.activityIndicator.isHidden = true
            cell.activityIndicator.stopAnimating()
          switch result {
            case .success(let value):
                cell.imgFruits.image = value.image
                cell.imgFruits.alpha = 1.0
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                  cell.imgFruits.roundCorners(corners: [.topLeft, .topRight], radius: 5)
                }
            case .failure(let error):
                print("Error: \(error)")
                cell.imgFruits.image = UIImage(named: "splash_logo")
                cell.imgFruits.alpha = 0.3
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                  cell.imgFruits.roundCorners(corners: [.topLeft, .topRight], radius: 5)
                }
          }
        //  cell.imgFruits.roundCorners(corners: [.topLeft, .topRight], radius: 5)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var cellSize: CGSize = collectionView.bounds.size
        cellSize.width = 135 //cellSize.width/3-15
        cellSize.height = 220 //cellSize.width/2.2
        
        return cellSize
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let productViewController = self.storyboard?.instantiateViewController(withIdentifier: "idProductDetailsViewController") as! ProductDetailsViewController
        productViewController.prodUID = MyFavoriteArray[indexPath.row].productUID
        self.navigationController?.pushViewController(productViewController, animated: true)
    }
}
