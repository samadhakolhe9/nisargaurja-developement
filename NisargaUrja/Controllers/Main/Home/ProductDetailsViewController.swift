//
//  ProductDetailsViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 20/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit



class ProductDetailsViewController: UITableViewController, TagListViewDelegate {

    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblOfferedBy: UILabel!
    @IBOutlet weak var lblQuoteMessage: UILabel!
    @IBOutlet weak var lblRelatedRecipes: UILabel!
    @IBOutlet weak var relatedRecipesCollectionView: UICollectionView!
    @IBOutlet var StretchyHeaderView: StretchyTableHeaderView!
    @IBOutlet weak var tagsView: TagListView!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var variationCollectionView: UICollectionView!
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet var subTotalView: UIView!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblCredits: UIButton!
    
    
    var CartListArray:[CartData] = []
    var ProductDetailsArray:[ProductDetails] = []
    struct ProductDetails {
        var productUID: String!
        var productName: String!
        var productDescription: String!
        var productImageURL: String!
        var brandName: String!
        var Date: String!
        var variationUID: [String]!
        var variationTitle: [String]!
        var discountPrice: [Double]!
        var MRP: [String]!
        var inStock: [Bool]!
        var isFavourite: Bool!
        var tags: [String]!
    }
    var RelatedRecipeArray:[RelatedRecipe] = []
    struct RelatedRecipe {
        var recipeUID: String!
        var recipeTitle: String!
        var recipeImageURL: String!
        var authorBrandName: String!
        var isFavourite: Bool!
    }
    var prodUID = ""
    var youHaveCartItems = false
    let backButton = UIButton(type: .custom)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
     //   self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        backButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backButton.setImage(UIImage(named: "backArrow")!.imageWithColor(color: .white
        ), for: UIControl.State.normal)
        backButton.addTarget(self, action: #selector(self.backButtonAction), for: UIControl.Event.touchUpInside)
        backButton.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        backButton.contentMode = .center
        let back = UIBarButtonItem(customView: backButton)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.leftBarButtonItems = [back]
        
        self.navigationController?.navigationBar.setNavbar(backgroundColorAlpha: 0.0, newColor: .white)
    }
    
    @objc func backButtonAction(_ sender: UIButton) {
        self.navigationController?.navigationBar.setNavbar(backgroundColorAlpha: 1.0, newColor: .white)
        self.navigationController?.popViewController(animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 140
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        
        StretchyHeaderView = StretchyTableHeaderView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 200))
        StretchyHeaderView.imageView.alpha = 0.3
        StretchyHeaderView.imageView.image = UIImage(named: "splash_logo")
        self.tableView.tableHeaderView = StretchyHeaderView
        
        setupCollectionView(collectionView: relatedRecipesCollectionView, itemSpacing: 5)
        setupVerticalCollectionView(collectionView: variationCollectionView, itemSpacing: 5)
        NotificationCenter.default.addObserver(self, selector: #selector(cartUpdate),
                                               name: NSNotification.Name(rawValue: "UpdateCartTotal"), object: nil)
        
        
        getCartItems()
        getRelatedRecipes()
        getCartTotal(isLoader: false)
        NotificationCenter.default.addObserver(self, selector:#selector(self.reloadPage), name: UIApplication.willEnterForegroundNotification, object: UIApplication.shared)
    }
    
    
    @objc func reloadPage(notification: NSNotification) {
        getCartItems()
        getRelatedRecipes()
        getCartTotal(isLoader: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let navigationBar = navigationController!.navigationBar
        navigationBar.reset()
        
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: TagListViewDelegate
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
        //tagView.isSelected = !tagView.isSelected
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let headerView = self.tableView.tableHeaderView as! StretchyTableHeaderView
        headerView.scrollViewDidScroll(scrollView: scrollView)
        let denominator: CGFloat = 50 //your offset treshold
        let alpha = min(1, scrollView.contentOffset.y / denominator)
        
        backButton.setImage(UIImage(named: "backArrow")!.imageWithColor(color: alpha > 0.8 ? .black : .white), for: UIControl.State.normal)
        self.title = alpha > 0.8 ? "Product details" : ""
        self.navigationController?.navigationBar.setNavbar(backgroundColorAlpha: alpha, newColor: .white)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//loadData()
    }
    
    @objc func cartUpdate(notification: NSNotification) {
        getCartTotal(isLoader: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.navigationBar.setNavbar(backgroundColorAlpha: 1.0, newColor: .white)
    }
    
    @IBAction func btnWeightDropdownPressed(_ sender: UIButton) {
        
    }
    
    
    func setupCollectionView(collectionView: UICollectionView, itemSpacing: CGFloat) {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
         let width = collectionView.frame.size.width
         if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumInteritemSpacing = itemSpacing
            layout.minimumLineSpacing = itemSpacing
            layout.itemSize = CGSize(width: width/3 - itemSpacing, height: collectionView.frame.size.height)
            layout.invalidateLayout()
        }
    }
    
    func setupVerticalCollectionView(collectionView: UICollectionView, itemSpacing: CGFloat) {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        collectionView.collectionViewLayout = layout
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
         let width = collectionView.frame.size.width
         if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumInteritemSpacing = itemSpacing
            layout.minimumLineSpacing = itemSpacing
            layout.itemSize = CGSize(width: width/3 - itemSpacing, height: collectionView.frame.size.height)
            layout.invalidateLayout()
        }
    }
    
    @IBAction func FavoriteButtonAction(_ sender: UIButton) {
        let urlString = APIConstants.setFavorites+"prodUID=\(prodUID)&phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    self.btnFavorite.isSelected = fieldsArray[KeysArray["status"].intValue].boolValue
                }
            }
        }
    }
    
    func setAllDetails() {
        tagsView.delegate = self
        tagsView.textFont = UIFont(name: "Lato-Regular", size: 14)!
        tagsView.alignment = .center
        tagsView.removeAllTags()
        tagsView.cornerRadius = 10
        if ProductDetailsArray.count > 0 {
            for tag in ProductDetailsArray[0].tags {
                tagsView.addTag(tag)
            }

            self.btnFavorite.isSelected = ProductDetailsArray[0].isFavourite
            self.btnFavorite.setImage(UIImage(named: "empty-heart")?.imageWithColor(color: .red), for: .normal)
            self.btnFavorite.setImage(UIImage(named: "heart")?.imageWithColor(color: .red), for: .selected)
            lblProductName.text = ProductDetailsArray[0].productName
            lblOfferedBy.text = ("By "+ProductDetailsArray[0].brandName)//.uppercased()
            lblQuoteMessage.text = ProductDetailsArray[0].productDescription
            
            let imagePath = ProductDetailsArray[0].productImageURL
            let imagePathString = imagePath!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: imagePathString)!
            let resource = ImageResource(downloadURL: url)
            
            KingfisherManager.shared.retrieveImage(with: resource,  options: nil) { result in
                switch result {
                case .success(let value):
                    self.StretchyHeaderView.imageView.image = value.image
                    self.StretchyHeaderView.imageView.alpha = 1.0
                    //self.tableView.addScalableCover(with: value.image, maxHeight: 100)
                case .failure( _):
                    self.StretchyHeaderView.imageView.alpha = 0.3
                    self.StretchyHeaderView.imageView.image = UIImage(named: "splash_logo")
                    //self.tableView.addScalableCover(with: UIImage(named: "splash_logo")!, maxHeight: 100)
                }
            }
        }
        loadData()
    }
    
    func loadData() {
        self.variationCollectionView.reloadData()
        self.relatedRecipesCollectionView.reloadData()
        self.viewDidLayoutSubviews()
        self.tableView.reloadData()
        
    }
    
    func getCartTotal(isLoader: Bool) {
        if DefaultAddressArray != nil {
            let urlString = APIConstants.getcartTotals+"phoneNo=\(globalUserPhone)&addressUID=\(DefaultAddressArray.addressUID!)"
            ApiManager().getRequestApi(url: urlString, runLoader: isLoader, showError: false) { (responseData, success) in
                if responseData.count > 0 {
                    responseData.forEach { (item) in
                        let keysArray = item.fieldLookup
                        let fieldsArray = item.fields
                        let total = fieldsArray[keysArray["total"].intValue].stringValue
                        self.lblSubTotal.text = "SUBTOTAL: ₹ "+total
                        self.lblCredits.setTitle("You have: ₹ 0", for: .normal)
                        self.youHaveCartItems = true
                    }
                } else {
                    self.youHaveCartItems = false
                }
                self.getCreditsEarned()
                self.tableView.reloadData()
            }
        }
    }
    func getCreditsEarned() {
        let urlString = APIConstants.totalCreditPoints+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (responseData, success) in
            if responseData.count > 0 {
                responseData.forEach { (item) in
                    let keysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    let credits = fieldsArray[keysArray["totalCredits"].intValue].stringValue
                   // self.lblCredits.setTitle("You have: ₹ "+credits, for: .normal)
                }
            }
            self.tableView.reloadData()
        }
    }
    func getCartItems() {
        let urlString = APIConstants.getcartItem+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.CartListArray = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    
                    self.CartListArray.append(CartData(brandName: fieldsArray[KeysArray["brandName"].intValue].stringValue, productName: fieldsArray[KeysArray["productName"].intValue].stringValue, productPackageImage: fieldsArray[KeysArray["productPackageImage"].intValue].stringValue, variationUID: fieldsArray[KeysArray["variationUID"].intValue].stringValue, variationTitle: fieldsArray[KeysArray["variationTitle"].intValue].stringValue, quantity: fieldsArray[KeysArray["quantity"].intValue]["low"].stringValue, MRP: fieldsArray[KeysArray["MRP"].intValue].stringValue, discountedPrice: fieldsArray[KeysArray["discountedPrice"].intValue].stringValue))
                    
                    self.lblCredits.setTitle("\(self.CartListArray.count) items", for: .normal)
                }
                //CartRightButton.badge = "\(self.CartListArray.count)"
            }
            self.getProductDetails()
        }
    }
    
    func getProductDetails() {
        let urlString = APIConstants.getProductDetails+"prodUID=\(prodUID)&phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.ProductDetailsArray = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    
                    let commonProduct = Response.filter{$0.fields[$0.fieldLookup["productUID"].intValue].stringValue == fieldsArray[KeysArray["productUID"].intValue].stringValue}
                    
                    self.ProductDetailsArray.append(ProductDetails(productUID: fieldsArray[KeysArray["productUID"].intValue].stringValue, productName: fieldsArray[KeysArray["productName"].intValue].stringValue, productDescription: fieldsArray[KeysArray["productDescription"].intValue].stringValue, productImageURL: fieldsArray[KeysArray["productImageURL"].intValue].stringValue, brandName: fieldsArray[KeysArray["brandName"].intValue].stringValue, Date: fieldsArray[KeysArray["Date"].intValue].stringValue, variationUID: commonProduct.map{$0.fields[$0.fieldLookup["variationUID"].intValue].stringValue}, variationTitle: commonProduct.map{$0.fields[$0.fieldLookup["variationTitle"].intValue].stringValue}, discountPrice: commonProduct.map{$0.fields[$0.fieldLookup["discountPrice"].intValue].doubleValue}
                        , MRP: commonProduct.map{$0.fields[$0.fieldLookup["MRP"].intValue].stringValue}, inStock: commonProduct.map{$0.fields[$0.fieldLookup["inStock"].intValue].boolValue}, isFavourite: fieldsArray[KeysArray["isFavourite"].intValue].boolValue, tags: fieldsArray[KeysArray["tags"].intValue].arrayValue.map{$0.stringValue}))
                }
                self.ProductDetailsArray = self.ProductDetailsArray.filterDuplicates{$0.productUID == $1.productUID}
            }
            print("Details: ", self.ProductDetailsArray)
            self.loadData()
            self.setAllDetails()
        }
    }
    
    func getRelatedRecipes() {
        let urlString = APIConstants.getRelatedRecipes+"prodUID=\(prodUID)&phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.RelatedRecipeArray = []
            if Response.count > 0 {
                self.lblRelatedRecipes.text = "Related Recipes"
                self.lblRelatedRecipes.font = UIFont.init(name: "Lato-Regular", size: 18.0)
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    
                    self.RelatedRecipeArray.append(RelatedRecipe(recipeUID: fieldsArray[KeysArray["recipeUID"].intValue].stringValue, recipeTitle: fieldsArray[KeysArray["recipeTitle"].intValue].stringValue, recipeImageURL: fieldsArray[KeysArray["recipeImageURL"].intValue].stringValue, authorBrandName: fieldsArray[KeysArray["authorBrandName"].intValue].stringValue, isFavourite: fieldsArray[KeysArray["isFavourite"].intValue].boolValue))
                }
            } else {
                self.lblRelatedRecipes.font = UIFont.init(name: "Lato-Regular", size: 14.0)
                self.lblRelatedRecipes.text = "No more related recipes"
            }
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        let height = variationCollectionView.collectionViewLayout.collectionViewContentSize.height
//        collectionHeightConstraint.constant = height
        self.view.layoutIfNeeded()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let variationeHeight = iconsCellHeight
        return [1,2].contains(indexPath.row) ? UITableView.automaticDimension : indexPath.row == 3 ? (iconsCellHeight > 150 ? variationeHeight-150 : iconsCellHeight) :  super.tableView(tableView, heightForRowAt: indexPath)  // [4,5].contains(indexPath.row) && RelatedRecipeArray.count == 0 ? 0 :
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        subTotalView.frame = CGRect(x:0, y: 0, width:tableView.frame.size.width,height:50)
        subTotalView.isHidden = !youHaveCartItems
        return subTotalView
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return youHaveCartItems ? 50 : 0
    }
    
    private var iconsCellHeight: CGFloat = 500

    func updateTable(table: UITableView, withDuration duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: { () -> Void in
            table.beginUpdates()
            table.endUpdates()
        })
    }

    override func viewWillLayoutSubviews() {
        let collectionViewContentHeight = variationCollectionView.contentSize.height
        if collectionViewContentHeight + 150 != iconsCellHeight {
            iconsCellHeight = collectionViewContentHeight + 150
            updateTable(table: tableView, withDuration: 0.2)
        }
    }
}

extension ProductDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == relatedRecipesCollectionView ? RelatedRecipeArray.count : ProductDetailsArray.count > 0 ? ProductDetailsArray[0].variationUID.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == relatedRecipesCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RelatedRecipesCell
          
            cell.lblProductName.text = RelatedRecipeArray[indexPath.row].recipeTitle
            cell.lblOrganizeBy.text = RelatedRecipeArray[indexPath.row].authorBrandName
            cell.btnFavorites.isSelected = RelatedRecipeArray[indexPath.row].isFavourite
            
            cell.btnFavorites.setImage(UIImage(named: "empty-heart")?.imageWithColor(color: .red), for: .normal)
            cell.btnFavorites.setImage(UIImage(named: "heart")?.imageWithColor(color: .red), for: .selected)
            
            let imagePath = RelatedRecipeArray[indexPath.row].recipeImageURL
            let imagePathString = imagePath!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: imagePathString)!
            let resource = ImageResource(downloadURL: url)

            cell.imgProduct.image = UIImage(named: "splash_logo")
            cell.imgProduct.alpha = 0.3
            cell.activityIndicator.isHidden = false
            cell.activityIndicator.startAnimating()
            KingfisherManager.shared.retrieveImage(with: resource,  options: nil) { result in
                cell.activityIndicator.isHidden = true
                cell.activityIndicator.stopAnimating()
              switch result {
                case .success(let value):
                    cell.imgProduct.image = value.image
                    cell.imgProduct.alpha = 1.0
                case .failure(let error):
                    print("Error: \(error)")
                    cell.imgProduct.image = UIImage(named: "splash_logo")
                    cell.imgProduct.alpha = 0.3
              }
            }
            cell.imgProduct.contentMode = .scaleAspectFill //scaleToFill //
            cell.imgProduct.clipsToBounds = true
              let height = variationCollectionView.collectionViewLayout.collectionViewContentSize.height
            collectionHeightConstraint.constant = height
            return cell
        }
        
        if collectionView == variationCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! VariationCell
          //  cell.bgView.applyCommonShadow(borderColor: .darkGray)
            cell.isAbleToChange = false
            cell.btnAddToCart.applyCommonShadow(shadowColor: AppCommonColor)
            cell.lblVariationTitle.text = ProductDetailsArray[0].variationTitle[indexPath.row]
            cell.lblMRP.attributedText = "MRP: ₹\(ProductDetailsArray[0].MRP![indexPath.row])".strikeThrough()
            cell.lblDiscountedPrice.text = String(format:"Our Price: ₹ %.1f", ProductDetailsArray[0].discountPrice[indexPath.row])
            
            cell.variationUID = ProductDetailsArray[0].variationUID.count > 0 ? ProductDetailsArray[0].variationUID[indexPath.row] : ""
            let cartValue = self.CartListArray.filter{$0.variationUID == cell.variationUID}
            
            cell.stprProducctQuantity.value = cartValue.count > 0 ? Double(Int("\(cartValue[0].quantity!)")!) : 0.0
            cell.quantityCount = cell.stprProducctQuantity.value
            cell.btnAddToCart.isHidden = cell.stprProducctQuantity.value == 0.0 ? false : true
            cell.stprProducctQuantity.isHidden = !cell.btnAddToCart.isHidden
            cell.isAbleToChange = true
            
            cell.bgViewWidthConstraint.constant = cell.bounds.size.width

            cell.btnAddToCart.backgroundColor = ProductDetailsArray[0].inStock[indexPath.row] ? AppCommonColor :  #colorLiteral(red: 0.8980392157, green: 0.5333333333, blue: 0.3137254902, alpha: 1)
            cell.btnAddToCart.layer.cornerRadius = 12.5
            cell.btnAddToCart.layer.borderWidth = 0.2
            cell.btnAddToCart.layer.borderColor = UIColor.gray.cgColor
            cell.btnAddToCart.layer.masksToBounds = false
            cell.btnAddToCart.layer.shadowRadius = 5
            cell.btnAddToCart.layer.shadowOpacity = 0.5
            cell.btnAddToCart.layer.shadowColor = UIColor.gray.cgColor
            cell.btnAddToCart.layer.shadowOffset = CGSize(width: 0 , height:2)
            
            
            if ProductDetailsArray[0].inStock[indexPath.row] {
                cell.btnAddToCart.isEnabled = true
                cell.btnAddToCart.alpha = 1.0
                cell.btnAddToCart.setTitle("ADD", for: .normal)
                cell.addButtonWidthConstraint.constant = 70
                cell.btnAddToCart.backgroundColor = AppCommonColor
                cell.btnAddToCart.layer.borderColor = UIColor.gray.cgColor
                cell.btnAddToCart.layer.shadowColor = UIColor.gray.cgColor
            } else {
                cell.btnAddToCart.isEnabled = false
                cell.btnAddToCart.alpha = 0.5
                cell.btnAddToCart.isHidden = false
                cell.btnAddToCart.setTitle("OUT OF STOCK", for: .normal)
                cell.addButtonWidthConstraint.constant = 100
                
                cell.btnAddToCart.backgroundColor = .clear
                cell.btnAddToCart.layer.borderWidth = 2
                cell.btnAddToCart.layer.borderColor = #colorLiteral(red: 0.8980392157, green: 0.5333333333, blue: 0.3137254902, alpha: 1)
                cell.btnAddToCart.layer.shadowColor = #colorLiteral(red: 0.8980392157, green: 0.5333333333, blue: 0.3137254902, alpha: 1)
                cell.btnAddToCart.setTitleColor(#colorLiteral(red: 0.8980392157, green: 0.5333333333, blue: 0.3137254902, alpha: 1), for: .normal)
            }

            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellSize: CGSize = collectionView.bounds.size
        if collectionView == variationCollectionView {
            let CellCount = ProductDetailsArray.count > 0 ? ProductDetailsArray[0].variationUID.count : 0
            if indexPath.row == CellCount-1 {
                if indexPath.row % 2  == 0 {
                    let spaceBetweenCell :CGFloat = 5
                    let screenWidth = collectionView.bounds.size.width - CGFloat(2 * spaceBetweenCell)
                  return CGSize(width: screenWidth , height: cellSize.height) // all Width and  same previous height
                }else {
                  return CGSize(width: cellSize.width/2-5, height: cellSize.height)
                }
            }
           return CGSize(width: cellSize.width/2-5, height: cellSize.height)
        }
        cellSize.width = cellSize.width/3-5
        return CGSize(width: cellSize.width, height: cellSize.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == variationCollectionView {
        } else {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "idRecipeDetailsViewController") as! RecipeDetailsViewController
            controller.RecipeUID = RelatedRecipeArray[indexPath.row].recipeUID
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
//    let spaceBetweenCell :CGFloat = 0
//    let screenWidth = UIScreen.main.bounds.size.width - CGFloat(2 * spaceBetweenCell)
//    let totalSpace = spaceBetweenCell * 1.0
//    let CellCount = ProductDetailsArray.count > 0 ? ProductDetailsArray[0].variationUID.count : 0
//    if indexPath.row == 2 {
//      // check if last cell is odd
//      if indexPath.row % 2  == 1 {
//        return CGSize(width: screenWidth , height: (screenWidth-totalSpace)/4) // all Width and  same previous height
//      }else {
//        return CGSize(width: cellSize.width/2, height: cellSize.height)
//      }
}


