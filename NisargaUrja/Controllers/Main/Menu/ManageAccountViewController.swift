//
//  ManageAccountViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 14/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class ManageAccountViewController: UITableViewController {

    @IBOutlet weak var lblPersonalDetails: UILabel!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var txtFirstName: SKCustomTextField!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var txtLastName: SKCustomTextField!
    @IBOutlet weak var lblPhoneNo: UILabel!
    @IBOutlet weak var txtPhoneNo: SKCustomTextField!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var txtEmail: SKCustomTextField!
    @IBOutlet weak var btnUpdateProfile: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Manage Account"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)

        let menuButton = UIButton(type: .custom)
        menuButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        menuButton.setImage(UIImage(named: "ic_menu_black_24dp")!.imageWithColor(color: .black), for: UIControl.State.normal)
        menuButton.addTarget(self, action: #selector(self.MenuButtonAction), for: UIControl.Event.touchUpInside)
        menuButton.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        menuButton.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        menuButton.contentMode = .center
        let menu = UIBarButtonItem(customView: menuButton)

        self.navigationItem.leftBarButtonItems = [menu]
    }
    
    @objc func MenuButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    func updateUI() {
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.btnUpdateProfile.layer.cornerRadius = 5
        self.btnUpdateProfile.clipsToBounds = true
        txtFirstName.addBottomBorder()
        txtLastName.addBottomBorder()
        txtPhoneNo.addBottomBorder()
        txtEmail.addBottomBorder()
        
        txtFirstName.text = LocalDB.shared.UserInfo.firstName
        txtLastName.text = LocalDB.shared.UserInfo.lastName
        txtPhoneNo.text = LocalDB.shared.UserInfo.phoneNo
        txtEmail.text = LocalDB.shared.UserInfo.email
    }

    @IBAction func btnUpdateProfilePressed(_ sender: UIButton) {
        chkVal = ""
        validateTextFieldWithRedLine([txtFirstName,txtLastName, txtEmail], color: .lightGray)
        if(chkVal == "")
        {
            if isValidEmail(txtEmail) {

                var components = ApiManager.components
                components.path = "/api/v1/register-customer"

                let phoneNo = URLQueryItem(name: "phoneNo", value: txtPhoneNo.text)
                let firstName = URLQueryItem(name: "firstName", value: txtFirstName.text)
                let lastName = URLQueryItem(name: "lastName", value: txtLastName.text)
                let email = URLQueryItem(name: "email", value: txtEmail.text)
                let isDeliverable = URLQueryItem(name: "isDeliverable", value: "true")
                
                components.queryItems = [phoneNo, firstName,lastName,email, isDeliverable]
                
                print("Generated URL: \(components.url!)")
            } else {
                ReusableManager.sharedInstance.showAlert(alertString: "Email id not valid!")
            }
        }
    }
    
    //TODO:: Update User Details
    func updateUserDetails() {
        
        var components = ApiManager.components
        components.path = "/api/v1/register-customer"

        let phoneNo = URLQueryItem(name: "phoneNo", value: txtPhoneNo.text)
        let firstName = URLQueryItem(name: "firstName", value: txtFirstName.text)
        let lastName = URLQueryItem(name: "lastName", value: txtLastName.text)
        let email = URLQueryItem(name: "email", value: txtEmail.text)
        let isDeliverable = URLQueryItem(name: "isDeliverable", value: "true")
        
        components.queryItems = [phoneNo, firstName,lastName,email, isDeliverable]
        
        print("Generated URL: \(components.url!)")

        ApiManager().getRequestApi(url: components.url!.absoluteString, runLoader: true, showError: false) { (response, success) in
            
            if response.count > 0 {
                debugPrint("Values from Response \(response)")
//                for response in responseData {
//                    //print("Values from Response \(responseData)")
//                }
            }
//            self.tableView.reloadData()
        }
        
    }
    
    func getUserDInfo() {
        let urlString = APIConstants.getUserInfo+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            if Response.count > 0 {
                let KeysArray = Response[0].fieldLookup
                let fieldsArray = Response[0].fields
                LocalDB.shared.UserInfo = User(customerUID: fieldsArray[KeysArray["customerUID"].intValue].stringValue, firstName: fieldsArray[KeysArray["firstName"].intValue].stringValue, lastName: fieldsArray[KeysArray["lastName"].intValue].stringValue, phoneNo: fieldsArray[KeysArray["phoneNo"].intValue].stringValue, email: fieldsArray[KeysArray["email"].intValue].stringValue, isValidated: fieldsArray[KeysArray["isValidated"].intValue].stringValue)
            }
        }
    }
}

//TODO:: Function to draw Green line at bottom of TextView
extension UITextField {
    func addBottomBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = UIColor.white.cgColor
        bottomLine.borderColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        bottomLine.borderWidth = 0.5
        borderStyle = .none
        layer.addSublayer(bottomLine)
    }
}
