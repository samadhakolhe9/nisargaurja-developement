//
//  ManageAddressViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 15/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit
import CoreLocation

class ManageAddressViewController: UITableViewController {
    
    @IBOutlet weak var btnAddNewAddress: UIButton!

    var YourAddressData:[Address] = []
       struct Address {
           var addressUID: String!
           var title: String!
           var house: String!
           var address: String!
           var pincode: String!
           var latitude: String!
           var longitude: String!
           var isDeliverable: Bool!
           var isBillingAddress: Bool!
           var isDefaultAddress: Bool!
       }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Manage Address"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)

        let menuButton = UIButton(type: .custom)
        menuButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        menuButton.setImage(UIImage(named: "ic_menu_black_24dp")!.imageWithColor(color: .black), for: UIControl.State.normal)
        menuButton.addTarget(self, action: #selector(self.MenuButtonAction), for: UIControl.Event.touchUpInside)
        menuButton.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        menuButton.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        menuButton.contentMode = .center
        let menu = UIBarButtonItem(customView: menuButton)

        self.navigationItem.leftBarButtonItems = [menu]
    }
    
    @objc func MenuButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        btnAddNewAddress.layer.cornerRadius = 5
        btnAddNewAddress.clipsToBounds = true
        
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getAllAddress()
    }
    
    @IBAction func btnAddNewAddressPressed(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "idSetDeliveryLocationController") as! SetDeliveryLocationController
        controller.isFromRegistration = false
        controller.isChangeAddress = false
        
        userRegisterParams["phoneNo"] = LocalDB.shared.UserInfo.phoneNo
        userRegisterParams["firstName"] = LocalDB.shared.UserInfo.firstName
        userRegisterParams["lastName"] = LocalDB.shared.UserInfo.lastName
        userRegisterParams["title"] = ""
        userRegisterParams["address"] = ""
        userRegisterParams["house"] = ""
        userRegisterParams["pincode"] = ""
        userRegisterParams["lat"] = ""
        userRegisterParams["lng"] = ""
        userRegisterParams["isDeliverable"] = ""
        
        setupReferenceUITabBarController().navigationController?.pushViewController(controller, animated: true)
    }
    
    func getAllAddress() {
        let urlString = APIConstants.getAddresses+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.YourAddressData = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    self.YourAddressData.append(Address(addressUID: fieldsArray[KeysArray["addressUID"].intValue].stringValue, title: fieldsArray[KeysArray["title"].intValue].stringValue, house: fieldsArray[KeysArray["house"].intValue].stringValue, address: fieldsArray[KeysArray["address"].intValue].stringValue, pincode: fieldsArray[KeysArray["pincode"].intValue]["low"].stringValue, latitude: fieldsArray[KeysArray["latitude"].intValue].stringValue, longitude: fieldsArray[KeysArray["longitude"].intValue].stringValue, isDeliverable: fieldsArray[KeysArray["isDeliverable"].intValue].boolValue, isBillingAddress: fieldsArray[KeysArray["isBillingAddress"].intValue].boolValue, isDefaultAddress: fieldsArray[KeysArray["isDefaultAddress"].intValue].boolValue))
                    
                }
            }
            self.tableView.reloadData()
        }
    }
}
//TODO:: TableView Delegate and DataSource Methods
extension ManageAddressViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return YourAddressData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? AddressCell else {
            return UITableViewCell()
        }
        
        
        cell.btnAddressTitle.isSelected = YourAddressData[indexPath.row].title != "Home"
        cell.btnAddressTitle.setTitle(YourAddressData[indexPath.row].title, for: .normal)
        cell.btnAddressTitle.setTitle(YourAddressData[indexPath.row].title, for: .selected)
        cell.btnServiceStatus.isSelected = YourAddressData[indexPath.row].isDeliverable
        cell.lblAddress.text = YourAddressData[indexPath.row].house + "\n" + YourAddressData[indexPath.row].address
        cell.btnSetAsDefault.isHidden = YourAddressData[indexPath.row].isDefaultAddress
        cell.lblDefaultAddressText.isHidden = !YourAddressData[indexPath.row].isDefaultAddress
        cell.addressLabelTopConstraint.constant = cell.lblDefaultAddressText.isHidden ? 10 : 25
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(DeleteAddressAction), for: .touchUpInside)
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(EditAddressAction), for: .touchUpInside)
        cell.btnSetAsDefault.tag = indexPath.row
        cell.btnSetAsDefault.addTarget(self, action: #selector(DefaultAddressAction), for: .touchUpInside)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
            let vw = UIView()
        vw.backgroundColor = UIColor.white
        btnAddNewAddress.frame = CGRect(x:10,y: 0 ,width:250,height:30)
        vw.addSubview(btnAddNewAddress)
        return vw
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    
    @objc func EditAddressAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "idSetDeliveryLocationController") as! SetDeliveryLocationController
        controller.isFromRegistration = false
        controller.isChangeAddress = true
        
        userRegisterParams["phoneNo"] = LocalDB.shared.UserInfo.phoneNo
        userRegisterParams["firstName"] = LocalDB.shared.UserInfo.firstName
        userRegisterParams["lastName"] = LocalDB.shared.UserInfo.lastName
        userRegisterParams["title"] = ""
        userRegisterParams["address"] = ""
        userRegisterParams["house"] = ""
        userRegisterParams["pincode"] = ""
        userRegisterParams["lat"] = ""
        userRegisterParams["lng"] = ""
        userRegisterParams["isDeliverable"] = ""
        userRegisterParams["addressUID"] = YourAddressData[sender.tag].addressUID
        
        setupReferenceUITabBarController().navigationController?.pushViewController(controller, animated: true)
        
//        let controller = self.storyboard?.instantiateViewController(withIdentifier: "idAddressViewController") as! AddressViewController
//        controller.isEditAddress = false
//        userRegisterParams["phoneNo"] = LocalDB.shared.UserInfo.phoneNo
//        userRegisterParams["firstName"] = LocalDB.shared.UserInfo.firstName
//        userRegisterParams["lastName"] = LocalDB.shared.UserInfo.lastName
//        userRegisterParams["title"] = YourAddressData[sender.tag].title
//        userRegisterParams["address"] = YourAddressData[sender.tag].address
//        userRegisterParams["house"] = YourAddressData[sender.tag].house
//        userRegisterParams["pincode"] = YourAddressData[sender.tag].pincode
//        userRegisterParams["lat"] = YourAddressData[sender.tag].latitude
//        userRegisterParams["lng"] = YourAddressData[sender.tag].longitude
//        userRegisterParams["isDeliverable"] = YourAddressData[sender.tag].isDeliverable
//
//        let latitude = (YourAddressData[sender.tag].latitude as NSString).doubleValue
//        let longitude = (YourAddressData[sender.tag].longitude as NSString).doubleValue
//
//        controller.locValue = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func DeleteAddressAction(_ sender: UIButton) {
        if YourAddressData[sender.tag].isDefaultAddress {
            ReusableManager.sharedInstance.showAlert(alertString: "You are not able to delete default address!")
        } else {
            let alert = UIAlertController(title: "Delete Confirmation", message: "Are you sure you want to delete the Address?", preferredStyle: .alert)
             alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                 let addressUID = self.YourAddressData[sender.tag].addressUID
                 let urlString = APIConstants.deleteAddress+"addressUID=\(addressUID!)"
                 ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
                     self.getAllAddress()
                     ReusableManager.sharedInstance.showAlert(alertString: "Address deleted successfully.")
                 }
             }))
             alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
             self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func DefaultAddressAction(_ sender: UIButton) {
        let addressUID = YourAddressData[sender.tag].addressUID
        let urlString = APIConstants.setasdefaultAddress+"phoneNo=\(globalUserPhone)&addressUID=\(addressUID!)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.YourAddressData = []
            if Response.count > 0 {
                self.getAllAddress()
                ReusableManager.sharedInstance.showAlert(alertString: "Set default address successfully.")
            }
        }
    }
}
