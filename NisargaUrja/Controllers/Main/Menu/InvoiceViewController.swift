//
//  OrderDetailViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 17/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit
import SwiftyJSON
import MessageUI

class InvoiceViewController: UITableViewController {

    @IBOutlet weak var lblBillingAddress: UILabel!
    @IBOutlet weak var lblShippingAddress: UILabel!
    @IBOutlet weak var lblBillingAddressValue: UILabel!
    @IBOutlet weak var lblShippingAddressValue: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblDeliveryDate: UILabel!
    @IBOutlet weak var lblOrderDateValue: UILabel!
    @IBOutlet weak var lblDeliveryDateValue: UILabel!
    @IBOutlet weak var lblPaid: UILabel!
    @IBOutlet weak var lblDelivered: UILabel!
    @IBOutlet weak var lblNameHeading: UILabel!
    @IBOutlet weak var lblQtyHeading: UILabel!
    @IBOutlet weak var lblRateHeading: UILabel!
    @IBOutlet weak var lblTotalHeading: UILabel!
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var lblSubtotalValue: UILabel!
    @IBOutlet weak var lblLoyaltyPoints: UILabel!
    @IBOutlet weak var lblCreditsUsed: UILabel!
    @IBOutlet weak var lblCouponCode: UILabel!
    @IBOutlet weak var lblTotalDiscount: UILabel!
    @IBOutlet weak var lblDeliveryCharges: UILabel!
    @IBOutlet weak var lblLoyaltyPointsValue: UILabel!
    @IBOutlet weak var lblCreditsUsedValue: UILabel!
    @IBOutlet weak var lblCouponCodeValue: UILabel!
    @IBOutlet weak var lblTotalDiscountValue: UILabel!
    @IBOutlet weak var lblDeliveryChargesValue: UILabel!
//    @IBOutlet weak var lblUsedLoyaltyPoints: UILabel!
//    @IBOutlet weak var lblAppliedCouponCode: UILabel!
//    @IBOutlet weak var lblAppliedTotalDiscount: UILabel!
//    @IBOutlet weak var lblFreeDeliveryCharges: UILabel!
//    @IBOutlet weak var lblBillPaymentStatus: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    @IBOutlet weak var lblHSNSAC: UILabel!
    @IBOutlet weak var lblHSNSACValue: UILabel!
    @IBOutlet weak var lblCompositionTaxable: UILabel!
    @IBOutlet weak var btnEmailInvoice: UIButton!
    @IBOutlet weak var lblOfferedUsed: UILabel!
    @IBOutlet weak var lblOffered: UILabel!

    var orderDetails: [OrderData] = []
    var selectedOrderUID = ""
    struct OrderData {
        var orderUID: String?
        var orderSubtotal: String?
        var orderTotal: String?
        var orderDate: String?
        var deliveryDate: String?
        var orderStatus: String?
        var deliveryStatus: String?
        var shippingAddress: String?
        var house: String?
        var title: String?
        var billingAddress: String?
        var deliveryCharges: String?
        var productName: [String]?
        var quantityPurchased: [String]?
        var variationTitle: [String]?
        var discountPrice: [String]?
        var couponUsed: String?
        var couponDiscount: String?
        var loyaltyPointsUsed: String?
        var creditsUsed: String?
        var offerTitle: String?
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "OrderDetailsCell", bundle: nil), forCellReuseIdentifier: "OrderDetailsCell")
        self.tableView.estimatedRowHeight = 40
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.tableFooterView = UIView()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)

        let menuButton = UIButton(type: .custom)
        menuButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        menuButton.setImage(UIImage(named: "backArrow")!.imageWithColor(color: .black), for: UIControl.State.normal)
        menuButton.addTarget(self, action: #selector(self.MenuButtonAction), for: UIControl.Event.touchUpInside)
        menuButton.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        menuButton.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        menuButton.contentMode = .center
        let menu = UIBarButtonItem(customView: menuButton)

        self.navigationItem.leftBarButtonItems = [menu]
        setupUI()
        getOrderDetails()
    }
    
    @objc func MenuButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getOrderDetails()
    }
    
    func setupUI() {
        lblPaid.layer.borderWidth = 0.5
        lblPaid.layer.borderColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        lblPaid.layer.cornerRadius = 5
        lblPaid.clipsToBounds = true
        lblDelivered.layer.borderWidth = 0.5
        lblDelivered.layer.borderColor = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1).cgColor
        lblDelivered.layer.cornerRadius = 5
        lblDelivered.clipsToBounds = true
        btnEmailInvoice.layer.cornerRadius = 5
        btnEmailInvoice.clipsToBounds = true
    }
    
    @IBAction func btnEmailInvoicePressed(_ sender: UIButton) {
        let pdfFilePath = self.tableView.exportAsPdfFromView(fileName: "invoice.pdf")
        print("PDF File Path: \(pdfFilePath)")
        
        if MFMailComposeViewController.canSendMail() {
            let mailComposer = MFMailComposeViewController()
            mailComposer.setSubject("NisargaUrja Order Invoice.")
            mailComposer.setMessageBody("""
                Hi User,
                    I hope you’re well! Please see attached invoice for your purchase on . Don’t hesitate to reach out if you have any questions.
                Kind regards,
                   - Team Nisarga Urja
            """, isHTML: false)
            mailComposer.setToRecipients([LocalDB.shared.UserInfo.email])

            let url = URL(fileURLWithPath: pdfFilePath)
            
            do {
                let attachmentData = try Data(contentsOf: url)
                    mailComposer.addAttachmentData(attachmentData, mimeType: "application/pdf", fileName: "invoice")
                    mailComposer.mailComposeDelegate = self
                    mailComposer.setPreferredSendingEmailAddress("cultivate@nisargaurja.com")
                    self.present(mailComposer, animated: true, completion: nil)
            } catch let error {
                print("We have encountered error \(error.localizedDescription)")
            }
            
        } else {
            print("Email is not configured in settings app or we are not able to send an email")
        }
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.orderDetails.count == 0 {
            return 0
        }

        return section == 3 ? self.orderDetails[0].productName!.count+1 : super.tableView(tableView, numberOfRowsInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 3 && indexPath.row > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsCell", for: indexPath) as! OrderDetailsCell
            let newIndex = indexPath.row-1

            cell.lblProductName.text = orderDetails[0].productName![newIndex] + " - " + orderDetails[0].variationTitle![newIndex]
            cell.lblProductQty.text = orderDetails[0].quantityPurchased![newIndex]
            cell.lblProductRate.text = "₹ " + orderDetails[0].discountPrice![newIndex]
            
            let discountPrice = Double("\(orderDetails[0].discountPrice![newIndex])")!
            let quantityPurchased = Double("\(orderDetails[0].quantityPurchased![newIndex])")!
            
            cell.lblProductTotalPrice.text = "₹ " + String(discountPrice * quantityPurchased)
            
            
            return cell
        }
        return super.tableView(tableView, cellForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
           if indexPath.section == 3 && indexPath.row > 0 {
               let newIndexPath = IndexPath(row: 0, section: indexPath.section)
               return super.tableView(tableView, indentationLevelForRowAt: newIndexPath)
           }
           return super.tableView(tableView, indentationLevelForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 3 && indexPath.row > 0 ? UITableView.automaticDimension : super.tableView(tableView, heightForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func updateUI() {
        self.orderDetails.forEach { (detail) in
            lblBillingAddressValue.text         = detail.billingAddress
            lblShippingAddressValue.text        = detail.shippingAddress
            let oDateString                     = detail.orderDate!
            let orderDate                       = oDateString.toDateString(inputDateFormat: "dd-MM-yyyy", ouputDateFormat: "d MMMM, yyyy")
            let dDateString                     = detail.orderDate!
            let deliveryDate                    = dDateString.toDateString(inputDateFormat: "dd-MM-yyyy", ouputDateFormat: "d MMMM, yyyy")
            lblOrderDateValue.text              = orderDate
            lblDeliveryDateValue.text           = deliveryDate
            lblSubtotalValue.text               = "₹ " + String(describing: detail.orderSubtotal!)
            lblTotalValue.text                  = "₹ " + String(describing: detail.orderTotal!)
            
            lblPaid.text                        = String(describing: detail.orderStatus!)
            lblDelivered.text                   = String(describing: detail.deliveryStatus!)
            setStatusBorder(label: lblPaid)
            setStatusBorder(label: lblDelivered)
            let loyaltyPointsUsed   = Double("\(orderDetails[0].loyaltyPointsUsed != "" ? orderDetails[0].loyaltyPointsUsed! : "0")")!
            let creditsUsed         = Double("\(orderDetails[0].creditsUsed != "" ? orderDetails[0].creditsUsed! : "0")")!
            let couponDiscount      = Double("\(orderDetails[0].couponDiscount != "" ? orderDetails[0].couponDiscount! : "0")")!
            
            lblLoyaltyPointsValue.text          = "₹ " + String(describing: detail.loyaltyPointsUsed != "" ? detail.loyaltyPointsUsed! : "0")
            lblCreditsUsedValue.text            = "₹ " + String(describing: detail.creditsUsed != "" ? detail.creditsUsed! : "0")
            lblCouponCodeValue.text             = detail.couponUsed != "" ? detail.couponUsed : "-" //"₹ " + String(describing: detail.couponDiscount != "" ? detail.couponDiscount! : "0")
           // lblAppliedCouponCode.text           = detail.couponUsed != "" ? detail.couponUsed : "-" //String(describing: detail.couponUsed!)
            lblOfferedUsed.text                 = detail.offerTitle != "" ? detail.offerTitle : "-"
            lblTotalDiscountValue.text          = "₹ " + String(describing: loyaltyPointsUsed+creditsUsed+couponDiscount)
            lblDeliveryChargesValue.text        = "₹ " + String(describing: detail.deliveryCharges != "" ? detail.deliveryCharges! : "0")
           
            
            if lblDelivered.text?.uppercased() == "NOT DELIVERED" {
                lblDelivered.textColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1)
                lblDelivered.layer.borderColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1).cgColor
            } else {
                lblDelivered.textColor = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1)
                lblDelivered.layer.borderColor = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1).cgColor
            }
        }
    }
    
    func setStatusBorder(label: UILabel) {
        if label.text?.uppercased() == "NOT PAID" {
            label.textColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1)
            label.layer.borderColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1).cgColor
        } else if label.text?.uppercased() == "PAID" {
            label.textColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1)
            label.layer.borderColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        } else if label.text?.uppercased() == "DELIVERED" {
            label.textColor  = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1)
            label.layer.borderColor = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1).cgColor
        } else if label.text == "" {
            label.textColor  = UIColor.clear
            label.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    func getOrderDetails() {
        let urlString = APIConstants.getOrderDetails+"orderUID=\(selectedOrderUID)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.orderDetails = []
            if Response.count > 0 {
                Response.forEach { (item) in
                     let KeysArray = item.fieldLookup
                     let fieldsArray = item.fields
                    
                    let commonProduct = Response.filter{$0.fields[$0.fieldLookup["orderUID"].intValue].stringValue == fieldsArray[KeysArray["orderUID"].intValue].stringValue}
                    
                    self.orderDetails.append(OrderData(orderUID: fieldsArray[KeysArray["orderUID"].intValue].stringValue, orderSubtotal: fieldsArray[KeysArray["orderSubtotal"].intValue].stringValue, orderTotal: fieldsArray[KeysArray["orderTotal"].intValue].stringValue, orderDate: fieldsArray[KeysArray["orderDate"].intValue].stringValue, deliveryDate: fieldsArray[KeysArray["deliveryDate"].intValue].stringValue, orderStatus: fieldsArray[KeysArray["orderStatus"].intValue].stringValue, deliveryStatus: fieldsArray[KeysArray["deliveryStatus"].intValue].stringValue, shippingAddress: fieldsArray[KeysArray["shippingAddress"].intValue].stringValue, house: fieldsArray[KeysArray["house"].intValue].stringValue, title: fieldsArray[KeysArray["title"].intValue].stringValue, billingAddress: fieldsArray[KeysArray["billingAddress"].intValue].stringValue, deliveryCharges: fieldsArray[KeysArray["deliveryCharges"].intValue].stringValue, productName: commonProduct.map{$0.fields[$0.fieldLookup["productName"].intValue].stringValue}, quantityPurchased: commonProduct.map{$0.fields[$0.fieldLookup["quantityPurchased"].intValue]["low"].stringValue}, variationTitle: commonProduct.map{$0.fields[$0.fieldLookup["variationTitle"].intValue].stringValue}, discountPrice: commonProduct.map{$0.fields[$0.fieldLookup["discountPrice"].intValue].stringValue}, couponUsed: fieldsArray[KeysArray["couponUsed"].intValue].stringValue, couponDiscount: fieldsArray[KeysArray["couponDiscount"].intValue].stringValue, loyaltyPointsUsed: fieldsArray[KeysArray["loyaltyPointsUsed"].intValue].stringValue, creditsUsed: fieldsArray[KeysArray["creditsUsed"].intValue].stringValue, offerTitle: fieldsArray[KeysArray["offerTitle"].intValue].stringValue))
                }
                self.updateUI()
            }
            self.tableView.reloadData()
        }
    }
}

extension InvoiceViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
       if let _ = error {
          self.dismiss(animated: true, completion: nil)
       }
       switch result {
          case .cancelled:
          print("Cancelled")
          break
          case .sent:
          print("Mail sent successfully")
          break
          case .failed:
          print("Sending mail failed")
          break
          default:
          break
       }
       controller.dismiss(animated: true, completion: nil)
    }
}

extension String
{
    func toDateString( inputDateFormat inputFormat  : String,  ouputDateFormat outputFormat  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = outputFormat
        return dateFormatter.string(from: date!)
    }
}
