//
//  MyOrdersViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 13/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit
import SwiftyJSON

class MyOrdersViewController: UIViewController {

    @IBOutlet weak var tblMyOrders: UITableView!
    
    var YourOrderData:[Order] = []
    struct Order {
        var orderUID: String!
        var deliveryDate: String!
        var orderTotal: String!
        var orderStatus: String!
        var deliveryStatus: String!
        var productName: JSON!
        var variationTitle: JSON!
        var quantityPurchased: JSON!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "My Orders"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)

        let menuButton = UIButton(type: .custom)
        menuButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        menuButton.setImage(UIImage(named: "ic_menu_black_24dp")!.imageWithColor(color: .black), for: UIControl.State.normal)
        menuButton.addTarget(self, action: #selector(self.MenuButtonAction), for: UIControl.Event.touchUpInside)
        menuButton.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        menuButton.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        menuButton.contentMode = .center
        let menu = UIBarButtonItem(customView: menuButton)

        self.navigationItem.leftBarButtonItems = [menu]
    }
    
    @objc func MenuButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblMyOrders.delegate = self
        tblMyOrders.dataSource = self
        tblMyOrders.estimatedRowHeight = 50
        tblMyOrders.rowHeight = UITableView.automaticDimension
        self.tblMyOrders.separatorStyle = UITableViewCell.SeparatorStyle.none
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getAllOrders()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
//        self.navigationController?.navigationBar.isHidden = true
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func getAllOrders() {
        let urlString = APIConstants.getAllOrder+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.YourOrderData = []
            if Response.count > 0 {
                for item in Response {
                    let KeysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    
                    self.YourOrderData.append(Order(orderUID: fieldsArray[KeysArray["orderUID"].intValue].stringValue, deliveryDate: fieldsArray[KeysArray["deliveryDate"].intValue].stringValue, orderTotal: fieldsArray[KeysArray["orderTotal"].intValue].stringValue, orderStatus: fieldsArray[KeysArray["orderStatus"].intValue].stringValue, deliveryStatus: fieldsArray[KeysArray["deliveryStatus"].intValue].stringValue, productName: fieldsArray[KeysArray["productName"].intValue], variationTitle: fieldsArray[KeysArray["variationTitle"].intValue], quantityPurchased: fieldsArray[KeysArray["quantityPurchased"].intValue]))
                }
            }
            self.tblMyOrders.reloadData()
        }
    }
}

//TODO:: TableView Delegate and DataSource Methods
extension MyOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return YourOrderData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "idMyOrdersTableViewCell") as? MyOrdersTableViewCell else {
            return UITableViewCell()
        }
        
        //cell.lblOrderNumber.text = YourOrderData[indexPath.row].orderUID
        cell.lblPayementStatus.text = YourOrderData[indexPath.row].orderStatus.uppercased()
        cell.lblPaymentDeliveryStatus.text = YourOrderData[indexPath.row].deliveryStatus.uppercased()
        
        
        if cell.lblPaymentDeliveryStatus.text?.uppercased() == "NOT DELIVERED" {
            cell.lblPaymentDeliveryStatus.textColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1)
            cell.lblPaymentDeliveryStatus.layer.borderColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1).cgColor
        } else {
            cell.lblPaymentDeliveryStatus.textColor = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1)
            cell.lblPaymentDeliveryStatus.layer.borderColor = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1).cgColor
        }
        
        if cell.lblPayementStatus.text?.uppercased() == "NOT PAID" {
            cell.lblPayementStatus.textColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1)
            cell.lblPayementStatus.layer.borderColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1).cgColor
        } else if cell.lblPayementStatus.text?.uppercased() == "PAID" {
            cell.lblPayementStatus.textColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1)
            cell.lblPayementStatus.layer.borderColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        } else if cell.lblPayementStatus.text?.uppercased() == "DELIVERED" {
            cell.lblPayementStatus.textColor  = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1)
            cell.lblPayementStatus.layer.borderColor = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1).cgColor
        }
        
        let date = YourOrderData[indexPath.row].deliveryDate
        let weekString = getConvertedDate(string: date!).timestampString!
        let fullDateString = getDate(date: date!, format: "dd-MM-yyyy", reverseFormat: "dd MMM yyyy")
        cell.lblDeliveryDate.text = weekString + ",\n" + fullDateString

//        let productNameArray = YourOrderData[indexPath.row].productName
//        let variationTitle = YourOrderData[indexPath.row].variationTitle
//        let quantityPurchased = YourOrderData[indexPath.row].quantityPurchased
//        if productNameArray!.count > 0 {
//            var allItems = ""
//            for index in 0..<productNameArray!.count {
//                allItems = allItems + productNameArray![index].stringValue + " {\(variationTitle![index].stringValue)} x \(quantityPurchased![index]["low"].stringValue)" + ",\n"
//            }
//            cell.lblProductDetails.text = allItems
//        } else{
//            cell.lblProductDetails.text = "No items found."
//        }
        cell.lblTotalAmount.text = "TOTAL: ₹" + YourOrderData[indexPath.row].orderTotal
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "idInvoiceViewController") as! InvoiceViewController
        vc.title = "Order Detail" //YourOrderData[indexPath.row].orderUID
        vc.selectedOrderUID = YourOrderData[indexPath.row].orderUID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
