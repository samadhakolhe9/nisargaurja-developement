//
//  LeftViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 04/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit
import SwiftyJSON

var defaultAddress = ""

class LeftViewController: UITableViewController, ImageHeaderViewDelegate {
    
    var imageHeaderView: ImageHeaderView!
    
    @IBOutlet weak var lblCreditsAmount: UILabel!
    @IBOutlet weak var lblLoyaltyPoints: UILabel!
    var loyaltyPoints: String! = "0"
//    struct Points {
//        var loyaltyUID: [JSON]!
//        var loyaltyPointsEarned: [JSON]!
//        var earnedDate: [JSON]!
//        var expiryDate: [JSON]!
//        var currentEarned: String!
//    }
    var creditPoints: String! = "0"
//    struct Credits {
//        var creditsUID: [JSON]!
//        var creditsEarned: [JSON]!
//        var earnedDate: [JSON]!
//        var totalCreditsEarned: String!
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblCreditsAmount.layer.cornerRadius = 5
        lblCreditsAmount.clipsToBounds = true
        lblLoyaltyPoints.layer.cornerRadius = 5
        lblLoyaltyPoints.clipsToBounds = true
        
        tableView.estimatedSectionHeaderHeight = 100
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        tableView.contentInsetAdjustmentBehavior = .never
        
        getLoyaltyPointsEarned()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.imageHeaderView.lblUserName.text = "Hi,\n\(LocalDB.shared.UserInfo.firstName.capitalized) \(LocalDB.shared.UserInfo.lastName.capitalized)" 
        self.imageHeaderView.lblUserAddress.text = "+91 \(LocalDB.shared.UserInfo.phoneNo!)"
    }
    func didCloseSideMenu() {
        self.slideMenuController()?.closeLeft()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.layoutIfNeeded()
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //TODO:: Loading XIB for User Profile View on Top
        self.imageHeaderView = ImageHeaderView.loadNib()
        self.imageHeaderView.delegate = self
       // self.imageHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 250)
        return self.imageHeaderView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 { // My Orders
           let vc = self.storyboard?.instantiateViewController(withIdentifier: "idMyOrdersViewController") as! MyOrdersViewController
           self.navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.row == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "idManageAccountViewController") as! ManageAccountViewController
            self.navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.row == 2 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "idManageAddressViewController") as! ManageAddressViewController
            self.navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.row == 3 {
            
        } else if indexPath.row == 4 {
            
        } else if indexPath.row == 5 {
            self.slideMenuController()?.closeLeft()
            LocalDB.shared.removeUserMobileNumber()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "idLoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
            //self.slideMenuController()?.navigationController?.popViewController(animated: true)
        } else if indexPath.row == 6 {
            
        } else if indexPath.row == 8 {
            
        } else if indexPath.row == 9 {
            
        }
    }
    
    //TODO:: Side Menu Close Button Action
    @IBAction func WhatsappClickedAction(_ sender: UIButton) {
        let urlWhats = "whatsapp://send?phone=+918446628681&abid=12354&text=Hi NisargaUrja"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    UIApplication.shared.open(whatsappURL, options: [:]) { (flag) in
                        
                    }
                } else {
                    ReusableManager.sharedInstance.showAlert(alertString: "Install whatsapp first.")
                }
            }
        }
    }
    
    func getLoyaltyPointsEarned() {
        let urlString = APIConstants.totalLoyaltyPoints+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
            self.loyaltyPoints = nil
            if responseData.count > 0 {
                responseData.forEach { (item) in
                    let keysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    
                    self.loyaltyPoints = "\(fieldsArray[keysArray["totalLoyaltyPoints"].intValue].doubleValue)"
                }
                self.lblLoyaltyPoints.text = self.loyaltyPoints != "" ? (self.loyaltyPoints+" points") : "0 points"
            } else {
                self.lblLoyaltyPoints.text = "0 points"
            }
            self.getCreditsEarned()
            self.tableView.reloadData()
        }
    }
    
    func getCreditsEarned() {
        let urlString = APIConstants.totalCreditPoints+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
            self.creditPoints = nil
            if responseData.count > 0 {
                responseData.forEach { (item) in
                    let keysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    
                    self.creditPoints = fieldsArray[keysArray["totalCredits"].intValue].stringValue
                }
                self.lblCreditsAmount.text = self.creditPoints != "" ? ("₹ "+self.creditPoints) : "₹ 0"
            } else {
               self.lblCreditsAmount.text = "₹ 0"
            }
            self.tableView.reloadData()
        }
    }
    
}



func getAttributedString(key: String, value: String) -> NSAttributedString{
    let attrs1 = [NSAttributedString.Key.font : UIFont.init(name: "Lato-Regular", size: 16.0), NSAttributedString.Key.foregroundColor : UIColor.black]
    
    let attrs2 = [NSAttributedString.Key.font : UIFont.init(name: "Lato-Regular", size: 14.0), NSAttributedString.Key.foregroundColor : UIColor.darkGray]
    
    let attributedString1 = NSMutableAttributedString(string:key, attributes:attrs1 as [NSAttributedString.Key : Any])
    
    let attributedString2 = NSMutableAttributedString(string: value, attributes:attrs2 as [NSAttributedString.Key : Any])
    
    attributedString1.append(attributedString2)
    return attributedString1
}


