//
//  OrderDetailViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 17/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class OrderDetailViewController: UITableViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Order Details"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2 * 4
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "idAddressLabelCell", for: indexPath) as? AddressLabelCell else {
                return UITableViewCell()
            }
            return cell
        } else if indexPath.row == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "idInvoiceHeadersCell", for: indexPath) as? InvoiceHeadersCell else {
                return UITableViewCell()
            }
            return cell
        } else if indexPath.row >= 2 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "idItemsDetailsCell", for: indexPath) as? ItemsDetailsCell else {
                return UITableViewCell()
            }
            return cell
        }
        return UITableViewCell()
    }
}
