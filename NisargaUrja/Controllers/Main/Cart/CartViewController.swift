//
//  CartViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 03/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit
import SwiftyJSON
import PopupDialog
import Razorpay

class CartViewController: UITableViewController, UITextViewDelegate {
    
    
    @IBOutlet weak var lblDliveryDate: UILabel!
    @IBOutlet weak var lblDeliveryDateValue: UILabel!
    @IBOutlet weak var lblOrderSummary: UILabel!
    @IBOutlet weak var lblPurchaseOffer: UILabel!
    @IBOutlet weak var txtNote: UITextView!
    @IBOutlet weak var bottomLineForNotesText: UIView!
    @IBOutlet weak var imgHotPrice: UIImageView!
    @IBOutlet weak var lblApplyCoupon: UILabel!
    @IBOutlet weak var btnSeeAvailableCoupons: UIButton!
    @IBOutlet weak var lblAvsilableLoyaltyPointsValue: UILabel!
    @IBOutlet weak var btnUseLoyaltyPoints: UIButton!
    @IBOutlet weak var lblAvailableCreditsValue: UILabel!
    @IBOutlet weak var btnApplyCredits: UIButton!
    @IBOutlet weak var lblSelectAnOffer: UILabel!
    @IBOutlet weak var billDetailsView: UIView!
    @IBOutlet weak var lblBillDetails: UILabel!
    @IBOutlet weak var lblSubtotalAmount: UILabel!
    @IBOutlet weak var lblDeliveryCharge: UILabel!
    @IBOutlet weak var lblLoyaltyPointsAmount: UILabel!
    @IBOutlet weak var lblCreditPointsAmount: UILabel!
    @IBOutlet weak var lblCouponCodeDiscountAmount: UILabel!
    @IBOutlet weak var lblTotalDiscountAmount: UILabel!
    @IBOutlet weak var lblDeliveryAmount: UILabel!
    @IBOutlet weak var lblUsedLoyaltyPoints: UILabel!
    @IBOutlet weak var lblAppliedCouponCode: UILabel!
    @IBOutlet weak var lblExtraOnTotalDiscount: UILabel!
    @IBOutlet weak var grayLineBottomView: UIView!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblFreeDelivery: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var lblDeliveringTo: UILabel!
    @IBOutlet weak var lblDeliveringToValue: UILabel!
    @IBOutlet weak var btnChangeAddress: UIButton!
    @IBOutlet weak var lblCurrentSelectedAddress: UILabel!
    @IBOutlet weak var btnPayOnDelivery: UIButton!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var appliedCouponButtonWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var appliedCouponView: UIView!
    @IBOutlet weak var lblCouponCode: UILabel!
    
    
    var razorpay: RazorpayCheckout!
    var cartItems: [CartItems] = []
    var cartDetailsData: CartTotals!
    struct CartTotals {
        var subTotal: Double!
        var total: Double!
        var creditsUsed: Double!
        var loyaltyPointsUsed: Double!
        var couponCode: String!
        var totalDiscount: Double!
        var loyaltyPointsEarned: Double!
        var offerUID: String!
        var deliveryCharges: Double!
        var cartItemsNo: String!
    }
    
    
    var cartOffers: [Offers] = []
    struct Offers {
        var offerUID: String!
        var description: String!
        var rule: String!
        var offer: String!
        var isEnabled: Bool!
        var isSelected: Bool!
    }
    
    var couponApplied: [Coupon] = []
    struct Coupon {
        var couponUID: String!
        var couponCode: String!
    }
    
    var loyaltyPoints: String! = "0"
//    struct Points {
//        var loyaltyUID: String!
//        var loyaltyPointsEarned: String!
//        var earnedDate: String!
//        var expiryDate: String!
//    }
    var creditPoints: String! = "0"
//    struct Credits {
//        var creditsUID: String!
//        var creditsEarned: String!
//        var earnedDate: String!
//    }
    
    var isShowingCreditsLoyalty = false
    var needToreloadPage = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        self.setupUI()
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "app_icon")
        imageView.image = image
        navigationItem.titleView = imageView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "CartItemCell", bundle: nil), forCellReuseIdentifier: "CartItemCell")
        tableView.register(UINib(nibName: "CartOffersCell", bundle: nil), forCellReuseIdentifier: "cartOffersCell")
        self.tableView.estimatedRowHeight = 40
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.tableFooterView = UIView()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(cartUpdate),
                                               name: NSNotification.Name(rawValue: "UpdateCartItem"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(cartCouponUpdate),
                                                                                      name: NSNotification.Name(rawValue: "UpdateCartCoupon"), object: nil)
        self.appliedCouponView.applyCommonShadow()
        UpdateUIDetails()
        loadAllContent()
        razorpay = RazorpayCheckout.initWithKey("rzp_test_XUuqPO8Psy96Z5", andDelegate: self)
        NotificationCenter.default.addObserver(self, selector:#selector(self.reloadPage), name: UIApplication.willEnterForegroundNotification, object: UIApplication.shared)
    }

        
    @objc func reloadPage(notification: NSNotification) {
        loadAllContent()
    }
    
    func loadAllContent() {
        if CartRightButton.badge != "0" && selectedTabIndex == 2 {
            getAllCoupons()
            getCartItems(isLoader: true)
            getLoyaltyPointsEarned()
            getCreditsEarned()
            getAppliedCoupon()
            getCartOfferes()
        } else if selectedTabIndex == 2 {
            NoItemsFoundInCartAlert()
        }
        self.tableView.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.lblDeliveringToValue.text = defaultAddressTitle
        self.lblCurrentSelectedAddress.text = defaultAddressText
        getUserDefaultAddress(loader: true)
        if DefaultAddressArray == nil {
            ReusableManager.sharedInstance.showAlert(alertString: "Select delivery address first.")
        }
        if txtNote.text == "" {
            txtNote.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        }
        if needToreloadPage {
            needToreloadPage = false
            getCartTotal(isLoader: true)
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        txtNote.backgroundColor = UIColor.white.withAlphaComponent(1.0)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtNote.text == "" {
            txtNote.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        }
    }
    
    @objc func cartUpdate(notification: NSNotification) {
        getCartItems(isLoader: true)
    }
    
    @objc func cartCouponUpdate(notification: NSNotification) {
        getAppliedCoupon()
    }
    
    func setupUI() {
        btnSeeAvailableCoupons.underlinedText()
        btnChangeAddress.underlinedText()
        billDetailsView.applyCommonShadow()
        btnPayOnDelivery.layer.cornerRadius = btnPayOnDelivery.layer.frame.size.height / 2
        btnPayOnDelivery.layer.borderColor = #colorLiteral(red: 0.5137254902, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
        btnPayOnDelivery.layer.borderWidth = 0.5
        btnPayNow.layer.cornerRadius = btnPayNow.layer.frame.size.height / 2
        btnPayNow.clipsToBounds = true
        btnPayOnDelivery.clipsToBounds = true
    }
    
    func UpdateUIDetails() {
        if cartDetailsData != nil {
            lblPurchaseOffer.text = "Congrats! You will earn \(cartDetailsData.loyaltyPointsEarned!) loyalty points with this order!"

            lblSubtotalAmount.text = "₹ "+String(format:"%.2f", cartDetailsData.subTotal)
            lblLoyaltyPointsAmount.text = "₹ "+String(format:"%.2f", cartDetailsData.loyaltyPointsUsed)
            lblUsedLoyaltyPoints.text = ""
            lblCreditPointsAmount.text = "₹ "+String(format:"%.2f", cartDetailsData.creditsUsed)
            lblAppliedCouponCode.text = cartDetailsData.couponCode != "" ? cartDetailsData.couponCode : "-"
            lblCouponCodeDiscountAmount.text = "₹ 0" //+cartDetailsData.couponDiscount
            lblTotalDiscountAmount.text = "₹ "+String(format:"%.2f", cartDetailsData.totalDiscount)
            lblDeliveryAmount.text = "₹ \(cartDetailsData.deliveryCharges != nil ? String(format:"%.2f", cartDetailsData.deliveryCharges!) : "0")" //.strikeThrough()
            lblTotalAmount.text = "₹ "+String(format:"%.2f", cartDetailsData.total)
            
            lblAvsilableLoyaltyPointsValue.text = self.cartDetailsData.loyaltyPointsUsed == 0.0 ? "You have \(loyaltyPoints!) loyalty points." : "You have applied \(self.cartDetailsData.loyaltyPointsUsed!) loyalty points."
            btnUseLoyaltyPoints.titleLabel?.text = self.cartDetailsData.loyaltyPointsUsed == 0.0 ? "Use Loyalty Points" : "Remove"
          //  setTitle(self.cartDetailsData.loyaltyPointsUsed == 0.0 ? "Use Loyalty Points" : "Remove", for: .normal)
            lblAvailableCreditsValue.text = self.cartDetailsData.creditsUsed == 0.0 ? "Your credits: ₹\(creditPoints!)" : "You have used ₹\(self.cartDetailsData.creditsUsed!) credits."
            btnApplyCredits.setTitle(self.cartDetailsData.creditsUsed == 0.0 ? "Apply Credits" : "Remove", for: .normal)
            
            lblFreeDelivery.isHidden = cartDetailsData.deliveryCharges != nil ? true : false
            btnUseLoyaltyPoints.underlinedText()
            
            isShowingCreditsLoyalty = (self.cartDetailsData.subTotal >= self.cartDetailsData.loyaltyPointsUsed! && self.cartDetailsData.subTotal >= self.cartDetailsData.creditsUsed!)
            self.tableView.reloadData()

        } else {
            lblPurchaseOffer.text = "Congrats! You will earn 0 loyalty points with this order!"
            lblSubtotalAmount.text = "₹ 0"
            lblLoyaltyPointsAmount.text = "₹ 0"
            lblUsedLoyaltyPoints.text = "₹ 0"
            lblCreditPointsAmount.text = "₹ 0"
            lblAppliedCouponCode.text = "-"
            lblCouponCodeDiscountAmount.text = "₹ 0"
            lblTotalDiscountAmount.text = "₹ 0"
            lblDeliveryAmount.text = "₹ 0"
            lblTotalAmount.text = "₹ 0"
            lblFreeDelivery.isHidden = true
            isShowingCreditsLoyalty = false
            self.tableView.reloadData()
        }
    }
    
    @IBAction func btnSeeAvailableCouponsPressed(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "idCouponsViewController") as! CouponsViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnUseLoyaltyPointsPressed(_ sender: UIButton) {
        if sender.titleLabel?.text == "Remove" {
            let urlString = APIConstants.removeLoyaltyPoints+"phoneNo=\(globalUserPhone)"
            ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
                self.getLoyaltyPointsEarned()
                self.getCartTotal(isLoader: false)
            }
        } else {
            if self.loyaltyPoints != nil && self.loyaltyPoints != "" && self.loyaltyPoints != "0" {

                let alertController = UIAlertController(title: "Use Loyalty Points", message: self.lblAvsilableLoyaltyPointsValue.text!, preferredStyle: UIAlertController.Style.alert)


                let saveAction = UIAlertAction(title: "Use", style: UIAlertAction.Style.default, handler: {
                (action : UIAlertAction!) -> Void in
                    if let textField = alertController.textFields?[0] {
                        if textField.text!.count > 0 {
                            let text = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                            let urlString = APIConstants.applyLoyaltyPoints+"loyaltyPoints=\(JSON(text!).intValue)&phoneNo=\(globalUserPhone)"
                            ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
                                self.getLoyaltyPointsEarned()
                                self.getCartTotal(isLoader: false)
                            }
                        }
                    }
                    
                })

                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
                    (action : UIAlertAction!) -> Void in
                    
                    
                })
                
                alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Enter Loyalty Points"
                    textField.isPhoneNumber = true
                    NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main, using:
                        {_ in
                            let text = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                            let textCount = text?.count ?? 0
                            let textIsNotEmpty = textCount > 0
                            let changedValue:Double = JSON(text!).doubleValue
                            let loyaltyPoints:Double = JSON(self.loyaltyPoints!).doubleValue
                            if (self.cartDetailsData.total-changedValue) > (changedValue+self.cartDetailsData.creditsUsed) {
                                
                            }

                            let appliedPoints = self.cartDetailsData.creditsUsed + self.cartDetailsData.loyaltyPointsUsed
                            saveAction.isEnabled = JSON(text!).intValue > 0 && textIsNotEmpty && loyaltyPoints >= changedValue && self.cartDetailsData.total >= changedValue && (self.cartDetailsData.total-appliedPoints) > 0
 
//                            let totalReduce = self.cartDetailsData.total-changedValue
//                            let totalApplied  = changedValue+self.cartDetailsData.creditsUsed
//                            alertController.message = text == "" ? self.lblAvsilableLoyaltyPointsValue.text! : "You have remaining \(totalReduce-totalApplied) loyalty points."
                    })
                    
                }
                saveAction.isEnabled = false
                alertController.addAction(cancelAction)
                alertController.addAction(saveAction)

                present(alertController, animated: true, completion: nil)
            } else {
                ReusableManager.sharedInstance.showAlert(alertString: "You dont have loyalty points to use.")
            }
        }
    }
    
    @IBAction func btnApplyCreditsPressed(_ sender: UIButton) {
        if sender.titleLabel?.text == "Remove" {
            let urlString = APIConstants.removeCredits+"phoneNo=\(globalUserPhone)"
            ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
                self.getCreditsEarned()
                self.getCartTotal(isLoader: false)
            }
        } else {
            if self.creditPoints != nil && self.creditPoints != "" && self.creditPoints != "0" {

                let alertController = UIAlertController(title: "Apply credits", message: self.lblAvailableCreditsValue.text!, preferredStyle: UIAlertController.Style.alert)


                let saveAction = UIAlertAction(title: "Apply", style: UIAlertAction.Style.default, handler: {
                (action : UIAlertAction!) -> Void in
                    if let textField = alertController.textFields?[0] {
                        if textField.text!.count > 0 {
                            let text = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                            let urlString = APIConstants.applyCredits+"credits=\(JSON(text!).intValue)&phoneNo=\(globalUserPhone)"
                            ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
                                self.getCreditsEarned()
                                self.getCartTotal(isLoader: false)
                            }
                        }
                    }
                    
                })

                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
                    (action : UIAlertAction!) -> Void in
                    
                    
                })
                
                
                alertController.addTextField { (textField : UITextField!) -> Void in
                    textField.placeholder = "Enter Credit"
                    textField.isPhoneNumber = true
                    NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main, using:
                        {_ in
                            let text = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                            let textCount = text?.count ?? 0
                            let textIsNotEmpty = textCount > 0
                            let changedValue:Double = JSON(text!).doubleValue
                            let creditPoints:Double = JSON(self.creditPoints!).doubleValue
                            saveAction.isEnabled = JSON(text!).intValue > 0 && textIsNotEmpty && creditPoints >= changedValue && self.cartDetailsData.total >= changedValue
                    })
                    
                }
                saveAction.isEnabled = false
                alertController.addAction(cancelAction)
                alertController.addAction(saveAction)

                present(alertController, animated: true, completion: nil)
            } else {
                ReusableManager.sharedInstance.showAlert(alertString: "You dont have credits to apply.")
            }
        }
    }
    
    @IBAction func CheckAddressButtonAction(_ sender: UIButton) {
        needToreloadPage = true
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "idChangeShippingAddressVC") as! ChangeShippingAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnPayOnDeliveryPressed(_ sender: UIButton) {
        if DefaultAddressArray != nil {
            if DefaultAddressArray.isDeliverable {
                CallPlaceOrder(status: "NOT PAID", paymentID: "")
            } else {
                AddressNotServisable()
            }
        } else {
            ReusableManager.sharedInstance.showAlert(alertString: "Select delivery address first.")
        }
    }
    
    @IBAction func btnPayNowPressed(_ sender: UIButton) {
        if DefaultAddressArray != nil {
            if DefaultAddressArray.isDeliverable {
                self.showPaymentForm()
            } else {
                AddressNotServisable()
            }
        } else {
            ReusableManager.sharedInstance.showAlert(alertString: "Select delivery address first.")
        }
    }
    
    
    func AddressNotServisable() {
        let alertController = UIAlertController(title: "Warning!", message: "You current address is not serviceable. Please change shipping address.", preferredStyle: UIAlertController.Style.alert)
        let saveAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "idChangeShippingAddressVC") as! ChangeShippingAddressVC
            self.navigationController?.pushViewController(vc, animated: true)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        
        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
        
    }
    
    internal func showPaymentForm(){
        let options: [String:Any] = [
            "amount": String(format: "%.0f", cartDetailsData.total!*100), //This is in currency subunits. 100 = 100 paise= INR 1.
                    "currency": "INR",//We support more that 92 international currencies.
                    "description": "NisargaUrja purchased payment",
                    //"order_id": "Order_\(LocalDB.shared.UserInfo.firstName!)\(LocalDB.shared.UserInfo.lastName!)\(LocalDB.shared.UserInfo.phoneNo!)_\(Date())",
                    "image": "https://s3.amazonaws.com/rzp-mobile/images/rzp.png",
                    "name": "\(LocalDB.shared.UserInfo.firstName!.capitalized) \(LocalDB.shared.UserInfo.lastName!.capitalized)",
                    "prefill": [
                        "contact": LocalDB.shared.UserInfo.phoneNo!, //"+919423113759", //
                        "email": LocalDB.shared.UserInfo.email!
                    ],
//                    "theme": [
//                        "color": "#F37254"
//                    ]
                ]
        //
        print(options)
        razorpay.open(options)
    }
    
    @IBAction func RemoveAppliedCouponAction(_ sender: Any) {
        let urlString = APIConstants.removeCoupon+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
            self.appliedCouponView.isHidden = true
            self.lblCouponCode.text = ""
            self.lblApplyCoupon.text = "APPLY COUPON"
            self.getCartTotal(isLoader: false)
        }
    }
    
    func removeAppliedCoupon() {
        if !self.appliedCouponView.isHidden {
            let urlString = APIConstants.removeCoupon+"phoneNo=\(globalUserPhone)"
            ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (responseData, success) in
                self.appliedCouponView.isHidden = true
                self.lblCouponCode.text = ""
                self.lblApplyCoupon.text = "APPLY COUPON"
                self.removeAppliedLoyalty()
            }
        } else{
            self.removeAppliedLoyalty()
        }
    }
    
    func removeAppliedLoyalty() {
        if self.cartDetailsData.loyaltyPointsUsed > 0.0 {
            let urlString = APIConstants.removeLoyaltyPoints+"phoneNo=\(globalUserPhone)"
            ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (responseData, success) in
                self.getLoyaltyPointsEarned()
                self.removeAppliedCredits()
            }
        } else {
            self.removeAppliedCredits()
        }
    }
    
    func removeAppliedCredits() {
        if self.cartDetailsData.creditsUsed > 0.0 {
            let urlString = APIConstants.removeCredits+"phoneNo=\(globalUserPhone)"
            ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
                ReusableManager.sharedInstance.showAlert(alertString: "Your loyalty points / credits / coupon was removed since it was not applicable.")
                self.getCreditsEarned()
                self.getCartTotal(isLoader: false)
            }
        } else {
            self.getCartTotal(isLoader: false)
            ReusableManager.sharedInstance.showAlert(alertString: "Your loyalty points / credits / coupon was removed since it was not applicable.")
        }
    }
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartDetailsData != nil && cartItems.count > 0 ? (section == 0 ? cartItems.count+1 : section == 2 ? self.cartOffers.count+1 : super.tableView(tableView, numberOfRowsInSection: section)) : 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 && indexPath.row > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartItemCell", for: indexPath) as! CartItemCell
            let newIndex = indexPath.row-1
            cell.isAbleToChange = false
            cell.variationUID = cartItems[newIndex].variationUID!
            cell.stprProducctQuantity.value = Double(cartItems[newIndex].quantity!)
            cell.quantityCount = cell.stprProducctQuantity.value
            cell.btnAddToCart.isHidden = cell.stprProducctQuantity.value == 0.0 ? false : true
            cell.stprProducctQuantity.isHidden = !cell.btnAddToCart.isHidden
            
            cell.lblItemName.text = cartItems[newIndex].productName! + " (\(cartItems[newIndex].variationTitle!))"
            cell.lblBrandName.text = cartItems[newIndex].brandName
            cell.lblMRP.attributedText = ("₹ "+cartItems[newIndex].MRP!).strikeThrough()
            cell.lblDiscountedPrice.text = "₹ "+cartItems[newIndex].discountedPrice!
            cell.isAbleToChange = true
            return cell
        } else if indexPath.section == 2 && indexPath.row > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cartOffersCell", for: indexPath) as! CartOffersCell
            let newIndex = indexPath.row-1
            cell.offerUID = cartOffers[newIndex].offerUID!
            cell.lblOfferDescription.text = self.cartOffers[newIndex].description
            cell.btnCheckRadio.tag = newIndex
            if self.cartOffers[newIndex].isEnabled {
                cell.btnCheckRadio.isSelected = self.cartOffers[newIndex].isSelected
                cell.btnCheckRadio.addTarget(self, action: #selector(ApplyOffers), for: .touchUpInside)
            }
            cell.lblOfferDescription.alpha = self.cartOffers[newIndex].isEnabled ? 1.0 : 0.3
            cell.btnCheckRadio.alpha = self.cartOffers[newIndex].isEnabled ? 1.0 : 0.3
            return cell
        }
        return super.tableView(tableView, cellForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        if indexPath.section == 0 && indexPath.row > 0 {
            let newIndexPath = IndexPath(row: 0, section: indexPath.section)
            return super.tableView(tableView, indentationLevelForRowAt: newIndexPath)
        } else if indexPath.section == 2 && indexPath.row > 0 {
               let newIndexPath = IndexPath(row: 0, section: indexPath.section)
               return super.tableView(tableView, indentationLevelForRowAt: newIndexPath)
           }
           return super.tableView(tableView, indentationLevelForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return indexPath.section == 1 ? (indexPath.row == 1 && !isShowingCreditsLoyalty ? 0 : UITableView.automaticDimension) : indexPath.section == 0 && indexPath.row > 0 ? UITableView.automaticDimension : indexPath.section == 2 ? (cartOffers.count > 0 ? UITableView.automaticDimension : 0) : super.tableView(tableView, heightForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 && indexPath.row > 0 {
            let newIndex = indexPath.row-1
            let offerUID = cartOffers[newIndex].offerUID
            let urlString = self.cartOffers[newIndex].isSelected ? APIConstants.removeOffer+"phoneNo=\(globalUserPhone)" : APIConstants.applyOffer+"offerUID=\(offerUID!)&phoneNo=\(globalUserPhone)"
            ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
                self.getCartOfferes()
                self.getCartTotal(isLoader: true)
            }
        }
    }
    @objc func ApplyOffers(_ sender: UIButton) {
//        let offerUID = cartOffers[sender.tag].offerUID
//        let urlString = self.cartOffers[sender.tag].isSelected ? APIConstants.removeOffer+"phoneNo=\(globalUserPhone)" : APIConstants.applyOffer+"offerUID=\(offerUID!)&phoneNo=\(globalUserPhone)"
//        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
//            self.getCartOfferes()
//            self.getCartTotal(isLoader: true)
//        }
    }
}

//MARK:- All API call
extension CartViewController {
    func getUserDefaultAddress(loader: Bool) {
        let urlString = APIConstants.getDefaultAddress+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: loader, showError: false) { (Response, success) in
            if Response.count > 0 {
                let KeysArray = Response[0].fieldLookup
                let fieldsArray = Response[0].fields
                defaultAddress = fieldsArray[KeysArray["address"].intValue].stringValue

                defaultAddressTitle = fieldsArray[KeysArray["title"].intValue].stringValue
                defaultAddressText = fieldsArray[KeysArray["address"].intValue].stringValue

                self.lblDeliveringToValue.text = defaultAddressTitle
                self.lblCurrentSelectedAddress.text = defaultAddressText
                
                DefaultAddressArray = Address(addressUID: fieldsArray[KeysArray["addressUID"].intValue].stringValue, title: fieldsArray[KeysArray["title"].intValue].stringValue, house: fieldsArray[KeysArray["house"].intValue].stringValue, address: fieldsArray[KeysArray["address"].intValue].stringValue, pincode: fieldsArray[KeysArray["pincode"].intValue].stringValue, latitude: fieldsArray[KeysArray["latitude"].intValue].stringValue, longitude: fieldsArray[KeysArray["longitude"].intValue].stringValue, isDeliverable: fieldsArray[KeysArray["isDeliverable"].intValue].boolValue, isBillingAddress: fieldsArray[KeysArray["isBillingAddress"].intValue].boolValue, isDefaultAddress: fieldsArray[KeysArray["isDefaultAddress"].intValue].boolValue)
            }
        }
    }
    
    func getCartItems(isLoader: Bool) {
        let urlString = APIConstants.getcartItem+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: isLoader, showError: false) { (responseData, success) in
            self.cartItems = []
            if responseData.count > 0 {
                responseData.forEach { (item) in
                    let keysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    self.cartItems.append(CartItems(brandName: fieldsArray[keysArray["brandName"].intValue].stringValue, productName: fieldsArray[keysArray["productName"].intValue].stringValue, productPackageImage: fieldsArray[keysArray["productPackageImage"].intValue].stringValue, variationUID: fieldsArray[keysArray["variationUID"].intValue].stringValue, variationTitle: fieldsArray[keysArray["variationTitle"].intValue].stringValue, quantity: fieldsArray[keysArray["quantity"].intValue]["low"].intValue, MRP: fieldsArray[keysArray["MRP"].intValue].stringValue, discountedPrice: fieldsArray[keysArray["discountedPrice"].intValue].stringValue, deliveryDate: fieldsArray[keysArray["deliveryDate"].intValue][0].stringValue))
                }
                self.lblDeliveryDateValue.text = self.cartItems.count > 0 ? getDate(date: self.cartItems[0].deliveryDate!, format: "yyyy-MM-dd", reverseFormat: "EEEE, dd MMM yyyy") : "--"
                self.lblOrderSummary.text = "ORDER SUMMARY"
            } else {
                self.lblOrderSummary.text = "NO ORDER SUMMARY"
            }
            self.getCartTotal(isLoader: true)
            self.tableView.reloadData()
        }
    }
    
    func getLoyaltyPointsEarned() {
        let urlString = APIConstants.totalLoyaltyPoints+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
            self.loyaltyPoints = nil
            if responseData.count > 0 {
                responseData.forEach { (item) in
                    let keysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    
                    self.loyaltyPoints = "\(fieldsArray[keysArray["totalLoyaltyPoints"].intValue].doubleValue)"
                }
            } else {
                self.loyaltyPoints = "0"
            }
            self.UpdateUIDetails()
            self.tableView.reloadData()
        }
    }
    
    func getCreditsEarned() {
        let urlString = APIConstants.totalCreditPoints+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
            self.creditPoints = nil
            if responseData.count > 0 {
                responseData.forEach { (item) in
                    let keysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    
                    self.creditPoints = fieldsArray[keysArray["totalCredits"].intValue].stringValue
                }
            } else {
                           self.creditPoints = "0"
                       }
            self.UpdateUIDetails()
            self.tableView.reloadData()
        }
    }
    
    func getAppliedCoupon() {
        let urlString = APIConstants.getAppliedCoupon+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
        self.couponApplied = []
            if responseData.count > 0 {
                responseData.forEach { (item) in
                    let keysArray = item.fieldLookup
                    let fieldsArray = item.fields
                    self.couponApplied.append(Coupon(couponUID: fieldsArray[keysArray["couponUID"].intValue].stringValue, couponCode: fieldsArray[keysArray["couponCode"].intValue].stringValue))
                }
                if self.couponApplied.count > 0 {
                    self.appliedCouponView.isHidden = false
                    self.lblCouponCode.text = self.couponApplied[0].couponCode
                    self.lblApplyCoupon.text = "APPLIED COUPON"
                }
            }
            self.getCartItems(isLoader: false)
            self.tableView.reloadData()
        }
    }
    
    func getCartOfferes() {
        if DefaultAddressArray != nil {
            let urlString = APIConstants.cartOffers+"phoneNo=\(globalUserPhone)&addressUID=\(DefaultAddressArray.addressUID!)"
            ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
             self.cartOffers = []
                if responseData.count > 0 {
                    responseData.forEach { (item) in
                        let keysArray = item.fieldLookup
                        let fieldsArray = item.fields
                     self.cartOffers.append(Offers(offerUID: fieldsArray[keysArray["offerUID"].intValue].stringValue, description: fieldsArray[keysArray["description"].intValue].stringValue, rule: fieldsArray[keysArray["rule"].intValue].stringValue, offer: fieldsArray[keysArray["offer"].intValue].stringValue, isEnabled: fieldsArray[keysArray["isEnabled"].intValue].boolValue, isSelected: fieldsArray[keysArray["isSelected"].intValue].boolValue))
                    }
                   //self.cartOffers = self.cartOffers.filter{$0.isEnabled}
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func getCartTotal(isLoader: Bool) {
        if DefaultAddressArray != nil {
            let urlString = APIConstants.getcartTotals+"phoneNo=\(globalUserPhone)&addressUID=\(DefaultAddressArray.addressUID!)"
            ApiManager().getRequestApi(url: urlString, runLoader: isLoader, showError: false) { (responseData, success) in
                self.cartDetailsData = nil
                if responseData.count > 0 {
                    responseData.forEach { (item) in
                        let keysArray = item.fieldLookup
                        let fieldsArray = item.fields
                        self.cartDetailsData = CartTotals(subTotal: fieldsArray[keysArray["subTotal"].intValue].doubleValue, total: fieldsArray[keysArray["total"].intValue].doubleValue, creditsUsed: fieldsArray[keysArray["creditsUsed"].intValue].doubleValue, loyaltyPointsUsed: fieldsArray[keysArray["loyaltyPointsUsed"].intValue].doubleValue, couponCode: fieldsArray[keysArray["couponCode"].intValue].stringValue, totalDiscount: fieldsArray[keysArray["totalDiscount"].intValue].doubleValue, loyaltyPointsEarned: fieldsArray[keysArray["loyaltyPointsEarned"].intValue].doubleValue, offerUID: fieldsArray[keysArray["offerUID"].intValue].stringValue, deliveryCharges: fieldsArray[keysArray["deliveryCharges"].intValue].doubleValue, cartItemsNo: fieldsArray[keysArray["cartItemsNo"]["low"].intValue].stringValue)
                        
                    }
                    let numberOfItems = responseData[0].fields[9]["low"].intValue
                    CartRightButton.badge = "\(numberOfItems)"
                    hhTabBarView.tabBarTabs[2].badge = CartRightButton.badge
                    self.lblDeliveryDateValue.text = self.cartItems.count > 0 ? getDate(date: self.cartItems[0].deliveryDate!, format: "yyyy-MM-dd", reverseFormat: "EEEE, dd MMM yyyy") : "--"
                    self.lblOrderSummary.text = "ORDER SUMMARY"
                    
                   // let appliedPoints = self.cartDetailsData.creditsUsed + self.cartDetailsData.loyaltyPointsUsed
                    if selectedTabIndex == 2 && self.cartDetailsData.total < 0 {
                        self.removeAppliedCoupon()
                    }
                } else {
                    CartRightButton.badge = "0"
                    hhTabBarView.tabBarTabs[2].badge = CartRightButton.badge
                    self.lblOrderSummary.text = "NO ORDER SUMMARY"
                    //(self.tableView as! DesignableTableView).backgroundImage = UIImage(named: "noCartItems")
                    self.NoItemsFoundInCartAlert()
                }
                hhTabBarView.tabBarTabs[2].badgeLabel.isHidden = CartRightButton.badge == "0" ? true : false
                self.UpdateUIDetails()
                self.tableView.reloadData()
            }
        }
    }
    
    func CallPlaceOrder(status: String, paymentID: String) {
        let urlString = APIConstants.placeOrder+"phoneNo=\(globalUserPhone)&addressUID=\(DefaultAddressArray.addressUID!)&note=\(txtNote.text!)&status=\(status)&paymentID=\(paymentID)" //+ paymentID != "" ? "&paymentID=\(paymentID)" : ""
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
                hhTabBarView.onTabTapped(0)
                ReusableManager.sharedInstance.showSuccessAlert(alertString: "Your order placed successfully.")
        }
    }
    func getAllCoupons() {
        let urlString = APIConstants.getCoupons+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
            if responseData.count > 0 {
                self.lblApplyCoupon.text = "APPLY COUPON"
                self.btnSeeAvailableCoupons.isHidden = false
                self.appliedCouponButtonWidthConstraint.constant = 155
            } else {
                self.appliedCouponButtonWidthConstraint.constant = 0
                self.btnSeeAvailableCoupons.isHidden = true
                self.lblApplyCoupon.text = "No coupons available at the moment."
            }
        }
    }
    
    func NoItemsFoundInCartAlert() {
        let alertController = UIAlertController(title: appName, message: "No items in cart.", preferredStyle: UIAlertController.Style.alert)
        let Continue = UIAlertAction(title: "Continue Shopping", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
            hhTabBarView.onTabTapped(0)
        })
        alertController.addAction(Continue)
        
        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
    }
}


extension CartViewController: RazorpayPaymentCompletionProtocol {
    public func onPaymentError(_ code: Int32, description str: String){
        ReusableManager.sharedInstance.showAlert(alertString: "Cancelled payment.")
    }

    public func onPaymentSuccess(_ payment_id: String){
        self.CallPlaceOrder(status: "PAID", paymentID: payment_id)
//        let alertController = UIAlertController(title: "Success", message: "You transaction has beed completed. Payment Id \(payment_id)", preferredStyle: UIAlertController.Style.alert)
//        let ContinueAction = UIAlertAction(title: "Continue to procced order", style: UIAlertAction.Style.default, handler: {
//        (action : UIAlertAction!) -> Void in
//        })
//        alertController.addAction(ContinueAction)
//        present(alertController, animated: true, completion: nil)
    }
}
