//
//  CouponsViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 26/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class CouponsViewController: UITableViewController {

    var coupons: [Coupons] = []
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "app_icon")
        imageView.image = image
        navigationItem.titleView = imageView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.getAllCoupons()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return coupons.count
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "titleLabel") as? CouponsCell else {
            return UITableViewCell()
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? CouponsCell else {
            return UITableViewCell()
        }
        cell.btnApplyNow.tag = indexPath.row
        cell.btnApplyNow.addTarget(self, action: #selector(ApplyCoupon), for: .touchUpInside)
        let coupon = coupons[indexPath.row]
        cell.updateUI(coupon: coupon)
        
        return cell
    }
    
    @objc func ApplyCoupon(_ sender: UIButton) {
        let couponCode = coupons[sender.tag].couponUID
        let urlString = APIConstants.applyCoupon+"couponUID=\(couponCode)&phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
           self.navigationController?.popViewController(animated: true)
           NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateCartCoupon"), object: nil)
        }
    }
    
    func getAllCoupons() {
        let urlString = APIConstants.getCoupons+"phoneNo=\(globalUserPhone)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (responseData, success) in
            if responseData.count > 0 {
                for response in responseData{
                    let KeysArray = response.fieldLookup
                    let fieldsArray = response.fields
                    self.coupons.append(Coupons(couponUID: fieldsArray[KeysArray["couponUID"].intValue].stringValue, couponName: fieldsArray[KeysArray["couponName"].intValue].stringValue, couponDescription: fieldsArray[KeysArray["couponDescription"].intValue].stringValue, couponCode: fieldsArray[KeysArray["couponCode"].intValue].stringValue))
                }
                    
                if self.coupons.isEmpty {
                    print("Coupons Array is empty")
                }
            }
            self.tableView.reloadData()
        }
    }
}
