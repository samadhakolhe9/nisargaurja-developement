
import UIKit

protocol CustomAlertDelegate {
    func didPressButton(button:UIButton)
}
protocol CustomTextAlertDelegate {
    func didPressButton(text: String)
}
class CustomAlertTextFieldController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var txtValueField: SKCustomTextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var delegate:CustomAlertDelegate!
    var delegateText:CustomTextAlertDelegate!
    var titleString: String?
    var textFieldText: String? = ""
    var textFieldPlaceholder: String? = ""
    var buttonText: String? = ""
    var buttonClickToDismiss:Bool = true
    var isErolPopup = false
    
    
    static func instantiate() -> CustomAlertTextFieldController? {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "\(CustomAlertTextFieldController.self)") as? CustomAlertTextFieldController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleString
        txtValueField.text = textFieldText
        txtValueField.placeholder = textFieldPlaceholder
        btnSubmit.setTitle(buttonText, for: .normal)
        
        
        btnSubmit.layer.cornerRadius = 5
        btnSubmit.backgroundColor = .white
        btnSubmit.layer.backgroundColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        btnSubmit.layer.borderWidth = 1
        btnSubmit.layer.borderColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1).cgColor
        
        
        if #available(iOS 13.0, *) {
            self.isModalInPresentation = true
        }
    }
    
    // MARK: Actions
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func SubmitButtonAction(_ sender: UIButton) {
        if buttonClickToDismiss {
            delegateText.didPressButton(text: txtValueField.text!)
            dismiss(animated: true, completion: nil)
        }
    }
    
}
