
//
//  TabViewController.swift

//
//  Created by SAMADHAN KOLHE   on 03/08/20.
//  Copyright © 2019 SAMADHAN KOLHE. All rights reserved.
//

import UIKit

let hhTabBarView = HHTabBarView.shared
var referenceUITabBarController = HHTabBarView.shared.referenceUITabBarController
var selectedTabIndex = 0

//2

func setupReferenceUITabBarController() -> UITabBarController {
    
    //Creating a storyboard reference
    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    
    let HomeController = storyboard.instantiateViewController(withIdentifier: "idHomeViewController") as! HomeViewController
    //HomeController.title = "Home"
    HomeController.view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    let HomeNavController: UINavigationController = UINavigationController(rootViewController: HomeController)
    
    let SearchController = storyboard.instantiateViewController(withIdentifier: "idSearchViewController") as! SearchViewController
    SearchController.title = "Search"
    SearchController.view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    let SearchNavController: UINavigationController = UINavigationController(rootViewController: SearchController)
    
    let CartController = storyboard.instantiateViewController(withIdentifier: "idCartViewController") as! CartViewController
    CartController.title = "Cart"
    CartController.view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    let CartNavController: UINavigationController = UINavigationController(rootViewController: CartController)
    
    //Update referenced TabbarController with your viewcontrollers
    referenceUITabBarController.setViewControllers([HomeNavController, SearchNavController, CartNavController], animated: false)
    
    setupHHTabBarView(selectedIndex: 0)
    return referenceUITabBarController
}

//3
func setupHHTabBarView(selectedIndex: Int) {
    let tabFont = UIFont.init(name: "Lato-Regular", size: 16.0)
    let spacing: CGFloat = 5.0
    
    let titles = ["Home", "Search", "Cart"]
    let images = [UIImage(named: "home_icon")!.imageWithColor(color: .black), UIImage(named: "search_icon")!.imageWithColor(color: .black), UIImage(named: "cart_icon")!.imageWithColor(color: .black)]
    
    let SelectedImages = [UIImage(named: "home_icon")!.imageWithColor(color: AppCommonColor), UIImage(named: "search_icon")!.imageWithColor(color: AppCommonColor), UIImage(named: "cart_icon")!.imageWithColor(color: AppCommonColor)]
    var tabs = [HHTabButton]()
    
    for index in 0..<titles.count {
        let tab = HHTabButton(withTitle: titles[index], selectedTabImage: SelectedImages[index], tabImage: images[index], index: index)
        tab.setTitleColor(.black, for: .normal)
        tab.setTitleColor(AppCommonColor, for: .selected)
        tab.titleLabel?.font = tabFont
        tab.titleLabel?.sizeToFit()
        tab.setHHTabBackgroundColor(color: #colorLiteral(red: 0.9528577924, green: 0.9529945254, blue: 0.9528279901, alpha: 1), forState: .normal)
        tab.setHHTabBackgroundColor(color: #colorLiteral(red: 0.9528577924, green: 0.9529945254, blue: 0.9528279901, alpha: 1), forState: .selected) //#colorLiteral(red: 0.1931178009, green: 0.9208925442, blue: 0.06933869829, alpha: 0.09892165493)
        tab.imageToTitleSpacing = spacing
        tab.imageVerticalAlignment = .top
        tab.imageHorizontalAlignment = .center
        tab.badgeValue = 1
        tabs.append(tab)
    }
    
    hhTabBarView.backgroundColor = #colorLiteral(red: 0.9528577924, green: 0.9529945254, blue: 0.9528279901, alpha: 1)
    //Set HHTabBarView position.
    hhTabBarView.tabBarViewPosition = .bottom
   
    //Set this value according to your UI requirements.
    hhTabBarView.tabBarViewTopPositionValue = 40
    
    //Set Default Index for HHTabBarView.
    hhTabBarView.tabBarTabs = tabs
    
    // To modify badge label.
    // Note: You should only modify badgeLabel after assigning tabs array.
    // Example:
//    tabs[2].badgeLabel?.backgroundColor = .red
//    tabs[2].badgeLabel?.textColor = .white
//    tabs[2].badgeLabel?.font = UIFont.boldSystemFont(ofSize: 12.0)

    hhTabBarView.tabBarTabs[2].badgeEdgeInsets = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 40)
    hhTabBarView.tabBarTabs[2].badgeBackgroundColor = .orange
    hhTabBarView.tabBarTabs[2].badgeLabel.isHidden = true
    
    //Handle Tab Change Event
    hhTabBarView.defaultIndex = selectedIndex
    
    //Show Animation on Switching Tabs
    hhTabBarView.tabChangeAnimationType = .none
    
    //Handle Tab Changes
    hhTabBarView.onTabTapped = { (tabIndex) in
        print("Selected Tab Index:\(tabIndex)")
        selectedTabIndex = tabIndex
        referenceUITabBarController = setupReferenceUITabBarController()
        setupHHTabBarView(selectedIndex: tabIndex)
        if CartRightButton.badge != "0" {
            hhTabBarView.tabBarTabs[2].badge = CartRightButton.badge
        }
    }
}
