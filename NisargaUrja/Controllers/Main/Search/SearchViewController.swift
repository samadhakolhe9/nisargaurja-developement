//
//  SearchViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 03/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var txtSearchBar: UITextField!
    @IBOutlet weak var tblSearchList: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var indicatorViewWidthConstraint: NSLayoutConstraint!
    
    var SearchResultData:[Results] = []
    struct Results {
        var itemUID: String!
        var itemName: String!
        var pageIndex: Int!
    }

//    var productUID: String!
//    var productName: String!
//    var brandUID: String!
//    var brandName: String!
//    var categoryUID: String!
//    var categoryName: String!
    var isSearchActive = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "app_icon")
        imageView.image = image
        navigationItem.titleView = imageView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.isHidden = true
        indicatorViewWidthConstraint.constant = 0
        searchView.applyCommonShadow()
        tblSearchList.tableFooterView = UIView()
        tblSearchList.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return SearchResultData.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SearchListCell

        cell.lblSearchedText.text = SearchResultData[indexPath.row].itemName
        let pageIndex = SearchResultData[indexPath.row].pageIndex
        cell.lblSearchType.text = pageIndex == 0 ? "in Products" : pageIndex == 1 ? "in Brands" : "in Categories"

        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SearchListCell
        cell.lblSearchedText.text = !isSearchActive ? "" : SearchResultData.count > 0 ? "Search Result:" : "No result found."
        cell.lblSearchType.text = ""
        cell.searchIconWidthConstraint.constant = 0
        cell.searchIcon.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pageIndex = SearchResultData[indexPath.row].pageIndex
        if pageIndex == 0 { // for Product
            let productViewController = self.storyboard?.instantiateViewController(withIdentifier: "idProductDetailsViewController") as! ProductDetailsViewController
            productViewController.prodUID = SearchResultData[indexPath.row].itemUID
            self.navigationController?.pushViewController(productViewController, animated: true)
        } else if pageIndex == 1 { // for Brand
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "idCategoryDetailsViewController") as! CategoryDetailsViewController
            controller.title = SearchResultData[indexPath.row].itemName
            controller.brandName = SearchResultData[indexPath.row].itemName
            self.navigationController?.pushViewController(controller, animated: true)
        } else if pageIndex == 2 { // for Category
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "idCategoryDetailsViewController") as! CategoryDetailsViewController
            controller.title = SearchResultData[indexPath.row].itemName
            controller.categoryName = SearchResultData[indexPath.row].itemName
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    func getSearchResult(text: String) {
        isSearchActive = true
        activityIndicator.isHidden = false
        indicatorViewWidthConstraint.constant = 15
        let urlString = APIConstants.getSearchResult+"searchQuery=\(text)"
        ApiManager().getRequestApi(url: urlString, runLoader: false, showError: false) { (responseData, success) in
            self.SearchResultData = []
            if responseData.count > 0 {
                responseData.forEach { (item) in
                    let fieldsArray = item.fields
                    if fieldsArray[0].stringValue != "" {
                        self.SearchResultData.append(Results(itemUID: fieldsArray[0].stringValue, itemName: fieldsArray[1].stringValue, pageIndex: 0))
                    }
                }
                responseData.forEach { (item) in
                    let fieldsArray = item.fields
                    if fieldsArray[2].stringValue != "" {
                        self.SearchResultData.append(Results(itemUID: fieldsArray[2].stringValue, itemName: fieldsArray[3].stringValue, pageIndex: 1))
                    }
                }
                responseData.forEach { (item) in
                    let fieldsArray = item.fields
                    if fieldsArray[4].stringValue != "" {
                        self.SearchResultData.append(Results(itemUID: fieldsArray[4].stringValue, itemName: fieldsArray[5].stringValue, pageIndex: 2))
                    }
                }
                self.SearchResultData = self.SearchResultData.filterDuplicates{$0.itemUID == $1.itemUID}
            }
            self.SearchResultData = self.txtSearchBar.text == "" ? [] : self.SearchResultData
            self.tblSearchList.reloadData()
            self.activityIndicator.isHidden = true
            self.indicatorViewWidthConstraint.constant = 0
        }
    }
}
extension SearchViewController : UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    public func  textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }

    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string != "" {
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            self.getSearchResult(text: newString as String)
        } else {
            self.SearchResultData = []
            self.isSearchActive = false
            self.activityIndicator.isHidden = true
            self.indicatorViewWidthConstraint.constant = 0
            self.tblSearchList.reloadData()
        }
        return true;
    }

}
