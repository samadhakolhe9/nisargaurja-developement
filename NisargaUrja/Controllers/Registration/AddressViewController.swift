//
//  AddressViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 02/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import Alamofire

class AddressViewController: UIViewController, UITextFieldDelegate, GMSMapViewDelegate {
    
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnConfirmLocation: UIButton!
    @IBOutlet weak var lblDelivered: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblSelectedAddress: UILabel!
    @IBOutlet weak var btnServiceable: UIButton!
    @IBOutlet weak var serviceableView: UIView!
    @IBOutlet weak var lblServiceableAddress: UILabel!
    @IBOutlet weak var serviceableSubView: UIView!
    @IBOutlet weak var popupHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var popupWidthConstraint: NSLayoutConstraint!
    
    
    //TODO:: Outlets for Add New Address View Elements
    @IBOutlet var containerAddNewAddress: UIView!
    @IBOutlet weak var closeBarView: UIView!
    @IBOutlet weak var btnCloseAddNewAddressView: UIButton!
    @IBOutlet weak var lblAddNewAddress: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var innerAddNewAddressView: UIView!
    @IBOutlet weak var lblYourLocation: UILabel!
    @IBOutlet weak var lblYourAddress: UILabel!
    @IBOutlet weak var btnChangeYourAddress: UIButton!
    @IBOutlet weak var txtFlatNumber: UITextField!
    @IBOutlet weak var lblCharCountFlatNumber: UILabel!
    @IBOutlet weak var txtStreetName: UITextField!
    @IBOutlet weak var lblCharCountStreetName: UILabel!
    @IBOutlet weak var btnAddressTypeHome: UIButton!
    @IBOutlet weak var btnAddressTypeWork: UIButton!
    @IBOutlet weak var btnAddressTypeOther: UIButton!
    @IBOutlet weak var btnSaveAndContinueAddNewAddress: UIButton!
    @IBOutlet weak var imgMapMarker: UIImageView!
    @IBOutlet weak var houseDividerView: UIView!
    @IBOutlet weak var streetDividerView: UIView!
    @IBOutlet weak var newAddressViewHeightConstraint: NSLayoutConstraint!
    
    
    var locValue:CLLocationCoordinate2D!
    var selectedPlace: GMSPlace!
    var selectedAddress = ""
    var marker = GMSMarker()
    var selectedAddressType = "Home"
    var selectedPinCode = ""
    var isKeyboardAppear = false
    var isFromRegistration = false
    var isChangeAddress = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = ""
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.setNavbar(backgroundColorAlpha: 0.0, newColor: .white)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSaveAndContinueAddNewAddress.layer.cornerRadius = 5
        btnSaveAndContinueAddNewAddress.backgroundColor = .white
        btnSaveAndContinueAddNewAddress.layer.backgroundColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        btnSaveAndContinueAddNewAddress.layer.borderWidth = 1
        btnSaveAndContinueAddNewAddress.layer.borderColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1).cgColor
        
        bottomView.bringSubviewToFront(mapView)
        imgMapMarker.bringSubviewToFront(mapView)
        bottomView.applyCommonShadow()
        btnChangeYourAddress.applyCommonShadow()
        btnConfirmLocation.layer.cornerRadius = 5
        btnConfirmLocation.layer.backgroundColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        btnConfirmLocation.layer.borderWidth = 1
        btnConfirmLocation.layer.borderColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1).cgColor

        lblAddress.attributedText = NSMutableAttributedString(string: selectedAddress, attributes: [NSAttributedString.Key.kern: 1.6])
        lblSelectedAddress.text = selectedAddress
        lblYourAddress.text = lblSelectedAddress.text
        
        
        btnServiceable.layer.cornerRadius = 5
        btnServiceable.layer.borderWidth = 1
        btnServiceable.layer.borderColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1).cgColor
        btnServiceable.layer.backgroundColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        
        serviceableSubView.layer.cornerRadius = 5
        print(self.locValue.latitude, self.locValue.longitude)
        if locValue != nil {
            self.mapView.delegate = self
            self.mapView.camera = GMSCameraPosition.camera(withLatitude: self.locValue.latitude, longitude: self.locValue.longitude, zoom: 18.0)
            self.marker = GMSMarker(position: CLLocationCoordinate2D(latitude: self.locValue.latitude, longitude: self.locValue.longitude))
            if selectedPlace == nil {
                updateAddress()
            } else {
                //marker.title = "\(self.selectedPlace.name!)"
                //marker.snippet = "\(self.selectedPlace.formattedAddress!)"
            }
            //marker.map = self.mapView
            
        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    func updateAddress() {
        deviceLatitude = "\(locValue.latitude)"
        deviceLongitude = "\(locValue.longitude)"

        userRegisterParams["lat"] = locValue.latitude
        userRegisterParams["lng"] = locValue.longitude
//        getAddress(latitude: locValue.latitude, longitude: locValue.longitude) { (address) in
//        }
        
        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(locValue.latitude),\(locValue.longitude)&key=AIzaSyAunyxVfp1E-gh14BnPGEjUILNfy_9X_Qo"
        print("Google geocode API:  ", url)
        AF.request(url).validate().responseJSON { response in
                    switch response.result {
                    case .success:
                    if response.value != nil {
                        let results = JSON(response.value!)["results"]
                        
                        var formattedAddress = results[0]["formatted_address"].stringValue
                        if formattedAddress.contains("Unnamed Road") {
                            formattedAddress = formattedAddress.replacingOccurrences(of: "Unnamed Road", with: "")
                            formattedAddress = String(formattedAddress.dropFirst(2))
                        }
                        self.setUpdatedAddress(address: formattedAddress)
                        
                        for component in results[0]["address_components"].arrayValue {
                            print(component)
                            let temp = component["types"].arrayValue
                            if (temp[0].stringValue == "sublocality_level_2") {
                              //  address = address + component.value["long_name"].stringValue + ", "
                                self.marker.title = component["long_name"].stringValue
                            }
                            if (temp[0].stringValue == "sublocality_level_1") {
                              //  address = address + component.value["long_name"].stringValue + ", "
                            }
                            if (temp[0].stringValue == "locality") {
                              //  address = address + component.value["long_name"].stringValue + ", "
                            }
                            if (temp[0].stringValue == "administrative_area_level_1") {
                              //  address = address + component.value["long_name"].stringValue + ", "
                            }
                            if (temp[0].stringValue == "country") {
                              //  address = address + component.value["long_name"].stringValue
                            }
                            if (temp[0].stringValue == "postal_code") {
                               // address = address + component.value["long_name"].stringValue + ", "
                                self.selectedPinCode = component["long_name"].stringValue

                                print("pin code :", self.selectedPinCode)
                            }
                        }
                    }
                    case .failure(let error):
                        print(error)
                    }
                }
    }
    
    func setUpdatedAddress(address: String) {
        self.lblAddress.attributedText = NSMutableAttributedString(string: address, attributes: [NSAttributedString.Key.kern: 1.6])
        self.lblSelectedAddress.text = self.selectedAddress == "" ? address : self.selectedAddress
        self.lblYourAddress.text = self.lblSelectedAddress.text
        userRegisterParams["address"] = self.lblSelectedAddress.text!
        self.selectedAddress = ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.setNavbar(backgroundColorAlpha: 1.0, newColor: .white)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification , object: nil)
    }

    func updateLocationoordinates(coordinates:CLLocationCoordinate2D) {
        CATransaction.begin()
        CATransaction.setAnimationDuration(1.0)
        CATransaction.commit()
    }

    func mapView(mapView: GMSMapView, didChangeCameraPosition position: GMSCameraPosition) {

        
    }

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("changed position: \(mapView.camera.target)")
        var destinationLocation = CLLocation()
        destinationLocation = CLLocation(latitude: position.target.latitude,  longitude: position.target.longitude)
        locValue = destinationLocation.coordinate
        self.updateAddress()
    }
    
    // Camera change Position this methods will call every time
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {

       self.marker.position =  position.target
    }
    
    //TODO:: Set AddresType Button Background Color
    func setAddressTypeButtonBackgroundColor(senderButton: UIButton) {
        senderButton.applyCommonShadow()
        if senderButton == btnAddressTypeHome {
            RemoveButtonColor(sender: btnAddressTypeWork)
            RemoveButtonColor(sender: btnAddressTypeOther)
        } else if senderButton == btnAddressTypeWork {
            RemoveButtonColor(sender: btnAddressTypeHome)
            RemoveButtonColor(sender: btnAddressTypeOther)
        } else if senderButton == btnAddressTypeOther {
            RemoveButtonColor(sender: btnAddressTypeWork)
            RemoveButtonColor(sender: btnAddressTypeHome)
        }
    }
    
    func RemoveButtonColor(sender: UIButton) {
        sender.backgroundColor = UIColor.clear
        sender.layer.cornerRadius = 5
        sender.clipsToBounds = true
    }


    var keyboardHeight: CGFloat = 0.0
    @objc func keyboardWillAppear(notification: NSNotification?) {

        guard let keyboardFrame = notification?.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        if #available(iOS 11.0, *) {
            keyboardHeight = keyboardFrame.cgRectValue.height - self.view.safeAreaInsets.bottom
        } else {
            keyboardHeight = keyboardFrame.cgRectValue.height
        }
        if !isKeyboardAppear {
            UIView.animate(withDuration: 0.5) {
                self.containerAddNewAddress.frame = CGRect(x: 0, y: self.view.frame.maxY-(self.keyboardHeight+430), width: self.view.bounds.width, height: 410)
            }
        }
        isKeyboardAppear = true
    }

    @objc func keyboardWillDisappear(notification: NSNotification?) {
        if isKeyboardAppear {
            UIView.animate(withDuration: 0.5) {
                self.containerAddNewAddress.frame = CGRect(x: 0, y: self.view.frame.maxY-410, width: self.view.bounds.width, height: 410)
            }
        }
        keyboardHeight = 0.0
        isKeyboardAppear = false
    }
    
    
    //MARK:- UITextfield Delegate
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        let changeIndex = newString.length
        if textField == txtFlatNumber {
            lblCharCountFlatNumber.text = "\(changeIndex > 10 ? 10: changeIndex)/10"
            return changeIndex <= 10
        } else if textField == txtStreetName {
            lblCharCountStreetName.text = "\(changeIndex > 40 ? 40: changeIndex)/40"
            return changeIndex <= 40
        }
        return range.location < 100
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtFlatNumber {
            houseDividerView.backgroundColor = AppCommonColor
        } else if textField == txtStreetName {
            houseDividerView.backgroundColor = AppCommonColor
        }
        return true
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == txtFlatNumber {
            houseDividerView.backgroundColor = AppCommonColor
        } else if textField == txtStreetName {
            houseDividerView.backgroundColor = .lightGray
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFlatNumber {
            houseDividerView.backgroundColor = .lightGray
        } else if textField == txtStreetName {
            houseDividerView.backgroundColor = .lightGray
        }
        self.view.endEditing(true)
        return true
    }
    
    
    @IBAction func btnCloseAddNewAddressViewPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.containerAddNewAddress.frame = CGRect(x: 0, y: self.view.frame.maxY, width: self.view.bounds.width, height: 410)
            self.containerAddNewAddress.removeFromSuperview()
        }
    }
    
    @IBAction func btnSkipAddNewAddressPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.containerAddNewAddress.frame = CGRect(x: 0, y: self.view.frame.maxY, width: self.view.bounds.width, height: 410)
            self.containerAddNewAddress.removeFromSuperview()
            self.serviceableView.isHidden = false
            self.popupHeightConstraint.constant = 0
            self.popupWidthConstraint.constant = 0
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.5) {
                self.popupHeightConstraint.constant = 320
                self.popupWidthConstraint.constant = 350
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func btnAddressTypePressed(_ sender: UIButton) {
        setAddressTypeButtonBackgroundColor(senderButton: sender)
        if sender.tag == 1 {
            //Home Button Selected
            selectedAddressType = "Home"
        } else if sender.tag == 2 {
            //Work Button is Selected
            selectedAddressType = "Work"
        } else {
            //Other Button is Selected
            selectedAddressType = "Other"
        }
        userRegisterParams["title"] = selectedAddressType
        lblServiceableAddress.text = selectedAddressType
        
        UIView.animate(withDuration: 0.5) {
            let viewHeight:CGFloat = sender.tag != 3 ? 350 : 410
            self.containerAddNewAddress.frame = CGRect(x: 0, y: self.view.frame.maxY-(self.keyboardHeight+viewHeight), width: self.view.bounds.width, height: viewHeight)
            self.newAddressViewHeightConstraint.constant = sender.tag != 3 ? 70 : 130
        }
    }
    
    @IBAction func btnAddNewAddressSaveAndContinuePressed(_ sender: UIButton) {
        self.view.endEditing(true)
        if txtFlatNumber.text == "" {
            txtFlatNumber.becomeFirstResponder()
            houseDividerView.backgroundColor = .red
        }  else if self.selectedAddressType == "Other" && txtStreetName.text == "" {
            txtStreetName.becomeFirstResponder()
            streetDividerView.backgroundColor = .red
        } else {
            userRegisterParams["house"] = txtFlatNumber.text!
            userRegisterParams["title"] = self.selectedAddressType == "Other" ? txtStreetName.text! : selectedAddressType
            var isServisable = false
            ApiManager().getRequestApi(url: APIConstants.getDeliveryPincodes, runLoader: true, showError: false) { (Response, success) in
                if Response.count > 0 {
                    for jsonValue in Response {
                        let Pincode = jsonValue.fields[0]["low"].stringValue
                        if Pincode == self.selectedPinCode {
                            isServisable = true
                        }
                    }
                }
                userRegisterParams["pincode"] = self.selectedPinCode
                userRegisterParams["isDeliverable"] = isServisable
                self.RegisterUserNewAddress(isServisable: isServisable)
            }
        }

    }
    
    func RegisterUserNewAddress(isServisable: Bool) {
        var paramString = ""
        for data in JSON(userRegisterParams) {
            paramString = paramString + "\(data.0)" + "=\(data.1)&"
        }
        paramString.removeLast(1)
        print("user registration param: ", paramString)
        
        if "\(userRegisterParams["addressUID"]!)" != "" {
            let urlString = APIConstants.editAddress+paramString
            ApiManager().getRequestApi(url: urlString, runLoader: true, showError: true) { (Response, success) in
                if success {
                    self.OpenServisablePopup(isServisable: isServisable)
                }
            }
        } else if !isFromRegistration {
            let urlString = APIConstants.addAddress+paramString
            ApiManager().getRequestApi(url: urlString, runLoader: true, showError: true) { (Response, success) in
                if success {
                    self.OpenServisablePopup(isServisable: isServisable)
                }
            }
        } else {
            let urlString = APIConstants.registerCustomer+paramString
            ApiManager().getRequestApi(url: urlString, runLoader: true, showError: true) { (Response, success) in
                if success {
                    LocalDB.shared.setUserMobileNumber(phone: "\(userRegisterParams["phoneNo"]!)")
                    self.OpenServisablePopup(isServisable: isServisable)
                }
            }
        }
    }
    var isOnNextPage = false
    func OpenServisablePopup(isServisable: Bool) {
        btnServiceable.setTitle(isServisable ? "YOUR AREA IS SERVICEABLE!" : "YOUR AREA IS NOT SERVICEABLE!", for: .normal)
        btnServiceable.layer.backgroundColor = isServisable ? UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor : UIColor.red.cgColor
        UIView.animate(withDuration: 0.5) {
            self.containerAddNewAddress.frame = CGRect(x: 0, y: self.view.frame.maxY, width: self.view.bounds.width, height: 410)
            self.containerAddNewAddress.removeFromSuperview()
            self.serviceableView.isHidden = false
            self.popupHeightConstraint.constant = 0
            self.popupWidthConstraint.constant = 0
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.5) {
                self.popupHeightConstraint.constant = 320
                self.popupWidthConstraint.constant = 350
                self.view.layoutIfNeeded()
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            if !self.isOnNextPage {
                self.gotoHomePage()
            }
        }
    }
    
    @IBAction func ChangeButtonAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "idSetDeliveryLocationController") as! SetDeliveryLocationController
        controller.isFromRegistration = isFromRegistration
        controller.isChangeAddress = isChangeAddress
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func ConfirmLocationAction(_ sender: UIButton) {
        containerAddNewAddress.frame = CGRect(x: 0, y: self.view.frame.maxY, width: self.view.bounds.width, height: 410)
        btnAddressTypeHome.applyCommonShadow()
        self.view.addSubview(containerAddNewAddress)
        innerAddNewAddressView.applyCommonShadow()
        containerAddNewAddress.bringSubviewToFront(mapView)
        UIView.animate(withDuration: 0.5) {
            let viewHeight:CGFloat = 350
            self.containerAddNewAddress.frame = CGRect(x: 0, y: self.view.frame.maxY-viewHeight, width: self.view.bounds.width, height: viewHeight)
            self.newAddressViewHeightConstraint.constant = 70
        }
    }
    
    @IBAction func ServisableButtonAction(_ sender: UIButton) {
        isOnNextPage = true
        self.gotoHomePage()
    }
    
    func gotoHomePage() {
        let leftViewController = self.storyboard?.instantiateViewController(withIdentifier: "idLeftViewController") as! LeftViewController
        let slideMenuController = ExSlideMenuController(mainViewController:setupReferenceUITabBarController(), leftMenuViewController: leftViewController)
        self.navigationController?.pushViewController(slideMenuController, animated: true)
    }
    
    func getAddress(latitude: Double, longitude: Double, handler: @escaping (String) -> Void)
    {
        var address: String = ""
        var formattedAddress: String = ""
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        //selectedLat and selectedLon are double values set by the app in a previous process
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark?
            placeMark = placemarks?[0]
            
            // Address dictionary
            //print(placeMark.addressDictionary ?? "")
            
            // Location name
            if let locationName = placeMark?.addressDictionary?["Name"] as? String {
                self.marker.title = locationName
                address += locationName + ", "
            }
            
            // Street address
            if let street = placeMark?.addressDictionary?["Thoroughfare"] as? String {
                address += street + ", "
            }
            
            // City
            if let city = placeMark?.addressDictionary?["City"] as? String {
                address += city + ", "
                formattedAddress += city + ", "
            }
            
            // Zip code
            if let zip = placeMark?.addressDictionary?["ZIP"] as? String {
                address += zip + ", "
                formattedAddress += zip + ", "
                self.selectedPinCode = zip
            }
            
            // Country
            if let country = placeMark?.addressDictionary?["Country"] as? String {
                address += country
                formattedAddress += country + ", "
            }
            self.marker.snippet = formattedAddress
           // Passing address back
           handler(address)
        })
    }
}
