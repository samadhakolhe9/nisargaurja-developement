//
//  RegistrationViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 01/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit
import GooglePlaces

var userRegisterParams:[String: Any] = ["phoneNo": "", "firstName": "", "lastName": "", "email": "", "title": "Home", "address": "", "house": "", "pincode": 000000, "lat": 0.0, "lng": 0.0, "isDeliverable": false, "addressUID": ""]

class RegistrationViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtFirstName: SKCustomTextField!
    @IBOutlet weak var txtLastName: SKCustomTextField!
    @IBOutlet weak var txtEmail: SKCustomTextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnExit: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnExit.titleLabel!.textAlignment = .right
        btnExit.titleLabel!.attributedText = NSMutableAttributedString(string: "Exit", attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.kern: 2.7])
        
        btnNext.layer.cornerRadius = 5
        btnNext.backgroundColor = .white
        btnNext.layer.backgroundColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        btnNext.layer.borderWidth = 1
        btnNext.layer.borderColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1).cgColor
        
        TextFiledGoToNext([txtFirstName,txtLastName, txtEmail])
        self.dismissKey()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        txtFirstName.bottomLine = true
        txtLastName.bottomLine = true
        txtEmail.bottomLine = true
    }
    
    @IBAction func ExitButtonAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "idLoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK:- UITextfield Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        (textField as! SKCustomTextField).linesColor = AppCommonColor
        return true
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text != "" {
            (textField as! SKCustomTextField).linesColor = AppCommonColor
        } else {
            (textField as! SKCustomTextField).linesColor = .red
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail {
            self.view.endEditing(true)
        } else {
            textField.nextField?.becomeFirstResponder()
        }
        validateTextFieldWithRedLine([txtFirstName,txtLastName, txtEmail], color: .lightGray)
        return true
    }
    
    @IBAction func NextButtonAction(_ sender: UIButton) {
        if txtFirstName.text == "" {
            ReusableManager.sharedInstance.showAlert(alertString: "Please enter a first name.")
        } else if txtLastName.text == "" {
            ReusableManager.sharedInstance.showAlert(alertString: "Please enter a last name.")
        } else if txtEmail.text == "" {
            ReusableManager.sharedInstance.showAlert(alertString: "Please enter a email address.")
        } else {
            if isValidEmail(txtEmail) {
                userRegisterParams["firstName"] = txtFirstName.text!
                userRegisterParams["lastName"] = txtLastName.text!
                userRegisterParams["email"] = txtEmail.text!
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "idSetDeliveryLocationController") as! SetDeliveryLocationController
                controller.isFromRegistration = true
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                txtEmail.linesColor = .red
                ReusableManager.sharedInstance.showAlert(alertString: "Please enter a valid email address.")
            }
        }
    }
}

extension RegistrationViewController: GMSAutocompleteViewControllerDelegate {

  // Handle the user's selection.
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    print("Place name: \(place.name!)")
    print("Place address: \(place.formattedAddress!)")
 // print("Place attributions: \(place.attributions!)")
    dismiss(animated: true, completion: nil)
    
  }

  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // TODO: handle the error.
    print("Error: ", error.localizedDescription)
  }

  // User canceled the operation.
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    dismiss(animated: true, completion: nil)
  }

  // Turn the network activity indicator on and off again.
  func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  }

  func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }

}


extension UIViewController {
    func dismissKey()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer( target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}

