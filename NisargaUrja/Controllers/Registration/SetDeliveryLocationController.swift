//
//  SetDeliveryLocationController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 18/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces
import GoogleMapsBase
import SwiftyJSON

var deviceLatitude = ""
var deviceLongitude = ""

class SetDeliveryLocationController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var txtSearchBox: SKCustomTextField!
    @IBOutlet weak var lblOrCurrentLocation: UILabel!
    @IBOutlet weak var btnCurrentLocation: UIButton!
    @IBOutlet weak var tblSearchedLocation: UITableView!
    
    var currentLocation: CLLocation!
    let locManager = CLLocationManager()
    var tableData = [GMSAutocompletePrediction]()
    var fetcher: GMSAutocompleteFetcher?
    var isFromRegistration = true
    var isChangeAddress = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // self.addLeftButtonWithImage(UIImage(named: "backArrow")!)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.hidesBackButton = false
        if isFromRegistration {
            self.title = "Set Delivery Location"
        } else {
            self.title = isChangeAddress ? "Change Delivery Location" : "Add Delivery Location"
        }
        btnCurrentLocation.setImage(btnCurrentLocation.imageView?.image?.imageWithColor(color: .black), for: .normal)
        btnCurrentLocation.setImage(btnCurrentLocation.imageView?.image?.imageWithColor(color: .black), for: .selected)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblOrCurrentLocation.applyCircleShadow()
        btnCurrentLocation.applyCommonShadow()
        tblSearchedLocation.estimatedRowHeight = 50
        tblSearchedLocation.rowHeight = UITableView.automaticDimension
        tblSearchedLocation.tableFooterView = UIView()
        self.edgesForExtendedLayout = []
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        filter.country = "IN"
        fetcher  = GMSAutocompleteFetcher(filter: filter)
        fetcher?.delegate = self

        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locManager.delegate = self
        let statusLoc = CLLocationManager.authorizationStatus()
        if statusLoc == .notDetermined{
            locManager.requestWhenInUseAuthorization()
        }
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.startUpdatingLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func getCoordinateBounds(latitude:CLLocationDegrees ,
                             longitude:CLLocationDegrees,
                             distance:Double = 0.001)->GMSCoordinateBounds{
        let center = CLLocationCoordinate2D(latitude: latitude,
                                            longitude: longitude)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + distance, longitude: center.longitude + distance)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - distance, longitude: center.longitude - distance)

        return GMSCoordinateBounds(coordinate: northEast,
                                   coordinate: southWest)

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        self.navigationController?.navigationBar.isHidden = true
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func UseCurrentLocationAction(_ sender: UIButton) {
        currentLocation = locManager.location
        if !hasLocationPermission() {
            let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertController.Style.alert)

            let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
                //Redirect to Settings app
                UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
            })

            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            alertController.addAction(cancelAction)

            alertController.addAction(okAction)

            self.present(alertController, animated: true, completion: nil)
        } else {
            if currentLocation != nil {
                deviceLatitude = "\(self.currentLocation.coordinate.latitude)"
                deviceLongitude = "\(self.currentLocation.coordinate.longitude)"
                self.title = ""
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "idAddressViewController") as! AddressViewController
                controller.locValue = CLLocationCoordinate2D(latitude: self.currentLocation.coordinate.latitude, longitude: self.currentLocation.coordinate.longitude)
                controller.isFromRegistration = isFromRegistration
                controller.isChangeAddress = isChangeAddress
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                ReusableManager.sharedInstance.showAlert(alertString: "Failed to get your current location, Please try again.")
            }
        }
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            @unknown default:
                fatalError()
            }
        } else {
            hasPermission = false
        }

        return hasPermission
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        (textField as! SKCustomTextField).linesColor = AppCommonColor
        tblSearchedLocation.isHidden = false
        return true
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text != "" {
            (textField as! SKCustomTextField).linesColor = AppCommonColor
            tblSearchedLocation.isHidden = false
        } else {
            (textField as! SKCustomTextField).linesColor = .lightGray
            tblSearchedLocation.isHidden = true
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        tblSearchedLocation.isHidden = false
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        fetcher?.sourceTextHasChanged(updatedString!)
        return true
    }
    
}
extension SetDeliveryLocationController: GMSAutocompleteFetcherDelegate {


func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {

    tableData.removeAll()

    for prediction in predictions{
        tableData.append(prediction)
        if (prediction.attributedSecondaryText!.string.lowercased()).contains("pune") {
        }
    }
    tblSearchedLocation.reloadData()
}

func didFailAutocompleteWithError(_ error: Error) {
    print(error.localizedDescription)
   }
}
extension SetDeliveryLocationController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GooglePlaceLocationCell

        cell.lblAddress.text = tableData[indexPath.row].attributedFullText.string
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tblSearchedLocation.isHidden = true
        txtSearchBox.text = tableData[indexPath.row].attributedFullText.string
        getLatLongFromAutocompletePrediction(prediction:tableData[indexPath.row])
    }
    
    func getLatLongFromAutocompletePrediction(prediction:GMSAutocompletePrediction) {
        let placeClient = GMSPlacesClient()
        placeClient.lookUpPlaceID(prediction.placeID) { (place, error) -> Void in
            if error != nil {
                return
            }
            if let place = place {
                self.title = ""
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "idAddressViewController") as! AddressViewController
                print(prediction.attributedFullText.string)
                controller.isFromRegistration = self.isFromRegistration
                controller.isChangeAddress = self.isChangeAddress
                controller.selectedPlace = place
                controller.selectedAddress = prediction.attributedFullText.string
                controller.locValue = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
}
