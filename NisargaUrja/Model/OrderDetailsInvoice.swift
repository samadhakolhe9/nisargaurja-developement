//
//  OrderDetailsInvoice.swift
//  NisargaUrja
//
//  Created by Bipin Tamkhane on 01/09/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import Foundation
import SwiftyJSON

class OrderDetails {
    private(set) var orderUID: String?
    private(set) var orderSubtotal: Double?
    private(set) var orderTotal: Double?
    private(set) var orderDate: String?
    private(set) var deliveryDate: String?
    private(set) var orderStatus: String?
    private(set) var deliveryStatus: String?
    private(set) var shippingAddress: String?
    private(set) var billingAddress: String?
    private(set) var deliveryCharges: Int?
    private(set) var quantityPurchased: [JSON]?
    private(set) var variationTitle: [JSON]?
    private(set) var productName: [JSON]?
    private(set) var discountedPrices: [JSON]?

    init(orderUID: String, orderSubtotal: Double, orderTotal: Double, orderDate: String, deliveryDate: String, orderStatus: String, deliveryStatus: String, shippingAddress: String, billingAddress: String, deliveryCharges: Int, quantityPurchased: [JSON], variationTitle: [JSON], productName: [JSON], discountedPrices: [JSON]) {
        self.orderUID = orderUID
        self.orderSubtotal = orderSubtotal
        self.orderTotal  = orderTotal
        self.orderDate = orderDate
        self.deliveryDate = deliveryDate
        self.orderStatus = orderStatus
        self.deliveryStatus = deliveryStatus
        self.shippingAddress = shippingAddress
        self.billingAddress = billingAddress
        self.deliveryCharges = deliveryCharges
        self.quantityPurchased = quantityPurchased
        self.variationTitle = variationTitle
        self.productName = productName
        self.discountedPrices = discountedPrices
    }
}
