//
//  UserAddresses.swift
//  NisargaUrja
//
//  Created by Bipin Tamkhane on 01/09/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import Foundation

class UserAddresses {
    private(set) var addressUID: String?
    private(set) var title: String?
    private(set) var houseNo: String?
    private(set) var address: String?
    private(set) var pincode: String?
    private(set) var isDeliverable: Bool?
    private(set) var isBillingAddress: Bool?
    private(set) var isDefaultAddress: Bool?
    
    init(addressUID: String,title: String, houseNo: String, address: String, pincode: String, isDeliverable: Bool, isBillingAddress: Bool, isDefaultAddress: Bool) {
        self.addressUID = addressUID
        self.title = title
        self.houseNo = houseNo
        self.address = address
        self.pincode = pincode
        self.isDeliverable = isDeliverable
        self.isBillingAddress = isBillingAddress
        self.isDefaultAddress = isDefaultAddress
    }
}
