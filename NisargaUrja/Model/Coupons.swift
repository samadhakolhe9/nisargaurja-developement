//
//  Coupons.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 26/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import Foundation

class Coupons {
    private(set) var couponUID: String
    private(set) var couponName: String
    private(set) var couponDescription: String
    private(set) var couponCode: String
    
    init(couponUID: String, couponName: String, couponDescription: String, couponCode: String) {
        self.couponUID = couponUID
        self.couponName = couponName
        self.couponDescription = couponDescription
        self.couponCode = couponCode
    }
}
