//
//  Cart.swift
//  NisargaUrja
//
//  Created by Bipin Tamkhane on 17/09/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import Foundation
import SwiftyJSON

struct CartItems {
    private(set) var brandName: String?
    private(set) var productName: String?
    private(set) var productPackageImage: String?
    private(set) var variationUID: String?
    private(set) var variationTitle: String?
    private(set) var quantity: Int?
    private(set) var MRP: String?
    private(set) var discountedPrice: String?
    private(set) var deliveryDate: String?
}
